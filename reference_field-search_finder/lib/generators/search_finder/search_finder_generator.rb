require 'rails/generators/named_base'

module Rails
  module Generators
    class SearchFinderGenerator < NamedBase
      include ResourceHelpers

      source_root File.expand_path("../templates", __FILE__)

      def create_view_file
        template "_search.html.erb", "app/views/#{controller_file_path}/finders/_search.html.erb"
      end

      hook_for :ajax, default: :ajax, in: :reference_field
      hook_for :test_framework
    end
  end
end
