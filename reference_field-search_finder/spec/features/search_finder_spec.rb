require 'rails_helper'

feature "SearchFinder", js: true do
  before do
    8.times do |n|
      create(:person, name: "AAA-#{n}")
      create(:person, name: "BBB-#{n}")
      create(:person, name: "CCC-#{n}")
    end
    visit edit_profile_path(profile, skip_animation: true)
  end

  let(:profile){ create(:profile) }
  let(:per){ 5 }

  let(:container){ find(".reference_field-finder[data-finder='search'] .search_finder-index") }

  shared_examples_for "include search result" do
    scenario "は、検索結果要素に検索結果の一覧を入れること" do
      expect(container).to have_selector(".event_button[data-event='reference_field:choice']", count: people.size)
      expect(container).to have_selector(".record_banner", text: /AAA-\d+/, count: people.size)
      people.each do |person|
        expect(container).to have_selector(".record_banner[data-record-id='#{person.id}']")
      end
    end
  end

  context "関連'profilable'の参照フィールドから参照ファインダーコンテナを開いて検索参照ファインダーを選択した場合" do
    before do
      click_button "探"
      find(".page-tab:nth-of-type(2)").click
    end

    scenario "は、検索結果が空であること" do
      expect(container).to have_no_selector(":scope > *")
    end

    context "検索条件を入力して送信した場合" do
      context "検索結果が1個以上ある場合" do
        before do
          fill_in "Name は以下を含む", with: "AAA"
          click_button "検索"
        end

        let(:people){ Person.ransack(name_cont: "AAA").result.limit(per) }

        scenario "は、検索結果要素にページナビゲーションを入れること" do
          expect(container).to have_selector("nav.pagination .page.current", text: "1")
          expect(container).to have_selector(".page_entries_info")
        end

        it_behaves_like "include search result"

        context "ページ移動リンクをクリックした場合" do
          before do
            click_link "Next"
          end

          let(:people){ Person.ransack(name_cont: "AAA").result.offset(per).limit(per) }

          scenario "は、検索結果要素にページナビゲーションを入れること" do
            expect(container).to have_selector("nav.pagination .page.current", text: "2")
            expect(container).to have_selector(".page_entries_info")
          end

          it_behaves_like "include search result"
        end

        context "参照先を決定して送信した場合" do
          before do
            within container.find("li:nth-child(3)") do
              click_button "決定"
            end
          end

          let(:new_profilable){ Person.ransack(name_cont: "AAA").result[2] }

          subject do
            proc{ click_button "Update Profile" }
          end

          scenario "は、選択した要素を関連先にセットすること" do
            expect(subject).to change{ profile.reload.profilable }.from(profile.profilable).to(new_profilable)
          end

          scenario "は、更新成功の通知を表示すること" do
            subject.call
            expect(page).to have_selector("#notice", text: "Profile was successfully updated.")
          end
        end
      end

      context "検索結果が0個の場合" do
        before do
          fill_in "Name は以下を含む", with: "unknown"
          click_button "検索"
        end

        scenario "は、検索結果要素にページ情報を入れること" do
          expect(container).to have_no_selector("nav.pagination")
          expect(container).to have_selector(".page_entries_info")
        end

        scenario "は、検索結果要素に検索結果を入れないこと" do
          expect(container).to have_no_selector(".event_button")
          expect(container).to have_no_selector(".record_banner")
        end
      end
    end
  end
end
