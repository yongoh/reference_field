require 'rails_helper'

feature "rails generate search_finder" do
  before do
    Dir.chdir(Rails.root)
    remove_files
    `rails generate search_finder profiles --responding=false --test-framework=rspec`
  end

  after do
    remove_files
  end

  def remove_files
    FileUtils.rm_r([
      "app/views/profiles/finders",
      "app/views/profiles/ajax",
      "spec/views/profiles/finders",
      "spec/views/profiles/ajax",
    ]) rescue Errno::ENOENT
  end

  let(:app_files){
    [
      "app/views/profiles/finders/_search.html.erb",
      "app/views/profiles/ajax/_index.html.erb",
    ]
  }

  let(:spec_files){
    [
      "spec/views/profiles/finders/_search.html.erb_spec.rb",
      "spec/views/profiles/ajax/_index.html.erb_spec.rb",
    ]
  }

  let(:generated_files){ app_files + spec_files }

  scenario "generate files" do
    generated_files.each do |path|
      expect(File).to be_exist(path)
    end
  end

  feature "run generated tests" do
    scenario "test succeeds" do
      stdout = `rspec #{spec_files.join(" ")}`
      expect(stdout).not_to match(/\b0 examples/)
      expect(stdout).to match(/\b0 failures/)
    end
  end

  feature "run destroy command" do
    before do
      `rails destroy search_finder profile --test-framework=rspec`
    end

    scenario "destroy files" do
      generated_files.each do |path|
        expect(File).not_to be_exist(path)
      end
    end
  end
end
