require 'rails_helper'

describe SearchFinder do
  let(:finder){ described_class.new(view_context, nil, "search") }

  describe "#ajax_target_field" do
    it "は、Ajaxの応答を格納する要素のセレクタを示す隠し入力欄を返すこと" do
      expect(finder.ajax_target_field).to have_tag("input", with: {type: "hidden", name: "ajax_target", value: "##{finder.unique_id}"})
    end
  end

  describe "#index_container" do
    it "は、Ajaxの応答を格納する要素を返すこと" do
      expect(finder.index_container).to have_tag(".search_finder-index##{finder.unique_id}")
    end
  end
end
