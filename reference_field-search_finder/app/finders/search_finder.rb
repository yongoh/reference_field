class SearchFinder < ReferenceField::Finder

  # @return [ActionView::SafeBuffer] Ajaxの応答を格納する要素のセレクタを示す隠し入力欄
  def ajax_target_field
    h.hidden_field_tag(:ajax_target, "##{unique_id}")
  end

  # @return [ActionView::SafeBuffer] Ajaxの応答を格納する要素
  def index_container
    h.tag(:div, id: unique_id, class: "search_finder-index")
  end
end
