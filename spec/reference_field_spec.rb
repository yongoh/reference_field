require 'rails_helper'

describe ReferenceField do
  describe "#referenced_field_banner" do
    it "は、参照バナー要素を返すこと" do
      expect(described_class.referenced_field_banner(view_context, ["Person", 123])).to have_tag(
        "span.referenced.record_banner",
        with: {"data-model" => "Person", "data-record-id" => "123"},
        text: "#<人物 id: 123>",
      )
    end
  end

  describe "#unreferenced_field_banner" do
    it "は、未参照状態を表すバナー要素を返すこと" do
      expect(described_class.unreferenced_field_banner(view_context)).to have_tag(
        "span.unreferenced.record_banner",
        text: "※未参照",
      )
    end
  end
end
