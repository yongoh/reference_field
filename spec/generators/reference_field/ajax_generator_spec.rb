require 'rails_helper'

feature "rails generate reference_field:ajax" do
  before do
    Dir.chdir(Rails.root)
    `rails g reference_field:ajax #{command_argument} #{command_options.join(" ")} --test-framework=rspec`
  end

  let(:command_argument){ "foo/bar/baz" }
  let(:command_options){ %w(--responding=false) }

  after do
    FileUtils.rm_r([
      "app/views/foo",
      "spec/views/foo",
    ]) rescue Errno::ENOENT
  end

  let(:app_view_path){ "app/views/foo/bar/bazs/ajax/_index.html.erb" }
  let(:spec_view_path){ "spec/views/foo/bar/bazs/ajax/_index.html.erb_spec.rb" }

  shared_examples_for "generate files" do
    scenario "各ファイルを作成" do
      [
        app_view_path,
        spec_view_path,
      ].each do |path|
        expect(File).to be_exist(path)
      end
    end

    feature "生成したテストを実行" do
      scenario "テスト成功" do
        stdout = `rspec #{spec_view_path}`
        expect(stdout).not_to match(/\b0 examples/)
        expect(stdout).to match(/\b0 failures/)
      end
    end
  end

  describe "オプション`--responding`" do
    after do
      xxx = File.read(controller_path).gsub(respond_to_regexp, "")
      open(controller_path, "w") do |file|
        file.puts(xxx)
      end

      FileUtils.rm_r([
        "app/views/profiles/ajax",
        "spec/views/profiles/ajax",
      ]) rescue Errno::ENOENT
    end

    let(:controller_path){ "app/controllers/profiles_controller.rb" }
    let(:app_view_path){ "app/views/profiles/ajax/_index.html.erb" }
    let(:spec_view_path){ "spec/views/profiles/ajax/_index.html.erb_spec.rb" }
    let(:command_argument){ "profile" }
    let(:respond_to_regexp){ %r{\s*^(\s*)respond_to do \|format\|$(.+?)^\1end}m }

    context "を渡さない場合 (default)" do
      let(:command_options){ [] }

      scenario "コントローラの`#index`内に`respond_to`ブロックを追加" do
        expect(File.read(controller_path).slice(/^(\s*)def index$(.+?)^\1end/m, 2)).to match(respond_to_regexp)
      end

      scenario "`respond_to`ブロック内に`format.js`を追加" do
        expect(File.read(controller_path).slice(respond_to_regexp, 2)).to match(/\bformat\.js\b/)
      end

      it_behaves_like "generate files"
    end

    context "に`false`を渡した場合" do
      scenario "コントローラの`#index`内に`respond_to`ブロックを追加しない" do
        expect(File.read(controller_path).slice(/^(\s*)def index$(.+?)^\1end/m, 2)).not_to match(/\brespond_to\b|\bformat\.js\b/)
      end

      it_behaves_like "generate files"
    end
  end

  describe "オプション`--fixture`" do
    context "に`factory_bot`を渡した場合 (default)" do
      scenario "レコードの作成にファクトリーを使用するテストを生成すること" do
        expect(File.read(spec_view_path)).to match(/\bcreate_list\(:baz,/)
      end
    end

    context "に`double`を渡した場合" do
      let(:command_options){ %w(--responding=false --fixture=double) }

      scenario "レコードの代わりにテストダブルを使用するテストを生成すること" do
        expect(File.read(spec_view_path)).to match(/\bdouble\(\s*"ActiveRecord::Base",/)
      end
    end

    context "に`dummy_record`を渡した場合" do
      let(:command_options){ %w(--responding=false --fixture=dummy_record) }

      scenario "レコードの代わりにテストダブルを使用するテストを生成すること" do
        expect(File.read(spec_view_path)).to include('RecordBanner::DummyRecord.build(["Foo::Bar::Baz", n])')
      end
    end
  end
end
