require 'rails_helper'

feature "rails generate reference_field:finder" do
  before do
    Dir.chdir(Rails.root)
    remove_files
  end

  after do
    remove_files
  end

  def remove_files
    FileUtils.rm_r([
      *app_file_paths.values,
      *spec_file_paths.values,
      "app/assets/javascripts/finders/foo",
      "app/assets/stylesheets/finders/foo",
    ]) rescue Errno::ENOENT
  end

  let(:generated_files){ app_file_paths.values + spec_file_paths.values }

  shared_examples_for "generate files and test" do
    scenario "各ファイルを生成" do
      generated_files.each do |path|
        expect(File).to be_exist(path)
      end
    end

    scenario "生成したテストを実行し成功" do
      stdout = `rspec #{spec_file_paths.values.join(" ")}`
      expect(stdout).not_to match(/\b0 examples/)
      expect(stdout).to match(/\b0 failures/)
    end

    scenario "正しいクラス名・参照ファインダー名を含むファイルを生成" do
      expect(File.read(app_file_paths[:coffee])).to match(/^class window.#{js_class_name} extends ReferenceField\.Finder$/)
      expect(File.read(app_file_paths[:scss])).to match(/\.reference_field-finder\[data-finder='#{finder_name}'\] \{/)
    end
  end

  context "参照ファインダー名を渡した場合" do
    before do
      `rails g reference_field:finder hoge --test-framework=rspec`
    end

    let(:app_file_paths){
      {
        finder: "app/finders/hoge_finder.rb",
        erb:    "app/views/application/finders/_hoge.html.erb",
        coffee: "app/assets/javascripts/finders/hoge_finder.js.coffee",
        scss:   "app/assets/stylesheets/finders/hoge.css.scss",
      }
    }
    let(:spec_file_paths){
      {
        finder: "spec/finders/hoge_finder_spec.rb",
        erb:    "spec/views/application/finders/_hoge.html.erb_spec.rb",
      }
    }

    let(:js_class_name){ "HogeFinder" }
    let(:finder_name){ "hoge" }

    it_behaves_like "generate files and test"
  end

  context "名前空間付きの参照ファインダー名を渡した場合" do
    before do
      `rails g reference_field:finder foo/bar/hoge --test-framework=rspec`
    end

    let(:app_file_paths){
      {
        finder: "app/finders/foo/bar/hoge_finder.rb",
        erb:    "app/views/application/finders/foo/bar/_hoge.html.erb",
        coffee: "app/assets/javascripts/finders/foo/bar/hoge_finder.js.coffee",
        scss:   "app/assets/stylesheets/finders/foo/bar/hoge.css.scss",
      }
    }
    let(:spec_file_paths){
      {
        finder: "spec/finders/foo/bar/hoge_finder_spec.rb",
        erb:    "spec/views/application/finders/foo/bar/_hoge.html.erb_spec.rb",
      }
    }

    let(:js_class_name){ "Foo.Bar.HogeFinder" }
    let(:finder_name){ 'foo\/bar\/hoge' }

    it_behaves_like "generate files and test"
  end
end
