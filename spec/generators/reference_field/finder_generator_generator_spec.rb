require 'rails_helper'

feature "rails generate reference_field:finder_generator" do
  before do
    Dir.chdir(Rails.root)
    remove_files
    `rails g reference_field:finder_generator hoge --ajax --test-framework=rspec`
  end

  after do
    remove_files
  end

  def remove_files
    FileUtils.rm_r([
      app_generator_root,
      spec_generator_root,
      generator_spec_path,
      script_path,
      stylesheet_path,
    ]) rescue Errno::ENOENT
  end

  let(:script_path){ "app/assets/javascripts/finders/hoge_finder.js.coffee" }
  let(:stylesheet_path){ "app/assets/stylesheets/finders/hoge.css.scss" }
  let(:generator_spec_path){ "spec/generators/hoge_finder_generator_spec.rb" }

  let(:app_generator_root){ "lib/generators/hoge_finder" }
  let(:spec_generator_root){ "lib/generators/rspec/hoge_finder" }

  let(:generated_files){
    [
      File.join(app_generator_root, "hoge_finder_generator.rb"),
      File.join(spec_generator_root, "hoge_finder_generator.rb"),
      generator_spec_path,
      File.join(app_generator_root, "USAGE"),
      File.join(app_generator_root, "templates/_hoge.html.erb"),
      File.join(spec_generator_root, "templates/_hoge.html.erb_spec.rb.erb"),
      "app/finders/hoge_finder.rb",
      "spec/finders/hoge_finder_spec.rb",
      script_path,
      stylesheet_path,
    ]
  }

  scenario "各ファイルを生成" do
    generated_files.each do |path|
      expect(File).to be_exist(path)
    end
  end

  feature "生成したテストを実行" do
    scenario "テスト成功" do
      stdout = `rspec #{generator_spec_path}`
      expect(stdout).not_to match(/\b0 examples/)
      expect(stdout).to match(/\b0 failures/)
    end

    scenario "正しいクラス名・参照ファインダー名を含むファイルを生成" do
      expect(File.read(script_path)).to match(/^class window\.HogeFinder extends ReferenceField\.Finder$/)
      expect(File.read(stylesheet_path)).to match(/\.reference_field-finder\[data-finder='hoge'\] \{/)
    end
  end

  feature "`destroy`コマンドを実行" do
    before do
      `rails destroy reference_field:finder_generator hoge --test-framework=rspec`
    end

    scenario "生成した各ファイルを削除" do
      generated_files.each do |path|
        expect(File).not_to be_exist(path)
      end
    end
  end
end
