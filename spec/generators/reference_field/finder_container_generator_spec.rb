require 'rails_helper'

feature "rails generate reference_field:finder_container" do
  before do
    Dir.chdir(Rails.root)
    remove_files
    finder_view_paths.each do |path|
      FileUtils.mkdir_p(path.sub(/[^\/]+\z/, ""))
      FileUtils.touch(path)
    end
    `rails g reference_field:finder_container foo/bar/baz hoge piyo:animals --test-framework=rspec`
  end

  after do
    remove_files
  end

  def remove_files
    FileUtils.rm_r([
      "app/views/foo",
      "spec/views/foo",
      "app/views/animals/finders",
    ]) rescue Errno::ENOENT
  end

  let(:app_view_path){ "app/views/foo/bar/bazs/finders/_finder_container.html.erb" }
  let(:spec_view_path){ "spec/views/foo/bar/bazs/finders/_finder_container.html.erb_spec.rb" }
  let(:generated_files){ [app_view_path, spec_view_path] }

  let(:finder_view_paths){
    [
      "app/views/foo/bar/bazs/finders/_hoge.html.erb",
      "app/views/animals/finders/_piyo.html.erb",
    ]
  }

  scenario "各ファイルを生成" do
    generated_files.each do |path|
      expect(File).to be_exist(path)
    end
  end

  feature "生成したテストを実行" do
    scenario "テスト成功" do
      stdout = `rspec #{spec_view_path}`
      expect(stdout).not_to match(/\b0 examples/)
      expect(stdout).to match(/\b0 failures/)
    end
  end

  feature "`destroy`コマンドを実行" do
    before do
      `rails destroy reference_field:finder_container foo/bar/baz --test-framework=rspec`
    end

    scenario "生成した各ファイルを削除" do
      generated_files.each do |path|
        expect(File).not_to be_exist(path)
      end
    end
  end
end
