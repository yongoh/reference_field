RSpec.configure do |config|
  config.include(Module.new do
    def view_context
      @view_context ||= Class.new(ActionView::Base) do
        include Rails.application.routes.url_helpers
        include Ransack::Helpers::FormHelper
        include ReferenceField::ApplicationHelper
        include EventButton::ApplicationHelper
        include ModalWindow::ApplicationHelper
        include ActionField::FormHelper
        include RecordBanner::ApplicationHelper
        include RadioButtonTab::ApplicationHelper

        def initialize(*args, &block)
          super(*args, &block)
          view_paths << File.join(Rails.root, "app/views")
        end

        def default_url_options
          {host: 'www.example.com'}
        end
      end.new
    end

    alias_method :h, :view_context
  end)
end
