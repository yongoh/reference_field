require 'rails_helper'

describe ActionView::Helpers::FormBuilder do
  let(:form_builder){ h.form_for(person){|f| return f } }

  describe "#reference_field" do
    subject{ form_builder.reference_field(:parent) }
    let(:person){ create(:person, :with_parent) }

    it "は、参照フィールドを返すこと" do
      is_expected.to have_tag("label.reference_field-field#person_parent-reference_field"){
        with_tag("input.foreign_type#person_parent-foreign_type:not([name])", with: {type: "hidden", value: "Person"})
        with_tag("input.foreign_key#person_parent-foreign_key", with: {type: "hidden", name: "person[parent_id]", value: person.parent.id})
        with_tag(".event_button", with: {"data-event" => "reference_field:find"})
        with_tag(".banner_container")
      }
    end

    it "は、ブロック引数に参照フィールドインスタンスを渡すこと" do
      expect{|block| form_builder.reference_field(:parent, &block) }.to yield_with_args(be_a(ReferenceField::Field))
    end

    shared_examples_for "unreferenced banner" do
      it "は、未参照バナー要素を含む参照フィールド要素を返すこと" do
        is_expected.to have_tag(".reference_field-field"){
          with_tag(".banner_container"){
            with_tag(".record_banner.unreferenced")
          }
        }
      end
    end

    context "オプション引数`:banner_proc`に手続きを渡した場合" do
      it "は、手続き引数に参照先レコードを渡すこと" do
        expect{|block| form_builder.reference_field(:parent, banner_proc: block) }.to yield_with_args(equal(person.parent))
      end
    end

    context "ポリモーフィック関連の場合" do
      subject{ form_builder.reference_field(:profilable) }
      let(:form_builder){ h.form_for(profile){|f| return f } }

      shared_examples_for "has name attribute" do
        it "は、属性`value`を持たず`name`を持つforeign_key入力欄を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_key:not([value])", with: {name: "profile[profilable_id]"})
          }
        end
      end

      shared_examples_for "has for attribute" do
        it "は、属性`for`を持つイベントボタン要素を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".event_button", with: {"data-event" => "reference_field:find", for: "profilable-trigger"})
          }
        end
      end

      context "関連先が設定されていない場合" do
        let(:profile){ build(:profile, profilable: nil) }

        it "は、属性`value`,`pattern`を持たず`name`を持つforeign_type入力欄を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_type:not([value]):not([pattern])", with: {name: "profile[profilable_type]"})
          }
        end

        it_behaves_like "has name attribute"
        it_behaves_like "has for attribute"
        it_behaves_like "unreferenced banner"
      end

      context "関連先が未保存である場合" do
        let(:profile){ build(:profile, profilable: build(:person)) }

        it "は、属性`pattern`を持たず`name`,`value`を持つforeign_type入力欄を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_type:not([pattern])", with: {name: "profile[profilable_type]", value: "Person"})
          }
        end

        it_behaves_like "has name attribute"
        it_behaves_like "has for attribute"

        it "は、参照バナー要素を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".banner_container"){
              with_tag(".record_banner.referenced:not([data-record-id])", with: {"data-model" => "Person"})
            }
          }
        end
      end

      context "関連先が保存済みである場合" do
        let(:profile){ build(:profile, :with_company) }

        it "は、属性`pattern`を持たず`name`,`value`を持つforeign_type入力欄を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_type:not([pattern])", with: {name: "profile[profilable_type]", value: "Company"})
          }
        end

        it "は、属性`name`,`value`を持つforeign_key入力欄を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_hidden_field("profile[profilable_id]", profile.profilable.id)
          }
        end

        it_behaves_like "has for attribute"

        it "は、参照バナー要素を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".banner_container"){
              with_tag(".record_banner.referenced", with: {"data-model" => "Company", "data-record-id" => profile.profilable.id})
            }
          }
        end
      end
    end

    context "ポリモーフィック関連ではない場合" do
      shared_examples_for "has name attribute" do
        it "は、属性`value`を持たず`name`を持つforeign_key入力欄を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_key:not([value])", with: {name: "person[parent_id]"})
          }
        end
      end

      shared_examples_for "has for attribute" do
        it "は、属性`for`を持つイベントボタン要素を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".event_button", with: {"data-event" => "reference_field:find", for: "people-trigger"})
          }
        end
      end

      context "関連先が設定されていない場合" do
        let(:person){ create(:person) }

        it "は、属性`value`,`name`を持たず`pattern`を持つforeign_type入力欄を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_type:not([value]):not([name])", with: {pattern: "^Person$"})
          }
        end

        it_behaves_like "has name attribute"
        it_behaves_like "has for attribute"
        it_behaves_like "unreferenced banner"
      end

      context "関連先が未保存である場合" do
        before do
          person.parent = build(:person)
        end

        it "は、属性`name`を持たず`value`,`pattern`を持つforeign_type入力欄を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_type:not([name])", with: {value: "Person", pattern: "^Person$"})
          }
        end

        it_behaves_like "has name attribute"
        it_behaves_like "has for attribute"

        it "は、参照バナー要素を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".banner_container"){
              with_tag(".record_banner.referenced:not([data-record-id])", with: {"data-model" => "Person"})
            }
          }
        end
      end

      context "関連先が保存済みである場合" do
        it "は、属性`name`を持たず`value`,`pattern`を持つforeign_type入力欄を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_type:not([name])", with: {value: "Person", pattern: "^Person$"})
          }
        end

        it "は、属性`name`,`value`を持つforeign_key入力欄を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_hidden_field("person[parent_id]", person.parent.id)
          }
        end

        it_behaves_like "has for attribute"

        it "は、参照バナー要素を含む参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".banner_container"){
              with_tag(".record_banner.referenced", with: {"data-model" => "Person", "data-record-id" => person.parent.id})
            }
          }
        end
      end
    end

    describe "バリデーションエラー" do
      subject{ form_builder.reference_field(:profilable) }

      before do
        allow(klass).to receive(:name).and_return("Profile")
      end

      let(:form_builder){ h.form_for(klass.new){|f| return f } }
      let(:klass){ Class.new(Profile) }

      shared_examples_for "field_with_errors" do
        it "は、指標付きの参照フィールドを返すこと" do
          is_expected.to have_tag(".field_with_errors"){
            with_tag(".reference_field-field"){
              without_tag(".field_with_errors")
            }
          }
        end
      end

      context "バリデーションエラーが無い場合" do
        before do
          form_builder.object.valid?
        end

        it "は、指標無しの参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field")
          is_expected.not_to have_tag(".field_with_errors")
        end
      end

      context "参照先モデル名にバリデーションエラーがある場合" do
        before do
          klass.validates(:profilable_type, presence: true)
          form_builder.object.valid?
        end

        it_behaves_like "field_with_errors"
      end

      context "参照先IDにバリデーションエラーがある場合" do
        before do
          klass.validates(:profilable_id, presence: true)
          form_builder.object.valid?
        end

        it_behaves_like "field_with_errors"
      end

      context "参照先モデル名と参照先IDにバリデーションエラーがある場合" do
        before do
          klass.validates(:profilable_type, presence: true)
          klass.validates(:profilable_id, presence: true)
          form_builder.object.valid?
        end

        it_behaves_like "field_with_errors"
      end
    end

    describe "オプション`:self_referencing`" do
      context "に`true`を渡した場合 (default)" do
        it "は、参照禁止レコードを指定していない参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field:not([data-prohibited-targets])")
        end
      end

      context "に`false`を渡した場合" do
        subject do
          form_builder.reference_field(:parent, self_referencing: false)
        end

        it "は、参照禁止レコードに関連元を指定した参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field", with: {"data-prohibited-targets" => "Person:#{person.id}"})
        end
      end
    end
  end

  describe "#collection_reference_field" do
    subject do
      form_builder.collection_reference_field(:friends)
    end

    let(:person){ create(:person, :with_friends) }

    it "は、参照フィールドを含むActionFieldコンテナを返すこと" do
      is_expected.to have_tag(
        ".action_field-container#person_friends_attributes-action_field-container",
        with: {"data-object-name" => "person[friends_attributes]"},
      ){
        person.friends.each_with_index do |friend, i|
          with_tag(
            ".action_field-member#person_friends_attributes_#{i}-action_field-member",
            with: {"data-object-name" => "person[friends_attributes][#{i}]"},
            count: 1,
          ){
            with_tag("label.reference_field-field#person_friends_attributes_#{i}-reference_field", with: {for: "person_friends_attributes_#{i}-find"}){
              with_tag("input.foreign_type#person_friends_attributes_#{i}-foreign_type:not([name])", with: {type: "hidden", value: "Person", pattern: "^Person$"})
              with_tag("input.foreign_key#person_friends_attributes_#{i}-foreign_key", with: {type: "hidden", name: "person[friend_ids][]", value: friend.id})
              with_tag(".event_button#person_friends_attributes_#{i}-find", with: {"data-event" => "reference_field:find"})
              with_tag(".banner_container"){
                with_tag(".record_banner.referenced", with: {"data-model" => "Person", "data-record-id" => friend.id})
              }
            }

            with_tag(".event_button", with: {"data-event" => "action_field:remove", for: "person_friends_attributes_#{i}-action_field-member"})
          }
        end

        with_tag(".action_field-member", count: person.friends.size)

        without_tag("input[name$='[id]']")
        without_tag("input[name$='[_destroy]']")
      }
    end

    it "は、複数コンテナ用のイベントボタンを含むメニュー要素を生成すること" do
      is_expected.to have_tag(".action_field-menu#person_friends_attributes-action_field-menu"){
        with_tag(".event_button[data-template]", with: {"data-event" => "action_field:add", for: "person_friends_attributes-action_field-container"})
        with_tag(".event_button", with: {"data-event" => "action_field:clear", for: "person_friends_attributes-action_field-container"})
      }
    end

    it "は、ブロック引数に参照フィールドインスタンスを渡すこと" do
      expect{|block| form_builder.collection_reference_field(:friends, &block) }.to yield_successive_args(
        *person.friends.map{|friend|
          be_a(ReferenceField::Field)
        },
        be_a(ReferenceField::Field)
      )
    end

    describe "イベントボタンのテンプレート" do
      let(:template_member){ subject.slice(/\bdata-template="(.+?)"/, 1).gsub("&quot;", '"') }

      it "は、未参照状態の参照フィールドを含むメンバー要素であること" do
        expect(template_member).to have_tag(".action_field-member", count: 1){
          with_tag(".reference_field-field"){
            with_tag(".foreign_type:not([name]):not([value])", with: {type: "hidden", pattern: "^Person$"})
            with_tag(".foreign_key:not([value])", with: {type: "hidden", name: "person[friend_ids][]"})
            with_tag(".event_button", with: {"data-event" => "reference_field:find"})
            with_tag(".banner_container"){
              with_tag(".record_banner.unreferenced")
            }
          }

          with_tag(".event_button", with: {"data-event" => "action_field:remove"})
        }
      end
    end

    context "オプション引数`:banner_proc`に手続きを渡した場合" do
      it "は、手続き引数に参照先レコードを渡すこと" do
        expect{|block| form_builder.collection_reference_field(:friends, banner_proc: block) }.to yield_successive_args(
          *person.friends.map{|friend|
            equal(friend)
          },
        )
      end
    end

    describe "第2引数" do
      context "を渡さない場合 (default)" do
        it "は、関連先を参照した状態の参照フィールド群を含むActionFieldコンテナを返すこと" do
          is_expected.to have_tag(".action_field-container"){
            person.friends.each do |friend|
              with_tag(".action_field-member .reference_field-field"){
                with_hidden_field("person[friend_ids][]", friend.id)
              }
            end
          }
        end
      end

      context "にレコードの配列を渡した場合" do
        subject do
          form_builder.collection_reference_field(:friends, others)
        end

        let(:others){ create_list(:person, 3) }

        it "は、渡したレコード群を参照した状態の参照フィールド群を含むActionFieldコンテナを返すこと" do
          is_expected.to have_tag(".action_field-container"){
            others.each do |other|
              with_tag(".action_field-member .reference_field-field"){
                with_hidden_field("person[friend_ids][]", other.id)
              }
            end

            person.friends.each do |friend|
              without_hidden_field("person[friend_ids][]", friend.id)
            end
          }
        end
      end
    end

    describe "オプション`:self_referencing`" do
      context "に`true`を渡した場合 (default)" do
        subject do
          form_builder.collection_reference_field(:friends)
        end

        it "は、参照禁止レコードを指定していない参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field:not([data-prohibited-targets])", count: person.friends.size)
        end
      end

      context "に`false`を渡した場合" do
        subject do
          form_builder.collection_reference_field(:friends, self_referencing: false)
        end

        it "は、参照禁止レコードに関連元を指定した参照フィールドを返すこと" do
          person.friends.each do |friend|
            is_expected.to have_tag(".reference_field-field", with: {"data-prohibited-targets" => "Person:#{person.id}"})
          end
        end
      end
    end
  end

  describe "#reference_label" do
    let(:person){ create(:person, :with_parent) }

    it "は、「探」イベントボタンに紐付けられたラベル要素を返すこと" do
      expect(form_builder.reference_label(:parent)).to have_tag("label", with: {for: "person_parent-find"}, text: "Parent")
    end
  end
end
