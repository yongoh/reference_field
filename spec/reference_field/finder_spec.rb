require 'rails_helper'

describe ReferenceField::Finder do
  describe "::build" do
    context "対応するクラスが定義されている参照ファインダー名を第2引数に渡した場合" do
      it "は、その参照ファインダークラスのインスタンスを返すこと" do
        expect(described_class.build(view_context, nil, "foo/bar/baz")).to be_instance_of(Foo::Bar::BazFinder)
      end
    end

    context "対応するクラスが定義されていない参照ファインダー名を第2引数に渡した場合" do
      it "は、基底参照ファインダークラスのインスタンスを返すこと" do
        expect(described_class.build(view_context, nil, "undefined")).to be_instance_of(described_class)
      end
    end

    context "オプション`:finder_class`にクラスを渡した場合" do
      let(:klass){ Struct.new(:template, :container_name, :name, :options) }

      it "は、渡したクラスのインスタンスを返すこと" do
        expect(described_class.build(view_context, nil, "foo/bar/baz", finder_class: klass)).to be_instance_of(klass)
      end
    end

    describe "オプション`:templates`" do
      context "を渡さない場合 (default)" do
        subject do
          described_class.build(view_context, nil, "hoge")
        end

        it "は、デフォルトのパスのみを含む配列を`#templates`が返す参照ファインダーインスタンスを返すこと" do
          is_expected.to be_a(ReferenceField::Finder).and have_attributes(templates: match([
            "application/finders/hoge",
          ]))
        end
      end

      context "に配列を渡した場合" do
        subject do
          described_class.build(view_context, nil, "hoge", templates: ["foo/bar/baz", {dir: "people"}, {file: "piyo"}])
        end

        it "は、中身をパス化した配列を`#templates`が返す参照ファインダーインスタンスを返すこと" do
          is_expected.to be_a(ReferenceField::Finder).and have_attributes(templates: match([
            "foo/bar/baz",
            "people/finders/hoge",
            "application/finders/piyo",
            "application/finders/hoge",
          ]))
        end
      end

      context "に文字列を渡した場合" do
        subject do
          described_class.build(view_context, nil, "hoge", templates: "foo/bar/baz")
        end

        it "は、渡した文字列を含む配列を`#templates`が返す参照ファインダーインスタンスを返すこと" do
          is_expected.to be_a(ReferenceField::Finder).and have_attributes(templates: match([
            "foo/bar/baz",
            "application/finders/hoge",
          ]))
        end
      end
    end
  end

  describe "::init_path" do
    context "空のハッシュを渡した場合" do
      it "は、デフォルトの部分テンプレートへのパスを返すこと" do
        expect(described_class.init_path({}){ "hoge" }).to eq("application/finders/hoge")
      end
    end

    context "キー`:dir`を含むハッシュを渡した場合" do
      it "は、ディレクトリ部分を入れ替えたの部分テンプレートへのパスを返すこと" do
        expect(described_class.init_path({dir: "people"}){ "hoge" }).to eq("people/finders/hoge")
      end
    end

    context "キー`:file`を含むハッシュを渡した場合" do
      it "は、ファイル名部分を入れ替えた部分テンプレートへのパスを返すこと" do
        expect(described_class.init_path({file: "piyo"}){ "hoge" }).to eq("application/finders/piyo")
      end
    end

    context "キー`:dir`と`:file`を含むハッシュを渡した場合" do
      it "は、ディレクトリ・ファイル名部分を入れ替えたの部分テンプレートへのパスを返すこと" do
        expect(described_class.init_path({dir: "people", file: "piyo"})).to eq("people/finders/piyo")
      end
    end

    context "文字列を渡した場合" do
      it "は、その文字列を返すこと" do
        expect(described_class.init_path("foo/bar/baz")).to eq("foo/bar/baz")
      end
    end

    context "キー`:file`を持つハッシュを渡し、ブロックを渡さない場合" do
      it{ expect(described_class.init_path({file: "piyo"})).to eq("application/finders/piyo") }
    end

    context "キー`:file`を持たないハッシュを渡し、ブロックを渡した場合" do
      it{ expect(described_class.init_path({}){ "piyo" }).to eq("application/finders/piyo") }
    end

    context "キー`:file`を持たないハッシュを渡し、ブロックを渡さない場合" do
      it{ expect{ described_class.init_path({}) }.to raise_error(KeyError, "key not found: :file") }
    end
  end

  describe "::parse" do
    context "区切り無しの文字列を渡した場合" do
      it "は、渡した文字列をキー`:finder`に持つハッシュを返すこと" do
        expect(described_class.parse("foo")).to match(finder: "foo")
      end
    end

    context "':'を含む文字列を渡した場合" do
      it "は、':'の前をキー`:finder`に、':'の後をキー`:dir`に持つハッシュを返すこと" do
        expect(described_class.parse("foo:bar")).to match(finder: "foo", dir: "bar")
      end
    end

    context "'#'を含む文字列を渡した場合" do
      it "は、'#'の前をキー`:finder`に、'#'の後をキー`:file`に持つハッシュを返すこと" do
        expect(described_class.parse("foo#baz")).to match(finder: "foo", file: "baz")
      end
    end

    context "':','#'を含む文字列を渡した場合" do
      it "は、キー`:finder`,`:dir`,`:file`に分割したハッシュを返すこと" do
        expect(described_class.parse("foo:bar#baz")).to match(finder: "foo", dir: "bar", file: "baz")
      end

      it "は、キー`:finder`,`:dir`,`:file`に分割したハッシュを返すこと" do
        expect(described_class.parse("foo#baz:bar")).to match(finder: "foo", dir: "bar", file: "baz")
      end
    end
  end

  let(:finder){ described_class.new(view_context, nil, "baz") }

  describe "#human_name" do
    subject{ finder.human_name }

    context "訳文のある参照ファインダー名の場合" do
      let(:finder){ described_class.new(view_context, nil, "foo") }
      it{ is_expected.to eq("ふー") }
    end

    context "訳文の無い参照ファインダー名の場合" do
      let(:finder){ described_class.new(view_context, nil, "undefined") }
      it{ is_expected.to eq("Undefined") }
    end
  end

  describe "#render" do
    context "存在する部分テンプレートのパスを含む配列を渡した場合" do
      it "は、部分テンプレートをレンダリングして返すこと" do
        expect(finder.render(["application/finders/baz", "application/finders/select_box"])).to have_tag(":root"){
          with_tag("h2", text: "Baz")
          with_tag("p", text: "application/finders/baz")
        }
      end
    end

    context "存在しない部分テンプレートのパスを含む配列を渡した場合" do
      context "次のパスが存在する場合" do
        it "は、部分テンプレートをレンダリングして返すこと" do
          expect(finder.render(["application/finders/foo", "application/finders/baz"])).to have_tag(":root"){
            with_tag("h2", text: "Baz")
            with_tag("p", text: "application/finders/baz")
          }
        end
      end

      context "全て存在しないパスの場合" do
        it{ expect{ finder.render(["application/finders/foo", "application/finders/bar"]) }.to raise_error(ActionView::MissingTemplate) }
      end
    end
  end

  describe "#radio_button" do
    let(:finder){ described_class.new(view_context, "foo[bar]", "baz") }

    it "は、ラジオボタン要素を返すこと" do
      expect(finder.radio_button).to have_tag(
        "input.reference_field-finder[id][value]",
        with: {type: "radio", name: "foo[bar]", "data-finder" => "baz"},
      )
    end
  end

  describe "#label" do
    subject{ finder.label }

    it "は、タブ要素を返すこと" do
      is_expected.to have_tag(".page-tab.reference_field-finder[for]", with: {"data-finder" => "baz"})
    end

    context "オプションなしの場合 (default)" do
      context "訳文のある参照ファインダー名の場合" do
        let(:finder){ described_class.new(view_context, nil, "foo") }

        it "は、その訳文を返すこと" do
          is_expected.to have_tag(".page-tab", text: "ふー")
        end
      end

      context "訳文の無い参照ファインダー名の場合" do
        let(:finder){ described_class.new(view_context, nil, "undefined") }

        it "は、参照ファインダー名を自然言語化した文字列を内容として持つタブ要素を返すこと" do
          is_expected.to have_tag(".page-tab", text: "Undefined")
        end
      end
    end
  end

  describe "#section" do
    let(:finder){ described_class.new(view_context, nil, "baz", templates: ["application/finders/baz"]) }

    it "は、本文要素を返すこと" do
      expect(finder.section).to have_tag(".page-content.reference_field-finder[id]", with: {"data-finder" => "baz"})
    end
  end
end
