require 'rails_helper'

describe ReferenceField::Field::Label do
  let(:element){ described_class.new(view_context) }

  describe "#render" do
    subject do
      element.render do
        h.concat(h.content_tag(:div, "(´･ω･｀)", id: "shobon"))
        h.concat(h.content_tag(:span, "（＾ω＾）", class: "boon"))
      end
    end

    it "は、ラベル要素を返すこと" do
      is_expected.to have_tag("label.reference_field-field"){
        with_tag("div#shobon", text: "(´･ω･｀)")
        with_tag("span.boon", text: "（＾ω＾）")
      }
    end
  end

  describe "#prohibited_targets=" do
    before do
      element.prohibited_targets = people + companies
    end

    let(:people){ create_list(:person, 2) }
    let(:companies){ create_list(:company, 2) }

    it "は、渡したレコード群への参照を禁止する指標をつけること" do
      expect(element.html_attributes[:data][:prohibited_targets]).to eq(
        "Person:#{people[0].id},Person:#{people[1].id},Company:#{companies[0].id},Company:#{companies[1].id}"
      )
    end
  end

  describe "#required=" do
    shared_examples_for "status method" do
      context "`true`を渡したあと" do
        before do
          element.required = true
        end

        it "は、必須状態指標を持っていること" do
          expect(element.html_attributes[:class]).to include("required")
        end
      end

      context "`false`を渡したあと" do
        before do
          element.required = false
        end

        it "は、必須状態指標を持っていないこと" do
          expect(element.html_attributes[:class]).not_to include("required")
        end
      end
    end

    context "必須状態指標を持っている場合" do
      it_behaves_like "status method"
    end

    context "必須状態指標を持っていない場合" do
      before do
        element.html_attributes.html_attribute_merge!(class: "required")
      end

      it_behaves_like "status method"
    end
  end
end
