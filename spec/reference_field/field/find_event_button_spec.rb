require 'rails_helper'

describe ReferenceField::Field::FindEventButton do
  let(:element){ described_class.new(view_context) }

  describe "#render" do
    before do
      element.html_attributes.html_attribute_merge!(id: "my-id", class: "my-class", for: "my-for")
    end

    it "は、`#html_attributes`をHTML属性値として持つ「探」イベントボタン要素を返すこと" do
      expect(element.render).to have_tag(
        ".event_button#my-id.my-class",
        with: {"data-event" => "reference_field:find", for: "my-for"},
        text: "探",
      )
    end
  end

  describe "#content=" do
    before do
      element.content = "(´･ω･｀)"
    end

    it "は、イベントボタン要素の内容をセットすること" do
      expect(element.render).to have_tag(".event_button", text: "(´･ω･｀)")
    end
  end

  describe "#for=" do
    before do
      element.for = "foo[bar][baz]"
    end

    it "は、モーダルウインドウのトリガー要素に対応した`for`属性値をセットすること" do
      expect(element.html_attributes[:for]).to eq("foo_bar_baz-trigger")
    end
  end
end
