require 'rails_helper'

describe ReferenceField::Field::HiddenField do
  let(:element){ described_class.new(view_context) }

  describe "#render" do
    before do
      element.html_attributes.html_attribute_merge!(id: "my-id", class: "my-class", name: "my-name")
    end

    it "は、`#html_attributes`をHTML属性値として持つ隠し入力欄要素を返すこと" do
      expect(element.render).to have_tag("input#my-id.my-class", with: {name: "my-name"})
    end
  end

  describe "#pattern=" do
    context "文字列を渡した場合" do
      before do
        element.pattern = "^Foo(Bar|Baz)$"
      end

      it "は、引数をHTML属性`pattern`の値にセットすること" do
        expect(element.html_attributes[:pattern]).to eq("^Foo(Bar|Baz)$")
      end
    end

    context "配列を渡した場合" do
      before do
        element.pattern = ["Foo", :Bar, Person]
      end

      it "は、引数を連結したパターン文字列をHTML属性`pattern`の値にセットすること" do
        expect(element.html_attributes[:pattern]).to eq("^Foo|Bar|Person$")
      end
    end
  end

  describe "#referenced?" do
    subject{ element.referenced? }

    context "`#html_attributes`が`value`属性を持たない場合" do
      it{ is_expected.to equal(false) }
    end

    context "`#html_attributes`が`value`属性を持つ場合" do
      before do
        element.html_attributes.merge!(value: "my-value")
      end

      it{ is_expected.to equal(true) }
    end
  end
end
