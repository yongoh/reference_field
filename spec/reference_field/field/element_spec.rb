require 'rails_helper'

describe ReferenceField::Field::Element do
  let(:element){ sub_class.new(view_context) }
  let(:sub_class){ Class.new(described_class) }

  describe "::html_attr_accessor" do
    before do
      sub_class.html_attr_accessor :foo, :bar, :baz
    end

    it "は、アクセサメソッドを定義すること" do
      expect(sub_class.instance_methods).to include(
        :foo, :bar, :baz,
        :foo=, :bar=, :baz=,
      )
    end

    describe "定義したメソッド" do
      describe "ゲッターメソッド" do
        before do
          element.html_attributes[:foo] = "Foo!"
        end

        it "は、メソッド名のHTML属性の値を返すこと" do
          expect(element.foo).to eq("Foo!")
        end
      end

      describe "セッターメソッド" do
        subject do
          element.foo = "Foo!"
        end

        it "は、メソッド名のHTML属性に値をセットすること" do
          subject
          expect(element.html_attributes[:foo]).to eq("Foo!")
        end

        it "は、引数を返すこと" do
          is_expected.to eq("Foo!")
        end
      end
    end
  end

  describe "::boolean_html_attr_accessor" do
    before do
      sub_class.boolean_html_attr_accessor :foo, :bar, :baz
    end

    it "は、アクセサメソッドを定義すること" do
      expect(sub_class.instance_methods).to include(
        :foo?, :bar?, :baz?,
        :foo=, :bar=, :baz=,
      )
      expect(sub_class.instance_methods).not_to include(:foo, :bar, :baz)
    end

    describe "定義したメソッド" do
      describe "ゲッターメソッド" do
        context "メソッド名のHTML属性がtruthyな値を持つ場合" do
          before do
            element.html_attributes[:foo] = "Foo!"
          end

          it{ expect(element.foo?).to equal(true) }
        end

        context "メソッド名のHTML属性がfalseyな値を持つ場合" do
          before do
            element.html_attributes[:foo] = nil
          end

          it{ expect(element.foo?).to equal(false) }
        end
      end

      describe "セッターメソッド" do
        context "truthyな値を渡した場合" do
          subject do
            element.foo = "Foo!"
          end

          it "は、メソッド名のHTML属性に`true`をセットすること" do
            subject
            expect(element.html_attributes[:foo]).to equal(true)
          end

          it "は、引数を返すこと" do
            is_expected.to eq("Foo!")
          end
        end

        context "falseyな値を渡した場合" do
          subject do
            element.foo = nil
          end

          it "は、メソッド名のHTML属性を削除すること" do
            subject
            expect(element.html_attributes).not_to have_key(:foo)
          end

          it "は、引数を返すこと" do
            is_expected.to be_nil
          end
        end
      end
    end
  end
end
