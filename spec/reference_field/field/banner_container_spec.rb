require 'rails_helper'

describe ReferenceField::Field::BannerContainer do
  let(:element){ described_class.new(view_context) }

  describe "#render" do
    it "は、バナーコンテナ要素を返すこと" do
      expect(element.render).to have_tag("span.banner_container"){
        with_tag(".unreferenced")
      }
    end

    it "は、`#banner`にブロックを委譲すること" do
      banner_proc = proc{}
      expect(element).to receive(:banner){|&block|
        expect(block).to equal(banner_proc)
      }
      element.render(&banner_proc)
    end
  end

  describe "#banner" do
    subject{ element.banner }

    context "`#record`が保存済みレコードを返す場合" do
      before do
        allow(element).to receive(:record).and_return(person)
      end

      let(:person){ create(:person) }

      it "は、参照バナー要素を返すこと" do
        is_expected.to have_tag(".record_banner.referenced", with: {"data-model" => "Person", "data-record-id" => person.id})
      end
    end

    context "`#record`が未保存のレコードを返す場合" do
      before do
        allow(element).to receive(:record).and_return(person)
      end

      let(:person){ build(:person) }

      context "ブロックを渡さない場合" do
        it "は、参照バナー要素を返すこと" do
          is_expected.to have_tag(".record_banner.referenced:not([data-record-id])", with: {"data-model" => "Person"})
        end
      end

      context "ブロックを渡した場合" do
        subject do
          element.banner do
            h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
            h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
          end
        end

        let(:person){ create(:person) }

        it "は、ブロック内で作成した要素を返すこと" do
          is_expected.to have_tag("div#shobon", text: "(´･ω･｀)")
          is_expected.to have_tag("span.boon", text: "（＾ω＾）")
        end

        it "は、ブロック引数に`#record`の結果を渡すこと" do
          expect{|block| element.banner(&block) }.to yield_with_args(equal(person))
        end
      end
    end

    context "`#record`が`nil`を返す場合" do
      before do
        allow(element).to receive(:record).and_return(nil)
      end

      it "は、未参照バナー要素を返すこと" do
        is_expected.to have_tag(".record_banner.unreferenced")
      end
    end
  end
end
