require 'rails_helper'

describe ReferenceField::Field do
  let(:field){ described_class.new(view_context) }

  describe "#required=" do
    context "truthyな値を渡した場合" do
      before do
        field.required = "foo"
      end

      it "は、ラベル要素のHTMLクラスに'required'を追加すること" do
        expect(field.label.html_attributes[:class]).to include("required")
      end

      it "は、隠しフィールドに`required`属性を追加すること" do
        expect(field.foreign_type.html_attributes[:required]).to equal(true)
        expect(field.foreign_key.html_attributes[:required]).to equal(true)
      end

      it "は、イベントボタンのHTML属性を変化させないこと" do
        expect(field.button.html_attributes).to match(class: be_empty, data: be_empty)
      end
    end

    context "falseyな値を渡した場合" do
      before do
        field.label.html_attributes.html_attribute_merge!(class: "required")
        field.foreign_type.html_attributes.merge!(required: true)
        field.foreign_key.html_attributes.merge!(required: true)
        field.button.html_attributes.merge!(required: true)

        field.required = nil
      end

      it "は、ラベル要素のHTMLクラスから'required'を削除すること" do
        expect(field.label.html_attributes[:class]).not_to include("required")
      end

      it "は、隠しフィールドから`required`属性を削除すること" do
        expect(field.foreign_type.html_attributes).not_to have_key(:required)
        expect(field.foreign_key.html_attributes).not_to have_key(:required)
      end

      it "は、イベントボタンのHTML属性を変化させないこと" do
        expect(field.button.html_attributes).to match(class: be_empty, data: be_empty, required: true)
      end
    end
  end

  describe "#required?" do
    subject{ field.required? }

    context "隠しフィールドがどちらも`required`属性を持たない場合" do
      it{ is_expected.to equal(false) }
    end

    context "隠しフィールドのどちらかが`required`属性を持つ場合" do
      before do
        field.foreign_type.html_attributes.merge!(required: true)
      end

      it{ is_expected.to equal(true) }
    end

    context "隠しフィールドがどちらも`required`属性を持つ場合" do
      before do
        field.foreign_type.html_attributes.merge!(required: true)
        field.foreign_key.html_attributes.merge!(required: true)
      end

      it{ is_expected.to equal(true) }
    end
  end

  describe "#disabled=" do
    context "truthyな値を渡した場合" do
      before do
        field.disabled = "foo"
      end

      it "は、ラベル要素のHTMLクラスに'disabled'を追加すること" do
        expect(field.label.html_attributes[:class]).to include("disabled")
      end

      it "は、隠しフィールドに`disabled`属性を追加すること" do
        expect(field.foreign_type.html_attributes[:disabled]).to equal(true)
        expect(field.foreign_key.html_attributes[:disabled]).to equal(true)
      end

      it "は、イベントボタンに`disabled`属性を追加すること" do
        expect(field.button.html_attributes[:disabled]).to equal(true)
      end
    end

    context "falseyな値を渡した場合" do
      before do
        field.label.html_attributes.html_attribute_merge!(class: "disabled")
        field.foreign_type.html_attributes.merge!(disabled: true)
        field.foreign_key.html_attributes.merge!(disabled: true)
        field.button.html_attributes.merge!(disabled: true)

        field.disabled = nil
      end

      it "は、ラベル要素のHTMLクラスから'disabled'を削除すること" do
        expect(field.label.html_attributes[:class]).not_to include("disabled")
      end

      it "は、隠しフィールドから`disabled`属性を削除すること" do
        expect(field.foreign_type.html_attributes).not_to have_key(:disabled)
        expect(field.foreign_key.html_attributes).not_to have_key(:disabled)
      end

      it "は、イベントボタンから`disabled`属性を削除すること" do
        expect(field.button.html_attributes).not_to have_key(:disabled)
      end
    end
  end

  describe "#disabled?" do
    subject{ field.disabled? }

    context "隠しフィールドがどちらも`disabled`属性を持たない場合" do
      it{ is_expected.to equal(false) }
    end

    context "隠しフィールドのどちらかが`disabled`属性を持つ場合" do
      before do
        field.foreign_type.html_attributes.merge!(disabled: true)
      end

      it{ is_expected.to equal(true) }
    end

    context "隠しフィールドがどちらも`disabled`属性を持つ場合" do
      before do
        field.foreign_type.html_attributes.merge!(disabled: true)
        field.foreign_key.html_attributes.merge!(disabled: true)
      end

      it{ is_expected.to equal(true) }
    end
  end

  describe "#readonly=" do
    context "truthyな値を渡した場合" do
      before do
        field.readonly = "foo"
      end

      it "は、ラベル要素のHTMLクラスに'readonly'を追加すること" do
        expect(field.label.html_attributes[:class]).to include("readonly")
      end

      it "は、隠しフィールドに`readonly`属性を追加すること" do
        expect(field.foreign_type.html_attributes[:readonly]).to equal(true)
        expect(field.foreign_key.html_attributes[:readonly]).to equal(true)
      end

      it "は、イベントボタンに`readonly`属性を追加しないこと" do
        expect(field.button.html_attributes).not_to have_key(:readonly)
      end

      it "は、イベントボタンに`disabled`属性を追加すること" do
        expect(field.button.html_attributes[:disabled]).to equal(true)
      end
    end

    context "falseyな値を渡した場合" do
      before do
        field.label.html_attributes.html_attribute_merge!(class: "readonly")
        field.foreign_type.html_attributes.merge!(readonly: true)
        field.foreign_key.html_attributes.merge!(readonly: true)
        field.button.html_attributes.merge!(readonly: true, disabled: true)

        field.readonly = nil
      end

      it "は、ラベル要素のHTMLクラスから'readonly'を削除すること" do
        expect(field.label.html_attributes[:class]).not_to include("readonly")
      end

      it "は、隠しフィールドから`readonly`属性を削除すること" do
        expect(field.foreign_type.html_attributes).not_to have_key(:readonly)
        expect(field.foreign_key.html_attributes).not_to have_key(:readonly)
      end

      it "は、イベントボタンから`readonly`属性を削除しないこと" do
        expect(field.button.html_attributes[:readonly]).to equal(true)
      end

      it "は、イベントボタンから`disabled`属性を削除すること" do
        expect(field.button.html_attributes).not_to have_key(:disabled)
      end
    end
  end

  describe "#readonly?" do
    subject{ field.readonly? }

    context "隠しフィールドがどちらも`readonly`属性を持たない場合" do
      it{ is_expected.to equal(false) }
    end

    context "隠しフィールドのどちらかが`readonly`属性を持つ場合" do
      before do
        field.foreign_type.html_attributes.merge!(readonly: true)
      end

      it{ is_expected.to equal(true) }
    end

    context "隠しフィールドがどちらも`readonly`属性を持つ場合" do
      before do
        field.foreign_type.html_attributes.merge!(readonly: true)
        field.foreign_key.html_attributes.merge!(readonly: true)
      end

      it{ is_expected.to equal(true) }
    end
  end

  describe "#autofocus=" do
    context "truthyな値を渡した場合" do
      before do
        field.autofocus = "foo"
      end

      it "は、ラベル要素のHTMLクラスに'autofocus'を追加しないこと" do
        expect(field.label.html_attributes[:class]).not_to include("autofocus")
      end

      it "は、隠しフィールドに`autofocus`属性を追加しないこと" do
        expect(field.foreign_type.html_attributes).not_to have_key(:autofocus)
        expect(field.foreign_key.html_attributes).not_to have_key(:autofocus)
      end

      it "は、イベントボタンに`autofocus`属性を追加すること" do
        expect(field.button.html_attributes[:autofocus]).to equal(true)
      end
    end

    context "falseyな値を渡した場合" do
      before do
        field.label.html_attributes.html_attribute_merge!(class: "autofocus")
        field.foreign_type.html_attributes.merge!(autofocus: true)
        field.foreign_key.html_attributes.merge!(autofocus: true)
        field.button.html_attributes.merge!(autofocus: true, disabled: true)

        field.autofocus = nil
      end

      it "は、ラベル要素のHTMLクラスから'autofocus'を削除しないこと" do
        expect(field.label.html_attributes[:class]).to include("autofocus")
      end

      it "は、隠しフィールドから`autofocus`属性を削除しないこと" do
        expect(field.foreign_type.html_attributes[:autofocus]).to equal(true)
        expect(field.foreign_key.html_attributes[:autofocus]).to equal(true)
      end

      it "は、イベントボタンから`autofocus`属性を削除すること" do
        expect(field.button.html_attributes).not_to have_key(:autofocus)
      end
    end
  end

  describe "#autofocus?" do
    subject{ field.autofocus? }

    context "イベントボタンが`autofocus`属性を持たない場合" do
      it{ is_expected.to equal(false) }
    end

    context "イベントボタンが`autofocus`属性を持つ場合" do
      before do
        field.button.html_attributes.merge!(autofocus: true)
      end

      it{ is_expected.to equal(true) }
    end
  end

  describe "#polymorphic=" do
    before do
      field.foreign_type.html_attributes[:name] = "my[name]"
    end

    context "`false`を渡した場合" do
      before do
        field.polymorphic = false
      end

      it "は、`#foreign_type`のHTML属性`name`を削除すること" do
        expect(field.foreign_type.html_attributes).not_to have_key(:name)
      end
    end

    context "`true`を渡した場合" do
      before do
        field.polymorphic = true
      end

      it "は、`#foreign_type`のHTML属性`name`を削除しないこと" do
        expect(field.foreign_type.html_attributes[:name]).to eq("my[name]")
      end
    end
  end

  describe "#polymorphic?" do
    subject{ field.polymorphic? }

    context "`#foreign_type`のHTML属性`name`が文字列の値を持つ場合" do
      before do
        field.foreign_type.html_attributes[:name] = "my[name]"
      end

      it{ is_expected.to equal(true) }
    end

    context "`#foreign_type`のHTML属性`name`が空文字の値を持つ場合" do
      before do
        field.foreign_type.html_attributes[:name] = ""
      end

      it{ is_expected.to equal(true) }
    end

    context "`#foreign_type`のHTML属性`name`の値が`nil`である場合" do
      before do
        field.foreign_type.html_attributes[:name] = nil
      end

      it{ is_expected.to equal(false) }
    end

    context "`#foreign_type`がHTML属性`name`を持たない場合" do
      it{ is_expected.to equal(false) }
    end
  end

  describe "#referenced?" do
    subject{ field.referenced? }

    context "隠しフィールドがどちらも`value`属性を持たない場合" do
      it{ is_expected.to equal(false) }
    end

    context "隠しフィールドのどちらかが`value`属性を持つ場合" do
      before do
        field.foreign_type.html_attributes.merge!(value: "my-value")
      end

      it{ is_expected.to equal(false) }
    end

    context "隠しフィールドがどちらも`value`属性を持つ場合" do
      before do
        field.foreign_type.html_attributes.merge!(value: "my-value")
        field.foreign_key.html_attributes.merge!(value: "my-value")
      end

      it{ is_expected.to equal(true) }
    end
  end

  describe "#id_prefix=" do
    before do
      field.id_prefix = "foo"
    end

    it "は、ラベル要素のHTML属性`id`をセットすること" do
      expect(field.label.html_attributes[:id]).to eq("foo-reference_field")
    end

    it "は、ラベル要素のHTML属性`for`をセットすること" do
      expect(field.label.html_attributes[:for]).to eq("foo-find")
    end

    it "は、隠し入力欄の`id`をセットすること" do
      expect(field.foreign_type.html_attributes[:id]).to eq("foo-foreign_type")
      expect(field.foreign_key.html_attributes[:id]).to eq("foo-foreign_key")
    end

    it "は、イベントボタン要素のHTML属性`id`をセットすること" do
      expect(field.button.html_attributes[:id]).to eq("foo-find")
    end
  end

  describe "#for=" do
    before do
      field.for = "foo"
    end

    it "は、ラベル要素のHTML属性`for`をセットしないこと" do
      expect(field.label.html_attributes).not_to have_key(:for)
    end

    it "は、イベントボタン要素のHTML属性`for`をセットすること" do
      expect(field.button.html_attributes[:for]).to eq("foo-trigger")
    end
  end

  describe "#form=" do
    before do
      field.form = "foo"
    end

    it "は、隠しフィールドに`form`属性を追加すること" do
      expect(field.foreign_type.html_attributes[:form]).to eq("foo")
      expect(field.foreign_key.html_attributes[:form]).to eq("foo")
    end
  end

  describe "#target=" do
    before do
      field.foreign_type.value = "foo"
      field.foreign_key.value = "bar"
      field.instance_variable_set(:@target, "baz")

      field.target = arg
    end

    shared_examples_for "save argument" do
      it "は、引数を保存すること" do
        expect(field.target).to equal(arg)
      end
    end

    context "保存済みレコードを渡した場合" do
      let(:arg){ create(:person) }

      it "は、隠し入力欄の`value`属性値を引数のモデル名・レコードIDで書き換えること" do
        expect(field.foreign_type.html_attributes[:value]).to eq("Person")
        expect(field.foreign_key.html_attributes[:value]).to eq(arg.id)
      end

      it_behaves_like "save argument"
    end

    context "未保存のレコードを渡した場合" do
      let(:arg){ build(:person) }

      it "は、隠し入力欄の`value`属性値を消去すること" do
        expect(field.foreign_type.html_attributes[:value]).to eq("Person")
        expect(field.foreign_key.html_attributes[:value]).to be_nil
      end

      it_behaves_like "save argument"
    end

    context "`nil`を渡した場合" do
      let(:arg){ nil }

      it "は、隠し入力欄の`value`属性値を消去すること" do
        expect(field.foreign_type.html_attributes[:value]).to be_nil
        expect(field.foreign_key.html_attributes[:value]).to be_nil
      end

      it_behaves_like "save argument"
    end
  end

  describe "#render" do
    subject{ field.render }

    it "は、参照フィールド要素を返すこと" do
      is_expected.to have_tag("label.reference_field-field:not([id]):not([for])"){
        with_tag("input.foreign_type:not([id]):not([name]):not([value])", with: {type: "hidden"})
        with_tag("input.foreign_key:not([id]):not([name]):not([value])", with: {type: "hidden"})
        with_tag(".event_button:not([id]):not([for])", with: {"data-event" => "reference_field:find"}, text: "探")
        with_tag(".banner_container")
      }
    end

    context "`required`に`true`をセットしていた場合" do
      before do
        field.required = true
      end

      it "は、必須参照フィールドを返すこと" do
        is_expected.to have_tag("label.reference_field-field:not([id]):not([for]).required"){
          with_tag("input.foreign_type:not([id]):not([name]):not([value])", with: {type: "hidden", required: "required"})
          with_tag("input.foreign_key:not([id]):not([name]):not([value])", with: {type: "hidden", required: "required"})
          with_tag(".event_button:not([id]):not([for]):not(required)", with: {"data-event" => "reference_field:find"}, text: "探")
          with_tag(".banner_container")
        }
      end
    end

    context "`disabled`に`true`をセットしていた場合" do
      before do
        field.disabled = true
      end

      it "は、無効状態の参照フィールドを返すこと" do
        is_expected.to have_tag("label.reference_field-field:not([id]):not([for]).disabled"){
          with_tag("input.foreign_type:not([id]):not([name]):not([value])", with: {type: "hidden", disabled: "disabled"})
          with_tag("input.foreign_key:not([id]):not([name]):not([value])", with: {type: "hidden", disabled: "disabled"})
          with_tag(".event_button:not([id]):not([for])", with: {"data-event" => "reference_field:find", disabled: "disabled"}, text: "探")
          with_tag(".banner_container")
        }
      end
    end

    context "`readonly`に`true`をセットしていた場合" do
      before do
        field.readonly = true
      end

      it "は、読み取り専用の参照フィールドを返すこと" do
        is_expected.to have_tag("label.reference_field-field:not([id]):not([for]).readonly"){
          with_tag("input.foreign_type:not([id]):not([name]):not([value])", with: {type: "hidden", readonly: "readonly"})
          with_tag("input.foreign_key:not([id]):not([name]):not([value])", with: {type: "hidden", readonly: "readonly"})
          with_tag(".event_button:not([id]):not([for]):not([readonly])", with: {"data-event" => "reference_field:find", disabled: "disabled"}, text: "探")
          with_tag(".banner_container")
        }
      end
    end

    context "`polymorphic`に" do
      before do
        field.foreign_type.html_attributes.merge!(id: "foo_bar_baz_type", name: "foo[bar][baz_type]")
        field.foreign_key.html_attributes.merge!(id: "foo_bar_baz_id", name: "foo[bar][baz_id]")
      end

      context "`true`をセットしていた場合" do
        before do
          field.polymorphic = true
        end

        it "は、ポリモーフィック対応の参照フィールド要素を返すこと" do
          is_expected.to have_tag("label.reference_field-field:not([id]):not([for])"){
            with_tag("input.foreign_type#foo_bar_baz_type:not([value])", with: {type: "hidden", name: "foo[bar][baz_type]"})
            with_tag("input.foreign_key#foo_bar_baz_id:not([value])", with: {type: "hidden", name: "foo[bar][baz_id]"})
            with_tag(".event_button:not([id]):not([for])", with: {"data-event" => "reference_field:find"}, text: "探")
            with_tag(".banner_container")
          }
        end
      end

      context "`false`をセットしていた場合" do
        before do
          field.polymorphic = false
        end

        it "は、ポリモーフィック非対応の参照フィールド要素を返すこと" do
          is_expected.to have_tag("label.reference_field-field:not([id]):not([for])"){
            with_tag("input.foreign_type#foo_bar_baz_type:not([name]):not([value])", with: {type: "hidden"})
            with_tag("input.foreign_key#foo_bar_baz_id:not([value])", with: {type: "hidden", name: "foo[bar][baz_id]"})
            with_tag(".event_button:not([id]):not([for])", with: {"data-event" => "reference_field:find"}, text: "探")
            with_tag(".banner_container")
          }
        end
      end
    end

    context "`form`をセットしていた場合" do
      before do
        field.form = "my-form"
      end

      it "は、フォームを指定した参照フィールドを返すこと" do
        is_expected.to have_tag("label.reference_field-field:not([id]):not([for]):not(.form)"){
          with_tag("input.foreign_type:not([id]):not([name]):not([value])", with: {type: "hidden", form: "my-form"})
          with_tag("input.foreign_key:not([id]):not([name]):not([value])", with: {type: "hidden", form: "my-form"})
          with_tag(".event_button:not([id]):not([for]):not([form])", with: {"data-event" => "reference_field:find"}, text: "探")
          with_tag(".banner_container")
        }
      end
    end

    context "`id_prefix`をセットしていた場合" do
      before do
        field.id_prefix = "my-id"
      end

      it "は、自身とイベントボタン要素が`id`属性を持つ参照フィールドを返すこと" do
        is_expected.to have_tag("label.reference_field-field#my-id-reference_field", with: {for: "my-id-find"}){
          with_tag("input.foreign_type#my-id-foreign_type:not([name]):not([value])", with: {type: "hidden"})
          with_tag("input.foreign_key#my-id-foreign_key:not([name]):not([value])", with: {type: "hidden"})
          with_tag(".event_button:not([for])#my-id-find", with: {"data-event" => "reference_field:find"}, text: "探")
          with_tag(".banner_container")
        }
      end
    end

    context "`for`をセットしていた場合" do
      before do
        field.for = "my-for"
      end

      it "は、イベントボタン要素が`for`属性を持つ参照フィールド要素を返すこと" do
        is_expected.to have_tag("label.reference_field-field:not([id]):not([for])"){
          with_tag("input.foreign_type:not([id]):not([name]):not([value]):not([for])", with: {type: "hidden"})
          with_tag("input.foreign_key:not([id]):not([name]):not([value]):not([for])", with: {type: "hidden"})
          with_tag(".event_button:not([id])", with: {"data-event" => "reference_field:find", for: "my-for-trigger"}, text: "探")
          with_tag(".banner_container")
        }
      end
    end
  end
end
