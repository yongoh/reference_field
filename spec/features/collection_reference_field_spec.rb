require 'rails_helper'

feature "複数関連対応参照フィールド", js: true do
  before do
    create_list(:person, 3)
    visit edit_person_path(person, skip_animation: true)
  end

  let(:person){ create(:person, :with_friends) }

  subject do
    proc{ click_button "Update 人物" }
  end

  shared_examples_for "successfully updated" do
    scenario "は、全体のレコード数を変化させないこと" do
      expect(subject).not_to change{ Person.count }
    end

    scenario "は、更新成功の通知を表示すること" do
      subject.call
      expect(page).to have_selector("#notice", text: "Person was successfully updated.")
    end
  end

  shared_examples_for "did not change old target" do
    scenario "は、旧関連先を変化させないこと" do
      old_friend = person.friends[1]
      expect(subject).not_to change{ old_friend.reload.attributes }
    end
  end

  context "関連先を一部削除して送信した場合" do
    before do
      within ".action_field-member:nth-of-type(2)" do
        click_button "削除"
      end
      sleep 0.5
    end

    scenario "は、関連先を減らすこと" do
      old_friends = person.friends
      expect(subject).to change{ person.reload.friends.size }.by(-1)
      expect(person.reload.friends).to match([old_friends[0], old_friends[2]])
    end

    it_behaves_like "did not change old target"
    it_behaves_like "successfully updated"
  end

  context "関連先を一部変更して送信した場合" do
    before do
      within ".action_field-member:nth-of-type(2)" do
        click_button "探"
      end
      find(".page-tab:nth-of-type(1)").click
      within ".reference_field-finder li:first-of-type" do
        click_button "決定"
      end
    end

    scenario "は、関連先の一部を入れ替えること" do
      old_friends = person.friends
      expect(subject).to change{ person.reload.friends }.to(match([old_friends[0], old_friends[2], Person.first]))
    end

    it_behaves_like "did not change old target"
    it_behaves_like "successfully updated"
  end

  context "関連先を追加して送信した場合" do
    before do
      click_button "追加"
      sleep 0.5
      within ".action_field-member:last-of-type" do
        click_button "探"
      end
      find(".page-tab:nth-of-type(1)").click
      within ".reference_field-finder li:first-of-type" do
        click_button "決定"
      end
    end

    scenario "は、関連先を追加すること" do
      expect(subject).to change{ person.reload.friends.size }.by(1)
    end

    it_behaves_like "successfully updated"
  end
end
