require 'rails_helper'

feature "ポリモーフィック非対応", js: true do
  before do
    create_list(:person, 5)
    visit edit_person_path(person, skip_animation: true)
  end

  let(:person){ create(:person, :with_parent) }

  subject do
    proc{ click_button "Update 人物" }
  end

  shared_examples_for "successfully updated" do
    scenario "は、更新成功の通知を表示すること" do
      subject.call
      expect(page).to have_selector("#notice", text: "Person was successfully updated.")
    end
  end

  context "関連'parent'の参照フィールドから参照ファインダーを開き、" do
    before do
      click_button "探"
      find(".page-tab:nth-of-type(1)").click
    end

    context "参照先を決定して送信した場合" do
      before do
        within ".reference_field-finder li:first-of-type" do
          click_button "決定"
        end
      end

      let(:new_parent){ Person.first }

      scenario "は、`parent_id`の値を更新すること" do
        expect(subject).to change{ person.reload.parent_id }.from(person.parent.id).to(new_parent.id)
      end

      scenario "は、参照先を変更すること" do
        expect(subject).to change{ person.reload.parent }.from(person.parent).to(new_parent)
      end

      it_behaves_like "successfully updated"
    end

    context "「参照解除」ボタンをクリックして送信した場合" do
      before do
        click_button "参照解除"
      end

      scenario "は、`parent_id`の値を`nil`にすること" do
        expect(subject).to change{ person.reload.parent_id }.from(person.parent.id).to(nil)
      end

      scenario "は、参照を解除すること" do
        expect(subject).to change{ person.reload.parent }.from(person.parent).to(nil)
      end

      it_behaves_like "successfully updated"
    end
  end
end
