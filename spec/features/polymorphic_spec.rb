require 'rails_helper'

feature "ポリモーフィック対応", js: true do
  before do
    create_list(:person, 5)
    create_list(:company, 5)
    visit edit_profile_path(profile, skip_animation: true)
  end

  let(:profile){ create(:profile) }

  subject do
    proc{ click_button "Update Profile" }
  end

  shared_examples_for "successfully updated" do
    scenario "は、更新成功の通知を表示すること" do
      subject.call
      expect(page).to have_selector("#notice", text: "Profile was successfully updated.")
    end
  end

  context "関連'profilable'の参照フィールドから参照ファインダーを開き、" do
    before do
      click_button "探"
      find(".page-tab:nth-of-type(4)").click
    end

    context "参照先を決定して送信した場合" do
      before do
        within ".reference_field-finder li:first-of-type" do
          click_button "決定"
        end
      end

      let(:new_profilable){ Company.first }

      scenario "は、`profilable_type`の値を更新すること" do
        expect(subject).to change{ profile.reload.profilable_type }.from("Person").to("Company")
      end

      scenario "は、`profilable_id`の値を更新すること" do
        expect(subject).to change{ profile.reload.profilable_id }.from(profile.profilable.id).to(new_profilable.id)
      end

      scenario "は、参照先を変更すること" do
        expect(subject).to change{ profile.reload.profilable }.from(profile.profilable).to(new_profilable)
      end

      it_behaves_like "successfully updated"
    end

    context "「参照解除」ボタンをクリックして送信した場合" do
      before do
        click_button "参照解除"
      end

      # NOTE: ポリモーフィック関連は自動で必須になるのでテストできない
    end
  end
end
