require 'rails_helper'

describe ReferenceField::ApplicationHelper do
  let(:banner_proc){
    proc do
      h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
      h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
    end
  }

  describe "#reference_field" do
    subject{ h.reference_field("foo[bar]", "baz") }

    it "は、参照フィールド要素を返すこと" do
      is_expected.to have_tag("label.reference_field-field#foo_bar_baz-reference_field", with: {for: "foo_bar_baz-find"}){
        with_tag("input.foreign_type#foo_bar_baz-foreign_type:not([value]):not([form]):not([name])", with: {type: "hidden"})
        with_tag("input.foreign_key#foo_bar_baz-foreign_key:not([value]):not([form])", with: {type: "hidden", name: "foo[bar][baz_id]"})
        with_tag(".event_button#foo_bar_baz-find", with: {"data-event" => "reference_field:find", for: "baz-trigger"}, text: "探")
        with_tag(".banner_container")
      }
    end

    it "は、ブロック引数に参照フィールドインスタンスを渡すこと" do
      expect{|block| h.reference_field("foo[bar]", "baz", &block) }.to yield_with_args(be_a(ReferenceField::Field))
    end

    describe "第3引数" do
      shared_examples_for "unreferenced banner" do
        it "は、未参照バナー要素を含む参照フィールド要素を返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".banner_container"){
              with_tag(".record_banner.unreferenced")
            }
          }
        end
      end

      shared_examples_for "banner brock" do
        subject{ h.reference_field("foo[bar]", "baz", person, banner_proc: banner_proc) }

        it "は、手続き引数に参照先レコードを渡すこと" do
          expect{|block| h.reference_field("foo[bar]", "baz", person, banner_proc: block) }.to yield_with_args(equal(person))
        end

        it "は、手続き内で作成した要素を含む参照フィールド要素を返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".banner_container"){
              with_tag("div#shobon", text: "(´･ω･｀)")
              with_tag("span.boon", text: "（＾ω＾）")
            }
          }
        end
      end

      context "に`nil`を渡した場合 (default)" do
        it "は、`value`属性を持たない隠し入力欄を含む参照フィールド要素を返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_type:not([value])")
            with_tag(".foreign_key:not([value])")
          }
        end

        context "オプション`:banner_proc`に`nil`を渡した場合 (default)" do
          it_behaves_like "unreferenced banner"
        end

        context "オプション`:banner_proc`に手続きを渡した場合" do
          subject{ h.reference_field("foo[bar]", "baz", banner_proc: banner_proc) }

          it "は、渡した手続きを実行しないこと" do
            expect{|block| h.reference_field("foo[bar]", "baz", banner_proc: block) }.to yield_successive_args
          end

          it_behaves_like "unreferenced banner"
        end
      end

      context "に未保存のレコードを渡した場合" do
        subject{ h.reference_field("foo[bar]", "baz", person) }
        let(:person){ build(:person) }

        it "は、`value`属性にモデルの情報だけを持つ隠し入力欄を含む参照フィールド要素を返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_type", with: {value: "Person"})
            with_tag(".foreign_key:not([value])")
          }
        end

        context "オプション`:banner_proc`に`nil`を渡した場合 (default)" do
          it "は、参照バナー要素を含む参照フィールド要素を返すこと" do
            is_expected.to have_tag(".reference_field-field"){
              with_tag(".banner_container"){
                with_tag(".record_banner.referenced:not([data-record-id])", with: {"data-model" => "Person"})
              }
            }
          end
        end

        context "オプション`:banner_proc`に手続きを渡した場合" do
          it_behaves_like "banner brock"
        end
      end

      context "に保存済みのレコードを渡した場合" do
        subject{ h.reference_field("foo[bar]", "baz", person) }
        let(:person){ create(:person) }

        it "は、`value`属性にレコードの情報を持つ隠し入力欄を含む参照フィールド要素を返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_type", with: {value: "Person"})
            with_tag(".foreign_key", with: {value: person.id})
          }
        end

        context "オプション`:banner_proc`に`nil`を渡した場合 (default)" do
          it "は、参照バナー要素を含む参照フィールド要素を返すこと" do
            is_expected.to have_tag(".reference_field-field"){
              with_tag(".banner_container"){
                with_tag(".record_banner.referenced", with: {"data-model" => "Person", "data-record-id" => person.id})
              }
            }
          end
        end

        context "オプション`:banner_proc`に手続きを渡した場合" do
          it_behaves_like "banner brock"
        end
      end
    end

    context "オプション`:polymorphic`に" do
      context "`false`を渡した場合 (default)" do
        it "は、渡した値を`for`属性値に持つイベントボタン要素を含む参照フィールド要素を返すこと" do
          is_expected.to have_tag(".reference_field-field:not([polymorphic])"){
            with_tag(".foreign_type:not([name])")
            with_tag(".foreign_key", with: {name: "foo[bar][baz_id]"})
          }
        end
      end

      context "`true`を渡した場合" do
        subject{ h.reference_field("foo[bar]", "baz", polymorphic: true) }

        it "は、渡した値を`for`属性値に持つイベントボタン要素を含む参照フィールド要素を返すこと" do
          is_expected.to have_tag(".reference_field-field:not([polymorphic])", with: {for: "foo_bar_baz-find"}){
            with_tag(".foreign_type", with: {name: "foo[bar][baz_type]"})
            with_tag(".foreign_key", with: {name: "foo[bar][baz_id]"})
          }
        end
      end
    end

    context "オプション`:for`を渡した場合" do
      subject{ h.reference_field("foo[bar]", "baz", for: "hoge") }

      it "は、渡した値を`for`属性値に持つイベントボタン要素を含む参照フィールド要素を返すこと" do
        is_expected.to have_tag(".reference_field-field", with: {for: "foo_bar_baz-find"}){
          with_tag(".event_button", with: {"data-event" => "reference_field:find", for: "hoge-trigger"})
        }
      end
    end

    context "オプション`:form`を渡した場合" do
      subject{ h.reference_field("foo[bar]", "baz", form: "hoge") }

      it "は、渡した値を`for`属性値に持つイベントボタン要素を含む参照フィールド要素を返すこと" do
        is_expected.to have_tag(".reference_field-field:not([form])"){
          with_tag(".foreign_type", with: {form: "hoge"})
          with_tag(".foreign_key", with: {form: "hoge"})
          with_tag(".event_button:not([form])", with: {"data-event" => "reference_field:find"})
        }
      end
    end
  end

  describe "#collection_reference_field" do
    subject do
      h.collection_reference_field("foo[bar]", "baz", people)
    end

    let(:people){ create_list(:person, 3) }

    it "は、参照フィールドを含むActionFieldコンテナを返すこと" do
      is_expected.to have_tag(
        ".action_field-container#foo_bar_baz-action_field-container",
        with: {"data-object-name" => "foo[bar][baz]"},
      ){
        people.each_with_index do |person, i|
          with_tag(
            ".action_field-member#foo_bar_baz_#{i}-action_field-member",
            with: {"data-object-name" => "foo[bar][baz][#{i}]"},
            count: 1,
          ){
            with_tag("label.reference_field-field#foo_bar_baz_#{i}-reference_field", with: {for: "foo_bar_baz_#{i}-find"}){
              with_tag("input.foreign_type#foo_bar_baz_#{i}-foreign_type:not([name])", with: {type: "hidden"})
              with_tag("input.foreign_key#foo_bar_baz_#{i}-foreign_key", with: {type: "hidden", name: "foo[bar][baz_ids][]", value: person.id.to_s})
              with_tag(".event_button#foo_bar_baz_#{i}-find", with: {"data-event" => "reference_field:find"})
              with_tag(".banner_container"){
                with_tag(".record_banner.referenced", text: "#<人物 id: #{person.id}>")
              }
            }

            with_tag(".event_button", with: {"data-event" => "action_field:remove", for: "foo_bar_baz_#{i}-action_field-member"})
          }
        end

        with_tag(".action_field-member", count: people.size)

        without_tag("input[name$='[id]']")
        without_tag("input[name$='[_destroy]']")
      }
    end

    it "は、複数コンテナ用のイベントボタンを含むメニュー要素を生成すること" do
      is_expected.to have_tag(".action_field-menu#foo_bar_baz-action_field-menu"){
        with_tag(".event_button[data-template]", with: {"data-event" => "action_field:add", for: "foo_bar_baz-action_field-container"})
        with_tag(".event_button", with: {"data-event" => "action_field:clear", for: "foo_bar_baz-action_field-container"})
      }
    end

    it "は、ブロック引数に参照フィールドインスタンスを渡すこと" do
      expect{|block| h.collection_reference_field("foo[bar]", "baz", people, &block) }.to yield_successive_args(
        *people.map{|friend|
          be_a(ReferenceField::Field)
        },
        be_a(ReferenceField::Field)
      )
    end

    describe "イベントボタンのテンプレート" do
      let(:template_member){ subject.slice(/\bdata-template="(.+?)"/, 1).gsub("&quot;", '"') }

      it "は、未参照状態の参照フィールドを含むメンバー要素であること" do
        expect(template_member).to have_tag(".action_field-member", count: 1){
          with_tag(".reference_field-field"){
            with_tag(".foreign_type:not([name]):not([value]):not([pattern])", with: {type: "hidden"})
            with_tag(".foreign_key:not([value])", with: {type: "hidden", name: "foo[bar][baz_ids][]"})
            with_tag(".event_button", with: {"data-event" => "reference_field:find"})
            with_tag(".banner_container"){
              with_tag(".record_banner.unreferenced")
            }
          }

          with_tag(".event_button", with: {"data-event" => "action_field:remove"})
        }
      end
    end

    describe "第3引数" do
      context "を渡さない場合 (default)" do
        subject do
          h.collection_reference_field("foo[bar]", "baz")
        end

        it "は、空のActionFieldコンテナを返すこと" do
          is_expected.to have_tag(".action_field-container:empty")
        end
      end

      context "にレコードの配列を渡した場合" do
        it "は、渡したレコード群を参照した状態の参照フィールド群を含むActionFieldコンテナを返すこと" do
          is_expected.to have_tag(".action_field-container"){
            people.each do |person|
              with_tag(".action_field-member .reference_field-field"){
                with_hidden_field("foo[bar][baz_ids][]", person.id)
              }
            end
          }
        end
      end
    end

    context "オプション`:banner_proc`に手続きを渡した場合" do
      subject{ h.collection_reference_field("foo[bar]", "baz", people, banner_proc: banner_proc) }

      it "は、手続き引数に参照先レコードを渡すこと" do
        expect{|block| h.collection_reference_field("foo[bar]", "baz", people, banner_proc: block) }.to yield_successive_args(
          *people.map{|person|
            equal(person)
          },
        )
      end

      it "は、手続き内で作成した要素を含む参照フィールド要素を返すこと" do
        is_expected.to have_tag(".action_field-container"){
          people.each_with_index do |person, i|
            with_tag(".action_field-member"){
              with_tag(".reference_field-field"){
                with_tag(".banner_container"){
                  with_tag("div#shobon", text: "(´･ω･｀)", count: people.size)
                  with_tag("span.boon", text: "（＾ω＾）", count: people.size)
                }
              }
            }
          end
        }
      end
    end
  end

  describe "#choice_button" do
    context "第1引数を渡さない場合" do
      it "は、「決定」イベントボタンを返すこと" do
        expect(h.choice_button).to have_tag(
          "button.event_button:not([data-model]):not([data-recod-id])",
          with: {"data-event" => "reference_field:choice"},
          text: "決定",
        )
      end
    end

    context "第1引数にレコードを渡した場合" do
      let(:person){ create(:person) }

      it "は、渡したレコードに対応する「決定」イベントボタンを返すこと" do
        expect(h.choice_button(person)).to have_tag(
          "button.event_button",
          with: {"data-event" => "reference_field:choice", "data-model" => "Person", "data-record-id" => person.id.to_s},
          text: "決定",
        )
      end
    end

    context "第1引数にレコード以外を渡した場合" do
      it "は、渡したものを内容とした「決定」イベントボタンを返すこと" do
        expect(h.choice_button("(´･ω･｀)")).to have_tag(
          "button.event_button:not([data-model]):not([data-recod-id])",
          with: {"data-event" => "reference_field:choice"},
          text: "(´･ω･｀)",
        )
      end
    end
  end

  describe "#reference_finder" do
    subject do
      h.reference_finder("hoge[piyo]") do |x|
        concat(x.finder("foo"){ "foofoofoofoofoo" })
        concat(x.finder("bar"){ "barbarbarbarbar" })
        concat(x.finder("baz"))
      end
    end

    it "は、参照ファインダーを含む参照ファインダーコンテナを返すこと" do
      is_expected.to have_tag("input.modal_window-trigger#hoge_piyo-trigger", with: {type: "checkbox"})
      is_expected.to have_tag(".modal_window-window.reference_field-finders#hoge_piyo-window"){
        with_tag(".event_button", with: {"data-event" => "reference_field:close", for: "hoge_piyo"})
        with_tag(".event_button[data-banner]", with: {"data-event" => "reference_field:dereference"}, text: "参照解除")
        with_tag(".radio_button_tab#hoge_piyo_tab"){

          with_tag("input.reference_field-finder", with: {type: "radio", name: "hoge[piyo][tab]"}, count: 3)
          with_tag("input.reference_field-finder", with: {"data-finder" => "foo"})
          with_tag("input.reference_field-finder", with: {"data-finder" => "bar"})
          with_tag("input.reference_field-finder", with: {"data-finder" => "baz"})

          with_tag(".page-tab.reference_field-finder", count: 3)
          with_tag(".page-tab.reference_field-finder", with: {"data-finder" => "foo"}, text: "ふー")
          with_tag(".page-tab.reference_field-finder", with: {"data-finder" => "bar"}, text: "Bar")
          with_tag(".page-tab.reference_field-finder", with: {"data-finder" => "baz"}, text: "Baz")

          with_tag(".page-content.reference_field-finder", count: 3)
          with_tag(".page-content.reference_field-finder", with: {"data-finder" => "foo"}, text: "foofoofoofoofoo")
          with_tag(".page-content.reference_field-finder", with: {"data-finder" => "bar"}, text: "barbarbarbarbar")
          with_tag(".page-content.reference_field-finder", with: {"data-finder" => "baz"}){
            with_tag("h2", text: "Baz")
            with_tag("p", text: "application/finders/baz")
          }
        }
      }
    end
  end

  describe "#reference_label" do
    it "は、「探」イベントボタンに紐付けられたラベル要素を返すこと" do
      expect(h.reference_label("foo[bar]", "baz")).to have_tag("label", with: {for: "foo_bar_baz-find"}, text: "Baz")
    end
  end
end
