describe "ReferenceField.Field", ->
  beforeEach ->
    loadFixtures("reference_field/animal.html")
    @field = new ReferenceField.Field(document.getElementById("animal_parent_id_reference_field"))

  describe "#validateAllowedTarget", ->
    beforeEach ->
      @field.element.dataset.prohibitedTargets = "Person:1"

    describe "参照禁止のモデル名＆レコードIDを渡した場合", ->
      it "は、バリデーションエラーを発生させること", ->
        expect(=> @field.validateAllowedTarget(type: "Person", id: "1")).toThrow(new ReferenceField.Field.ValidationError("This record is not allowed to be referenced."))

    describe "参照禁止ではないモデル名を渡した場合", ->
      it "は、例外を発生させないこと", ->
        expect(=> @field.validateAllowedTarget(type: "Animal", id: "1")).not.toThrow()

    describe "参照禁止ではないレコードIDを渡した場合", ->
      it "は、例外を発生させないこと", ->
        expect(=> @field.validateAllowedTarget(type: "Person", id: "2")).not.toThrow()

    describe "参照禁止ではないモデル名＆レコードIDを渡した場合", ->
      it "は、例外を発生させないこと", ->
        expect(=> @field.validateAllowedTarget(type: "Animal", id: "2")).not.toThrow()

  describe "#disable", ->
    describe "`true`を渡した場合 (default)", ->
      beforeEach ->
        @field.disable(true)

      it "は、参照フィールドを無効状態にすること", ->
        expect(EventButton.SELECTORS.button("reference_field:find")).toBeDisabled()
        expect("#animal_parent_id_reference_field > #{ReferenceField.Field.SELECTORS.FOREIGN_TYPE}").toBeDisabled()
        expect("#animal_parent_id").toBeDisabled()
        expect(@field.element).toHaveClass("disabled")

      describe "「探」ボタンをクリックした場合", ->
        beforeEach ->
          appendLoadFixtures("finder_container.html")
          appendLoadStyleFixtures("modal_window.css")
          @findButton = @field.element.querySelector(EventButton.SELECTORS.button("reference_field:find"))
          document.getElementById("reference_finder-trigger").checked = false

        it "は、ボタンが含まれる参照フィールドに指標をつけないこと", ->
          @findButton.click()
          expect(@field.element).not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)

        it "は、参照ファインダーコンテナを表示しないこと", ->
          @findButton.click()
          expect("#reference_finder-overlay").toBeHidden()

    describe "`false`を渡した場合", ->
      beforeEach ->
        @field.disable(false)

      it "は、参照フィールドを有効状態にすること", ->
        expect(EventButton.SELECTORS.button("reference_field:find")).not.toBeDisabled()
        expect("#animal_parent_id_reference_field > #{ReferenceField.Field.SELECTORS.FOREIGN_TYPE}").not.toBeDisabled()
        expect("#animal_parent_id").not.toBeDisabled()
        expect(@field.element).not.toHaveClass("disabled")

  describe "#current", ->
    describe "`true`を渡した場合 (true)", ->
      it "は、自身の要素に指標をつけること", ->
        @field.current(true)
        expect(@field.element).toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)

    describe "`false`を渡した場合", ->
      beforeEach ->
        @field.element.classList.add(ReferenceField.Field.HTML_CLASSES.CURRENT)

      it "は、自身の要素から指標を外すこと", ->
        @field.current(false)
        expect(@field.element).not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)

  describe "#focus", ->
    beforeEach ->
      @button = @field.element.querySelector(EventButton.SELECTORS.button("reference_field:find"))

    describe "`true`を渡した場合 (default)", ->
      it "は、「探」イベントボタンにフォーカスすること", ->
        @field.focus(true)
        expect(@button).toBeFocused()

    describe "`false`を渡した場合", ->
      beforeEach ->
        @button.focus()

      it "は、「探」イベントボタンのフォーカスをはずすこと", ->
        @field.focus(false)
        expect(@button).not.toBeFocused()
