describe "ReferenceField.Finder", ->
  beforeEach ->
    loadFixtures("finder_container.html")
    @finder = new ReferenceField.Finder(document.querySelector("#reference_finder_tab-foo-content"))
    appendLoadFixtures("animal_list.html")
    @finder.element.appendChild(document.getElementById("animal_list"))

  describe "#highlightCurrentRecord", ->
    it "は、指定したレコードのバナー要素をハイライトすること", ->
      @finder.highlightCurrentRecord({model: "Animal", recordId: "2"})
      expect(".record_banner[data-model='Animal'][data-record-id='1']").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect(".record_banner[data-model='Animal'][data-record-id='2']").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect(".record_banner[data-model='Animal'][data-record-id='3']").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)

  describe "#disableChoiceButton", ->
    it "は、指定したレコードに対応する「決定」イベントボタンを無効化すること", ->
      @finder.disableChoiceButton({model: "Animal", recordId: "2"})
      expect("#{EventButton.SELECTORS.button("reference_field:choice")}[data-model='Animal'][data-record-id='1']").not.toBeDisabled()
      expect("#{EventButton.SELECTORS.button("reference_field:choice")}[data-model='Animal'][data-record-id='2']").toBeDisabled()
      expect("#{EventButton.SELECTORS.button("reference_field:choice")}[data-model='Animal'][data-record-id='3']").not.toBeDisabled()

describe "ReferenceField.Finder::build", ->
  beforeEach ->
    loadFixtures("finder_container.html")
    @finderElm = document.querySelector("#reference_finder_tab-foo-content")

  it "は、参照ファインダーオブジェクトを返すこと", ->
    field = ReferenceField.Finder.build(@finderElm)
    expect(field).toEqual(jasmine.any(ReferenceField.Finder))
    expect(field.element).toEqual(@finderElm)

describe "ReferenceField.Finder::getClass", ->
  beforeEach ->
    window.Hoge = {Piyo: {FooBarFinder: ->}}

  describe "存在するクラス名をオプション`jsClass`に渡した場合", ->
    it "は、その名前のクラスを返すこと", ->
      expect(ReferenceField.Finder.getClass(jsClass: "Hoge.Piyo.FooBarFinder")).toEqual(Hoge.Piyo.FooBarFinder)

  describe "存在しないクラス名をオプション`jsClass`に渡した場合", ->
    it "は、基底参照ファインダークラスを返すこと", ->
      expect(ReferenceField.Finder.getClass(jsClass: "UndefinedFinder")).toEqual(ReferenceField.Finder)

  describe "対応するクラスが存在する参照ファインダー名をオプション`finder`に渡した場合", ->
    it "は、そのクラスを返すこと", ->
      expect(ReferenceField.Finder.getClass(finder: "hoge/piyo/foo_bar")).toEqual(Hoge.Piyo.FooBarFinder)

  describe "対応するクラスが存在しない参照ファインダー名をオプション`finder`に渡した場合", ->
    it "は、基底参照ファインダークラスを返すこと", ->
      expect(ReferenceField.Finder.getClass(finder: "undefined")).toEqual(ReferenceField.Finder)

  describe "オプション`jsClass`と`finder`を両方渡した場合", ->
    before ->
      window.HogePiyoFinder = ->

    it "は、`jsClass`を優先すること", ->
      expect(ReferenceField.Finder.getClass(jsClass: "Hoge.Piyo.FooBarFinder", finder: "HogePiyoFinder")).toEqual(Hoge.Piyo.FooBarFinder)
      expect(ReferenceField.Finder.getClass(jsClass: "UndefinedFinder", finder: "HogePiyoFinder")).toEqual(HogePiyoFinder)
