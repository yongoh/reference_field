itBehavesLikeClose = ->
  it "は、キューに含まれている全ての参照フィールドの指標をはずすこと", ->
    ids = String(@finderContainer.dataset.fieldQue).split(ReferenceField.Field.Que.SEPARATOR)
    @button.click()
    ids.forEach (id)->
      expect("##{id}").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)

  it "は、参照フィールドキューを空にすること", ->
    @button.click()
    expect(@finderContainer.dataset).not.toHaveData("field-que")

  it "は、参照ファインダーコンテナを隠すこと", (done)->
    @button.click()
    setTimeout =>
      expect(@finderContainer).toBeHidden()
      done()
    , 1000

  it "は、参照フィールドの「探」ボタンにフォーカスすること", ->
    @button.click()
    expect(@findButton).toBeFocused()

itBehavesLikeAlwaysClose = ->
  describe "1つの参照フィールドがキューに入っている場合", ->
    itBehavesLikeClose.call(this)

  describe "複数の参照フィールドがキューに入っている場合", ->
    beforeEach ->
      @finderContainer.dataset.fieldQue = "field-1,field-2,field-3"

    itBehavesLikeClose.call(this)

itBehavesLikeCloseOrStay = ->
  describe "1つの参照フィールドがキューに入っている場合", ->
    itBehavesLikeClose.call(this)

  describe "複数の参照フィールドがキューに入っている場合", ->
    beforeEach ->
      @finderContainer.dataset.fieldQue = "field-1,field-2,field-3"

    it "は、キューに入っているうち末尾の一つ前を除く参照フィールドの指標をはずすこと", ->
      @button.click()
      expect("#field-0").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect("#field-1").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect("#field-2").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect("#field-3").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect("#field-4").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)

    it "は、参照フィールドキューから末尾を削除すること", ->
      @button.click()
      expect(@finderContainer).toHaveData("field-que", "field-1,field-2")

    it "は、参照ファインダーコンテナを隠さないこと", (done)->
      @button.click()
      setTimeout =>
        expect(@finderContainer).not.toBeHidden()
        done()
      , 1000

    it "は、参照フィールドの「探」ボタンにフォーカスすること", ->
      @button.click()
      expect(@findButton).toBeFocused()


describe "events", ->
  beforeEach ->
    # 参照ファインダーコンテナ
    loadFixtures("finder_container.html")
    appendLoadStyleFixtures("modal_window.css")
    appendLoadStyleFixtures("radio_button_tab.css")
    @finderContainer = document.querySelector(ReferenceField.Finder.SELECTORS.CONTAINER)

    # 参照フィールド
    @ids = [0..4].map((n)-> "field-#{n}")
    for id in @ids
      appendLoadFixtures("reference_field/animal.html")
      elm = document.querySelector(ReferenceField.Field.SELECTORS.FIELD + ":last-of-type")
      elm.id = id
      elm.classList.add(ReferenceField.Field.HTML_CLASSES.CURRENT)
    @field = document.getElementById("field-3")
    @findButton = @field.querySelector(EventButton.SELECTORS.button("reference_field:find"))

  describe "参照ファインダーコンテナを非表示にした状態で「探」ボタンをクリックした場合", ->
    beforeEach ->
      document.getElementById("reference_finder-trigger").checked = false

    describe "`for`属性値に対応する参照ファインダーコンテナが存在する場合", ->
      it "は、ボタンが含まれる参照フィールドに指標をつけること", ->
        @findButton.click()
        expect(@field).toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)

      it "は、ボタンが含まれる参照フィールドをキューに追加すること", ->
        @findButton.click()
        expect(@finderContainer.dataset.fieldQue).toBe(@field.id)

      it "は、対応する参照ファインダーコンテナを表示すること", ->
        @findButton.click()
        expect(@finderContainer).toBeVisible()

      it "は、現在の参照ファインダーを選択すること", ->
        @findButton.click()
        expect("#reference_finder_tab-foo-tab").toBeChecked()
        expect("#reference_finder_tab-bar-tab").not.toBeChecked()
        expect("#reference_finder_tab-baz-tab").not.toBeChecked()
        expect("#reference_finder_tab-foo-content").toBeVisible()
        expect("#reference_finder_tab-bar-content").toBeHidden()
        expect("#reference_finder_tab-baz-content").toBeHidden()

      it "は、現在の参照ファインダーのコールバックが実行されること", ->
        document.querySelector("#reference_finder_tab-foo-content").dataset.jsClass = "CustomFinder"
        spyOn(window, "alert")
        @findButton.click()
        expect(window.alert).toHaveBeenCalledWith("Before activation!")

    describe "`for`属性値に対応する参照ファインダーコンテナが存在しない場合", ->
      beforeEach ->
        document.getElementById("reference_finder-trigger").id = "other_id"

      it "は、参照ファインダーコンテナが見つからなかったことを示すアラートを発生させること", ->
        spyOn(window, "alert")
        @findButton.click()
        expect(window.alert).toHaveBeenCalledWith("Missing reference finder. for='reference_finder-trigger'")

      it "は、ボタンが含まれる参照フィールドに指標をつけないこと", ->
        @findButton.click()
        expect(@field).not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)

    describe "イベントボタン要素に属性`for`が存在しない場合", ->
      beforeEach ->
        @findButton.removeAttribute("for")

      it "は、属性`for`が存在しないことを示すアラートを発生させること", ->
        spyOn(window, "alert")
        @findButton.click()
        expect(window.alert).toHaveBeenCalledWith("Missing reference finder. for='null'")

      it "は、ボタンが含まれる参照フィールドに指標をつけないこと", ->
        @findButton.click()
        expect(@field).not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)

  describe "参照ファインダーコンテナを表示した状態で", ->
    beforeEach ->
      document.getElementById("reference_finder-trigger").checked = true
      @finderContainer.dataset.fieldQue = @field.id

    describe "参照ファインダーのタブをクリックした場合", ->
      beforeEach ->
        @label = @finderContainer.querySelector("label[for='reference_finder_tab-bar-tab']")

      it "は、その参照ファインダーを選択すること", ->
        @label.click()
        expect("#reference_finder_tab-foo-tab").not.toBeChecked()
        expect("#reference_finder_tab-bar-tab").toBeChecked()
        expect("#reference_finder_tab-baz-tab").not.toBeChecked()

      it "は、参照ファインダー本体を表示すること", ->
        @label.click()
        expect("#reference_finder_tab-foo-content").toBeHidden()
        expect("#reference_finder_tab-bar-content").toBeVisible()
        expect("#reference_finder_tab-baz-content").toBeHidden()

      it "は、参照ファインダーのコールバックが実行されること", ->
        document.querySelector("#reference_finder_tab-bar-content").dataset.jsClass = "CustomFinder"
        spyOn(window, "alert")
        @label.click()
        expect(window.alert).toHaveBeenCalledWith("Before activation!")

  describe "フィールドを選択した状態で", ->
    beforeEach ->
      @field.classList.add(ReferenceField.Field.HTML_CLASSES.CURRENT)
      @finderContainer.dataset.fieldQue = @field.id
      document.getElementById("reference_finder-trigger").checked = true
      @foreignKeyField = @field.querySelector(ReferenceField.Field.SELECTORS.FOREIGN_KEY)

    describe "「閉じる」ボタンをクリックした場合", ->
      beforeEach ->
        @button = @finderContainer.querySelector(EventButton.SELECTORS.button("reference_field:close"))

      itBehavesLikeAlwaysClose.call(this)

    describe "モーダルウインドウ背景をクリックした場合", ->
      beforeEach ->
        @button = document.querySelector("label[for='#{ModalWindow.SELECTORS.toTriggerId(@finderContainer.id)}']")

      itBehavesLikeAlwaysClose.call(this)

    describe "「参照解除」ボタンをクリックした場合", ->
      beforeEach ->
        @button = @finderContainer.querySelector(EventButton.SELECTORS.button("reference_field:dereference"))

      describe "外部キーフィールドに属性`required`が無い場合", ->
        it "は、カレントフィールドの参照先モデル名隠しフィールドの`value`属性を削除すること", ->
          @button.click()
          expect(@field.querySelector(":scope > #{ReferenceField.Field.SELECTORS.FOREIGN_TYPE}")).not.toHaveAttr("value")

        it "は、カレントフィールドの参照先モデルID隠しフィールドの`value`属性を削除すること", ->
          @button.click()
          expect(@foreignKeyField).not.toHaveAttr("value")

        it "は、カレントフィールドのバナーを未参照を示すものに変えること", ->
          @button.click()
          expect(@field).toContainElement(".unreferenced")
          expect(@field).not.toContainElement(".record_banner[data-model='Animal']")

        itBehavesLikeCloseOrStay.call(this)

      describe "外部キーフィールドに属性`required`がある場合", ->
        beforeEach ->
          @foreignKeyField.required = true

        it "は、必須項目であることをしらせるアラートを表示すること", ->
          spyOn(window, "alert")
          @button.click()
          expect(window.alert).toHaveBeenCalledWith("'##{@foreignKeyField.id}' is required.")

        it "は、カレントフィールドを変化させないこと", ->
          @button.click()
          expect(@foreignKeyField).toHaveValue("2")
          expect(@field).not.toContainElement(".unreferenced")
          expect(@field).toContainElement(".record_banner[data-model='Animal']")
          expect(@field).toEqual("#{ReferenceField.Field.SELECTORS.FIELD}.#{ReferenceField.Field.HTML_CLASSES.CURRENT}")
          expect(@finderContainer.dataset.fieldQue).toBe(@field.id)

        it "は、参照ファインダーコンテナを隠さないこと", ->
          @button.click()
          expect(@finderContainer).toBeVisible()

    describe "「決定」ボタンをクリックした場合", ->
      beforeEach ->
        appendLoadFixtures("animal_list.html")
        document.querySelector("#reference_finder_tab-foo-content").appendChild(document.getElementById("animal_list"))
        @button = document.querySelector("#{EventButton.SELECTORS.button("reference_field:choice")}[data-model='Animal'][data-record-id='3']")

      describe "参照フィールドがポリモーフィック非対応の場合", ->
        describe "許可されるモデルの場合", ->
          it "は、決定したレコードを参照フィールドの値にセットすること", ->
            @button.click()
            expect(@field.querySelector(ReferenceField.Field.SELECTORS.FOREIGN_TYPE)).toHaveValue("Animal")
            expect(@field.querySelector(ReferenceField.Field.SELECTORS.FOREIGN_KEY)).toHaveValue("3")
            expect(@field).not.toContainElement(".record_banner[data-record-id='2']")
            expect(@field).toContainElement(".record_banner[data-record-id='3']")

          itBehavesLikeCloseOrStay.call(this)

        describe "許可されないモデルの場合", ->
          beforeEach ->
            @button.dataset.model = "Company"

          it "は、メッセージを表示すること", ->
            spyOn(window, "alert")
            @button.click()
            expect(window.alert).toHaveBeenCalledWith("Cannot set Company as reference.")

          it "は、参照フィールドの値を変化させないこと", ->
            @button.click()
            expect(@field.querySelector(ReferenceField.Field.SELECTORS.FOREIGN_TYPE)).toHaveValue("Animal")
            expect(@field.querySelector(ReferenceField.Field.SELECTORS.FOREIGN_KEY)).toHaveValue("2")
            expect(@field).toContainElement(".record_banner[data-record-id='2']")
            expect(@field).not.toContainElement(".record_banner[data-record-id='3']")

          it "は、参照ファインダーコンテナを隠さないこと", ->
            @button.click()
            expect(@finderContainer).toBeVisible()

      describe "参照フィールドがポリモーフィック対応の場合", ->
        beforeEach ->
          foreignTypeField = @field.querySelector(ReferenceField.Field.SELECTORS.FOREIGN_TYPE)
          foreignTypeField.name = "animal[parent_type]"
          foreignTypeField.value = "Company"
          foreignTypeField.pattern = "^Person|Animal|Company$"

        describe "許可されるモデルの場合", ->
          it "は、決定したレコードを参照フィールドの値にセットすること", ->
            @button.click()
            expect(@field.querySelector(ReferenceField.Field.SELECTORS.FOREIGN_TYPE)).toHaveValue("Animal")
            expect(@field.querySelector(ReferenceField.Field.SELECTORS.FOREIGN_KEY)).toHaveValue("3")
            expect(@field).not.toContainElement(".record_banner[data-record-id='2']")
            expect(@field).toContainElement(".record_banner[data-record-id='3']")

          itBehavesLikeCloseOrStay.call(this)

        describe "許可されないモデルの場合", ->
          beforeEach ->
            @button.dataset.model = "Profile"

          it "は、メッセージを表示すること", ->
            spyOn(window, "alert")
            @button.click()
            expect(window.alert).toHaveBeenCalledWith("Cannot set Profile as reference.")

          it "は、参照フィールドの値を変化させないこと", ->
            @button.click()
            expect(@field.querySelector(ReferenceField.Field.SELECTORS.FOREIGN_TYPE)).toHaveValue("Company")
            expect(@field.querySelector(ReferenceField.Field.SELECTORS.FOREIGN_KEY)).toHaveValue("2")
            expect(@field).toContainElement(".record_banner[data-record-id='2']")
            expect(@field).not.toContainElement(".record_banner[data-record-id='3']")

          it "は、参照ファインダーコンテナを隠さないこと", ->
            @button.click()
            expect(@finderContainer).toBeVisible()

      describe "参照禁止レコードを参照した場合", ->
        beforeEach ->
          @field.dataset.prohibitedTargets = "Animal:3"

        it "は、メッセージを表示すること", ->
          spyOn(window, "alert")
          @button.click()
          expect(window.alert).toHaveBeenCalledWith("This record is not allowed to be referenced.")

        it "は、参照フィールドの値を変化させないこと", ->
          @button.click()
          expect(@field.querySelector(ReferenceField.Field.SELECTORS.FOREIGN_TYPE)).toHaveValue("Animal")
          expect(@field.querySelector(ReferenceField.Field.SELECTORS.FOREIGN_KEY)).toHaveValue("2")
          expect(@field).toContainElement(".record_banner[data-record-id='2']")
          expect(@field).not.toContainElement(".record_banner[data-record-id='3']")

        it "は、参照ファインダーコンテナを隠さないこと", ->
          @button.click()
          expect(@finderContainer).toBeVisible()
