describe "ReferenceField.Finder.Container", ->
  beforeEach ->
    loadFixtures("finder_container.html")
    appendLoadStyleFixtures("modal_window.css")
    appendLoadStyleFixtures("radio_button_tab.css")
    @finderContainer = new ReferenceField.Finder.Container(document.querySelector(ReferenceField.Finder.SELECTORS.CONTAINER))

  describe "#currentFinder", ->
    beforeEach ->
      document.querySelector("#reference_finder_tab-bar-tab").click()

    it "は、現在選択されている参照ファインダーオブジェクトを返すこと", ->
      finder = @finderContainer.currentFinder()
      expect(finder).toEqual(jasmine.any(ReferenceField.Finder))
      expect(finder.element).toHaveId("reference_finder_tab-bar-content")

  describe "#open", ->
    beforeEach ->
      appendLoadFixtures("reference_field/profile.html")
      document.getElementById("reference_finder-trigger").checked = false

      @field = new ReferenceField.Field(document.getElementById("profile_profilable_id_reference_field"))

    it "は、参照ファインダーコンテナを表示すること", ->
      @finderContainer.open(@field)
      expect(@finderContainer.element).toBeVisible()

    it "は、「閉じる」ボタンをフォーカスすること", ->
      @finderContainer.open(@field)
      expect(@finderContainer.element.querySelector(EventButton.SELECTORS.button("reference_field:close"))).toBeFocused()

    it "は、現在の参照ファインダーを選択すること", ->
      @finderContainer.open(@field)
      expect("#reference_finder_tab-foo-tab").toBeChecked()
      expect("#reference_finder_tab-bar-tab").not.toBeChecked()
      expect("#reference_finder_tab-baz-tab").not.toBeChecked()
      expect("#reference_finder_tab-foo-content").toBeVisible()
      expect("#reference_finder_tab-bar-content").toBeHidden()
      expect("#reference_finder_tab-baz-content").toBeHidden()

    it "は、現在の参照ファインダーのコールバックが実行されること", ->
      document.querySelector("#reference_finder_tab-foo-content").dataset.jsClass = "CustomFinder"
      spyOn(window, "alert")
      @finderContainer.open(@field)
      expect(window.alert).toHaveBeenCalledWith("Before activation!")

    describe "カレント参照フィールドの外部キーフィールドに属性`required`がある場合", ->
      beforeEach ->
        document.getElementById("profile_profilable_type").required = true
        document.getElementById("profile_profilable_id").required = true

      it "は、「参照解除」ボタンを無効化すること", ->
        @finderContainer.open(@field)
        expect(EventButton.SELECTORS.button("reference_field:dereference")).toBeDisabled()

  describe "#close", ->
    beforeEach ->
      appendLoadFixtures("reference_field/profile.html")
      document.getElementById("reference_finder-trigger").checked = true
      @fieldQue = new ReferenceField.Field.Que(@finderContainer.element)
      @fieldQue.element.dataset.fieldQue = "field-0,field-1,field-2"

    it "は、参照ファインダーコンテナを隠すこと", (done)->
      @finderContainer.close(@fieldQue)
      setTimeout =>
        expect(@finderContainer.element).toBeHidden()
        done()
      , 1000

    it "は、キューを空にすること", ->
      @finderContainer.close(@fieldQue)
      expect(@finderContainer.element).not.toHaveData("field-que")
