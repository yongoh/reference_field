appendLoadFields = ->
  @ids = [0..4].map((n)-> "field-#{n}")
  for id in @ids
    appendLoadFixtures("reference_field/profile.html")
    elm = document.querySelector(ReferenceField.Field.SELECTORS.FIELD + ":last-of-type")
    elm.id = id
    elm.classList.add(ReferenceField.Field.HTML_CLASSES.CURRENT)

describe "ReferenceField.Field.Que", ->
  beforeEach ->
    loadFixtures("finder_container.html")
    @element = document.querySelector(ReferenceField.Finder.SELECTORS.CONTAINER)
    @fieldQue = new ReferenceField.Field.Que(@element)

  describe "#_que", ->
    describe "`data-field-que`属性がID群を値に持つ場合", ->
      beforeEach ->
        @element.dataset.fieldQue = "foo,bar,baz"

      it "は、ID群の配列を返すこと", ->
        expect(@fieldQue._que()).toEqual(["foo", "bar", "baz"])

    describe "`data-field-que`属性が空文字を値に持つ場合", ->
      beforeEach ->
        @element.dataset.fieldQue = ""

      it "は、空の配列を返すこと", ->
        expect(@fieldQue._que()).toEqual([])

    describe "`data-field-que`属性がない場合", ->
      it "は、空の配列を返すこと", ->
        expect(@fieldQue._que()).toEqual([])

  describe "#forEach", ->
    beforeEach ->
      @ids = [0..4].map((n)-> "field-#{n}")
      for id in @ids
        appendLoadFixtures("reference_field/profile.html")
        document.querySelector(ReferenceField.Field.SELECTORS.FIELD + ":last-of-type").id = id

      @func = ->
      @spy = spyOn(@func, 'call')

    describe "`data-field-que`属性がID群を値に持つ場合", ->
      beforeEach ->
        @element.dataset.fieldQue = "field-1,field-2,field-x,field-3"

      it "は、第1引数に渡した関数にIDに対応する参照フィールドオブジェクトを1つずつ渡して実行すること", ->
        ary = []
        @fieldQue.forEach (field)->
          expect(field).toEqual(jasmine.any(ReferenceField.Field))
          ary.push(field)
        expect(ary[0].element.id).toBe("field-1")
        expect(ary[1].element.id).toBe("field-2")
        expect(ary[2].element.id).toBe("field-3")
        expect(ary.length).toBe(3)

    describe "`data-field-que`属性が空文字を値に持つ場合", ->
      beforeEach ->
        @element.dataset.fieldQue = ""

      it "は、第1引数に渡した関数を実行しないこと", ->
        @fieldQue.forEach(@func)
        expect(@spy).not.toHaveBeenCalled()

    describe "`data-field-que`属性がない場合", ->
      it "は、第1引数に渡した関数を実行しないこと", ->
        @fieldQue.forEach(@func)
        expect(@spy).not.toHaveBeenCalled()

  describe "#isEmpty", ->
    describe "`data-field-que`属性がID群を値に持つ場合", ->
      beforeEach ->
        @element.dataset.fieldQue = "foo,bar,baz"

      it "は、`false`を返すこと", ->
        expect(@fieldQue.isEmpty()).toBeFalse()

    describe "`data-field-que`属性が空文字を値に持つ場合", ->
      beforeEach ->
        @element.dataset.fieldQue = ""

      it "は、`true`を返すこと", ->
        expect(@fieldQue.isEmpty()).toBeTrue()

    describe "`data-field-que`属性がない場合", ->
      it "は、`true`を返すこと", ->
        expect(@fieldQue.isEmpty()).toBeTrue()

  describe "#push", ->
    beforeEach ->
      appendLoadFields.call(this)
      @fields = [3..4].map((n)-> new ReferenceField.Field(document.querySelector("#field-#{n}")))

    describe "`data-field-que`属性がID群を値に持つ場合", ->
      beforeEach ->
        @element.dataset.fieldQue = [0..2].map((n)-> "field-#{n}").join(ReferenceField.Field.Que.SEPARATOR)

      it "は、渡した参照フィールドのIDを`data-field-que`属性値の末尾に追加すること", ->
        expect(@element).toHaveAttr("data-field-que", "field-0,field-1,field-2")
        @fieldQue.push(@fields...)
        expect(@element).toHaveAttr("data-field-que", "field-0,field-1,field-2,field-3,field-4")

      it "は、追加後のキューの末尾の参照フィールドに指標をつけ、それ以外の指標を削除すること", ->
        expect("#field-0").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-1").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-2").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-3").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-4").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        @fieldQue.push(@fields...)
        expect("#field-0").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-1").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-2").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-3").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-4").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)

      it "は、追加後のキューの数を返すこと", ->
        expect(@fieldQue.push(@fields...)).toBe(5)

      describe "すでにキューに含まれる参照フィールドを渡した場合", ->
        beforeEach ->
          @fields = [1..3].map((n)-> new ReferenceField.Field(document.querySelector("#field-#{n}")))

        it "は、その参照フィールドは追加しないこと", ->
          expect(@element).toHaveAttr("data-field-que", "field-0,field-1,field-2")
          expect(@fieldQue.push(@fields...)).toBe(4)
          expect(@element).toHaveAttr("data-field-que", "field-0,field-1,field-2,field-3")

    describe "`data-field-que`属性が空文字を値に持つ場合", ->
      beforeEach ->
        @element.dataset.fieldQue = ""

      it "は、渡した文字列を`data-field-que`属性にセットすること", ->
        @fieldQue.push(@fields...)
        expect(@element).toHaveData("field-que", "field-3,field-4")

  describe "#pop", ->
    beforeEach ->
      appendLoadFields.call(this)

    describe "`data-field-que`属性がID群を値に持つ場合", ->
      beforeEach ->
        @element.dataset.fieldQue = @ids.join(ReferenceField.Field.Que.SEPARATOR)
        document.getElementById(@ids[@ids.length - 1]).classList.add(ReferenceField.Field.HTML_CLASSES.CURRENT)

      it "は、`data-field-que`属性の値から末尾のIDを削除すること", ->
        expect(@element).toHaveAttr("data-field-que", "field-0,field-1,field-2,field-3,field-4")
        @fieldQue.pop()
        expect(@element).toHaveAttr("data-field-que", "field-0,field-1,field-2,field-3")

      it "は、削除後のキューの末尾の参照フィールドに指標をつけ、それ以外の指標を削除すること", ->
        expect("#field-0").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-1").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-2").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-3").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-4").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        @fieldQue.pop()
        expect("#field-0").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-1").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-2").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-3").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-4").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)

      it "は、キューから削除した参照フィールドを返すこと", ->
        result = @fieldQue.pop()
        expect(result).toEqual(jasmine.any(ReferenceField.Field))
        expect(result.element).toHaveId("field-4")

    describe "`data-field-que`属性がない場合", ->
      beforeEach ->
        document.getElementById("field-1").classList.remove(ReferenceField.Field.HTML_CLASSES.CURRENT)
        document.getElementById("field-3").classList.remove(ReferenceField.Field.HTML_CLASSES.CURRENT)

      it "は、`data-field-que`属性を付与しないこと", ->
        @fieldQue.pop()
        expect(@element).not.toHaveData("field-que")

      it "は、全ての参照フィールドの指標を変化させないこと", ->
        expect("#field-0").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-1").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-2").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-3").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-4").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        @fieldQue.pop()
        expect("#field-0").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-1").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-2").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-3").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
        expect("#field-4").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)

      it "は、`null`を返すこと", ->
        expect(@fieldQue.pop()).toBeNull()

  describe "#clear", ->
    beforeEach ->
      appendLoadFields.call(this)
      @element.dataset.fieldQue = "field-1,field-2,field-4"

    it "は、`data-field-que`属性を削除すること", ->
      @fieldQue.clear()
      expect(@element).not.toHaveData("field-que")

    it "は、キューに含まれる全ての参照フィールドの指標を削除すること", ->
      expect("#field-0").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect("#field-1").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect("#field-2").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect("#field-3").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect("#field-4").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      @fieldQue.clear()
      expect("#field-0").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect("#field-1").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect("#field-2").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect("#field-3").toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)
      expect("#field-4").not.toHaveClass(ReferenceField.Field.HTML_CLASSES.CURRENT)

  describe "#currentField", ->
    describe "`data-field-que`属性がID群を値に持つ場合", ->
      beforeEach ->
        @element.dataset.fieldQue = "foo,bar,profile_profilable_id_reference_field"

      describe "末尾のIDに対応する参照フィールド要素がある場合", ->
        beforeEach ->
          appendLoadFixtures("reference_field/profile.html")

        it "は、参照フィールドオブジェクトを返すこと", ->
          expect(@fieldQue.currentField()).toEqual(jasmine.any(ReferenceField.Field))

      describe "末尾のIDに対応する参照フィールド要素がない場合", ->
        it "は、`null`を返すこと", ->
          expect(@fieldQue.currentField()).toBeNull()

    describe "`data-field-que`属性が空文字を値に持つ場合", ->
      beforeEach ->
        @element.dataset.fieldQue = ""

      it "は、`null`を返すこと", ->
        expect(@fieldQue.currentField()).toBeNull()

    describe "`data-field-que`属性がない場合", ->
      it "は、`null`を返すこと", ->
        expect(@fieldQue.currentField()).toBeNull()
