describe "ReferenceField.Field.EventButton", ->
  beforeEach ->
    loadFixtures("reference_field/animal.html")
    appendLoadFixtures("finder_container.html")
    elm = document.querySelector(EventButton.SELECTORS.button("reference_field:find"))
    @eventButton = new ReferenceField.Field.EventButton({target: elm})

  describe "#finderContainer", ->
    describe "対応する参照ファインダーコンテナがある場合", ->
      it "は、参照ファインダーコンテナを返すこと", ->
        finderContainer = @eventButton.finderContainer()
        expect(finderContainer).toEqual(jasmine.any(ReferenceField.Finder.Container))
        expect(finderContainer.element).toHaveId("reference_finder-window")

    describe "対応する参照ファインダーコンテナが無い場合", ->
      beforeEach ->
        document.getElementById("reference_finder-trigger").id = "other_id"

      it "は、参照ファインダーコンテナが見つからなかったことを示す例外を発生させること", ->
        expect(=> @eventButton.finderContainer()).toThrow(new ReferenceField.Field.MissingError("Missing reference finder. for='reference_finder-trigger'"))

  describe "#field", ->
    it "は、自身が属する参照フィールドを返すこと", ->
      field = @eventButton.field()
      expect(field).toEqual(jasmine.any(ReferenceField.Field))
      expect(field.element).toHaveId("animal_parent_id_reference_field")
