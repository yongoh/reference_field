describe "ReferenceField.Field.HiddenField", ->
  beforeEach ->
    loadFixtures("reference_field/profile.html")
    @input = new ReferenceField.Field.HiddenField(document.getElementById("profile_profilable_type"))

  describe "#setValue", ->
    describe "値を渡した場合", ->
      it "は、`value`属性に値をセットすること", ->
        @input.setValue("Person")
        expect("#profile_profilable_type").toHaveValue("Person")

    describe "`null`を渡した場合", ->
      it "は、`value`属性を削除すること", ->
        @input.setValue(null)
        expect("#profile_profilable_type").not.toHaveAttr("value")

  describe "#validate", ->
    describe "`null`を渡した場合", ->
      describe "入力欄が`required`属性を持つ場合", ->
        beforeEach ->
          @input.element.required = true

        it "は、バリデーションエラーを発生させること", ->
          expect(=> @input.validate(null)).toThrow(new ReferenceField.Field.ValidationError("'#profile_profilable_type' is required."))

    describe "入力欄に`pattern`属性がある場合", ->
      describe "パターンに合う値を渡した場合", ->
        it "は、例外を発生させないこと", ->
          expect(=> @input.validate("Person")).not.toThrow()

      describe "パターンに合わない値を渡した場合", ->
        it "は、バリデーションエラーを発生させること", ->
          expect(=> @input.validate("Profile")).toThrow(new ReferenceField.Field.ValidationError("Cannot set Profile as reference."))

    describe "入力欄に`pattern`属性が無い場合", ->
      beforeEach ->
        @input.element.removeAttribute("pattern")

      it "は、例外を発生させないこと", ->
        expect(=> @input.validate("Person")).not.toThrow()
