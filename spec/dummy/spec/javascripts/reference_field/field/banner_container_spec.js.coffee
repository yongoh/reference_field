describe "ReferenceField.Field.BannerContainer", ->
  beforeEach ->
    loadFixtures("reference_field/animal.html")
    @container = new ReferenceField.Field.BannerContainer(document.querySelector("#animal_parent_id_reference_field > #{ReferenceField.Field.SELECTORS.BANNER_CONTAINER}"))

  describe "#setValue", ->
    describe "HTML要素を渡した場合", ->
      beforeEach ->
        @newBanner = document.createElement('div')
        @newBanner.id = "my-id"
        @newBanner.classList.add("my-class")

      it "は、バナーコンテナに渡した要素を入れること", ->
        @container.setValue(@newBanner)
        expect(@container.element).toContainElement("div#my-id.my-class")
        expect(@container.element).not.toContainElement("span.record_banner[data-model='Animal'][data-record-id='2']")

    describe "HTML文字列を渡した場合", ->
      it "は、バナーコンテナに渡した要素を入れること", ->
        @container.setValue('<div id="my-id" class="my-class"></div>')
        expect(@container.element).toContainElement("div#my-id.my-class")
        expect(@container.element).not.toContainElement("span.record_banner[data-model='Animal'][data-record-id='2']")

    describe "`null`を渡した場合", ->
      it "は、バナーコンテナを空にすること", ->
        @container.setValue(null)
        expect(@container.element).toBeEmpty()

  describe "#validate", ->
    describe "`null`を渡した場合", ->
      it "は、バリデーションエラーを発生させること", ->
        expect(=> @container.validate(null)).toThrow(new ReferenceField.Field.ValidationError("Banner is required."))
