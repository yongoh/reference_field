require 'rails_helper'

describe "people/finders/_search" do
  before do
    render partial: "people/finders/search", locals: {finder: finder}
  end

  let(:finder){ SearchFinder.new(self, nil, "search") }

  it "は、見出し要素を描画すること" do
    expect(rendered).to have_tag("h2", text: "人物を検索")
  end

  it "は、検索フォームを表示すること" do
    expect(rendered).to have_form("/people.js", "get"){
      with_hidden_field("ajax_target")

      with_tag("label", with: {for: "q_name_cont"})
      with_text_field("q[name_cont]")

      with_submit("検索")
    }
  end

  it "は、空の結果表示欄を描画すること" do
    expect(rendered).to have_tag(".search_finder-index:empty")
  end
end
