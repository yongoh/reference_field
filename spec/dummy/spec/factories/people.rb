FactoryBot.define do
  factory :person do
    sequence(:name){|n| "Person-#{n}"}

    trait :with_parent do
      association :parent, factory: :person
    end

    trait :with_children do
      children{ create_list(:person, 3) }
    end

    trait :with_friends do
      friends{ create_list(:person, 3) }
    end
  end
end
