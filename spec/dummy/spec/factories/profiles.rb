FactoryBot.define do
  factory :profile do
    disription { "MyString" }
    profilable{ create(:person) }

    trait :with_company do
      profilable{ create(:company) }
    end
  end
end
