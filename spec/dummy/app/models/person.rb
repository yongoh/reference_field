class Person < ApplicationRecord
  belongs_to :parent, class_name: name, required: false
  has_many :children, class_name: name, foreign_key: :parent_id
  has_and_belongs_to_many :friends, class_name: name, join_table: :friends, foreign_key: :from_id, association_foreign_key: :to_id
  accepts_nested_attributes_for :friends
end
