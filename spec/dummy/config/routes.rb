Rails.application.routes.draw do
  resources :profiles do
    collection{ get :search }
  end
  resources :people do
    collection{ get :search }
  end
  mount JasmineRails::Engine => '/specs' if defined?(JasmineRails)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
