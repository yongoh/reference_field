class AddParentIdFromPeople < ActiveRecord::Migration[5.2]
  def change
    add_column :people, :parent_id, :integer, index: true
  end
end
