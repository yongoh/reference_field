class CreateFriends < ActiveRecord::Migration[5.2]
  def change
    create_table :friends do |t|
      t.belongs_to :from, foreign_key: true
      t.belongs_to :to, foreign_key: true
    end
  end
end
