class RemoveParentIdFromPeople < ActiveRecord::Migration[5.2]
  def change
    remove_column :people, :parent_id, :integer
  end
end
