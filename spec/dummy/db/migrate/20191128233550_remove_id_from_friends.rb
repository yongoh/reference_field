class RemoveIdFromFriends < ActiveRecord::Migration[5.2]
  def change
    remove_column :friends, :id, :integer
  end
end
