# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_28_233550) do

  create_table "companies", force: :cascade do |t|
    t.string "name"
  end

  create_table "friends", id: false, force: :cascade do |t|
    t.integer "from_id"
    t.integer "to_id"
    t.index ["from_id"], name: "index_friends_on_from_id"
    t.index ["to_id"], name: "index_friends_on_to_id"
  end

  create_table "people", force: :cascade do |t|
    t.string "name"
    t.integer "parent_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.string "profilable_type"
    t.integer "profilable_id"
    t.string "disription"
    t.index ["profilable_type", "profilable_id"], name: "index_profiles_on_profilable_type_and_profilable_id"
  end

end
