module ReferenceField
  module Ransack
    class Railtie < ::Rails::Railtie

      initializer "extend Ransack" do
        ::Ransack::Helpers::FormBuilder.prepend(Ransack)
      end
    end
  end
end
