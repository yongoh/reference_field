require "ransack/helpers/form_builder"

require "reference_field/ransack/railtie"

module ReferenceField
  module Ransack

    # @param [String] association_name 関連名
    # @param [ActiveRecord::Base] record 参照フィールドの初期値となるレコード
    # @return [ActiveSupport::SafeBuffer] 検索用参照フィールド
    # @see ActionView::Base#reference_field
    def reference_field(association_name, record = nil, self_referencing: true, **options)
      @template.reference_field(object_name, association_name, options) do |field|
        # 隠し入力欄の`name`属性値をセット
        field.foreign_type.name = "#{object_name}[#{association_name}_type_eq]"
        field.foreign_key.name = "#{object_name}[#{association_name}_id_eq]"

        # 参照先レコードを設定
        field.target = record || association_condition_records(association_name, "eq").first

        # 関連の種類を設定
        field.reflection = reflect_on_association(association_name)

        yield(field) if block_given?
      end
    end

    # @param [String] association_name 関連名
    # @param [Array<ActiveRecord::Base>] records 参照フィールドの初期値となるレコード群
    # @param [Hash] options ActionField用・参照フィールド用が混合したオプション群
    # @return [ActiveSupport::SafeBuffer] OR検索用参照フィールド
    # @see ActionField::FormHelper#action_field
    # @see ActionView::Base#collection_reference_field
    def collection_reference_field(association_name, records = nil, **options)
      reflection = reflect_on_association(association_name)
      records ||= association_condition_records(association_name, "in")

      # 複数関連フィールドを作成
      @template.collection_reference_field(object_name, association_name, records, options) do |field|
        # 隠し入力欄の`name`属性値をセット
        field.foreign_type.name = "#{object_name}[#{association_name}_type_in][]"
        field.foreign_key.name = "#{object_name}[#{association_name}_id_in][]"

        # 関連の種類を設定
        field.reflection = reflection

        yield(field) if block_given?
      end
    end

    private

    def reflect_on_association(association_name)
      object.klass.reflect_on_association(association_name)
    end

    # @param [String] association_name 関連名
    # @param [String] predicate 述語
    # @return [Array<RecordBanner::DummyRecord>] 検索条件に対応する関連先レコードの配列
    def association_condition_records(association_name, predicate)
      reflection = reflect_on_association(association_name)

      types = condition_values("#{association_name}_type", predicate)
      condition_values("#{association_name}_id", predicate).each_with_index.map do |id, i|
        RecordBanner::DummyRecord.build([
          reflection.polymorphic? ? types[i] : reflection.klass,
          id,
        ])
      end
    end

    # @param [String] attribute 検索条件の属性名
    # @param [String] predicate 述語
    # @return [Array] 検索条件の値の配列
    def condition_values(attribute, predicate)
      object.conditions.select{|condition|
        condition.attributes.map(&:name).include?(attribute) && condition.predicate.name == predicate
      }.map{|condition| condition.values.map(&:value) }.flatten
    end
  end
end
