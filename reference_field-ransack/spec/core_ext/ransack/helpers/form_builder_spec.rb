require 'rails_helper'

describe ReferenceField::Ransack do
  let(:form_builder){ h.search_form_for(ransack){|f| return f } }
  let(:ransack){ Person.ransack }

  describe "#reference_field" do
    it "は、検索用参照フィールドを返すこと" do
      expect(form_builder.reference_field(:parent)).to have_tag(
        "label.reference_field-field#q_parent-reference_field",
        with: {for: "q_parent-find"},
      ){
        with_tag("input.foreign_type#q_parent-foreign_type:not([value]):not([name])", with: {type: "hidden", pattern: "^Person$"})
        with_tag("input.foreign_key#q_parent-foreign_key:not([value])", with: {type: "hidden", name: "q[parent_id_eq]"})
        with_tag(".event_button", with: {"data-event" => "reference_field:find", for: "people-trigger"}, text: "探")
        with_tag(".banner_container")
      }
    end

    it "は、ブロック引数に参照フィールドインスタンスを渡すこと" do
      expect{|block| form_builder.reference_field(:parent, &block) }.to yield_with_args(be_a(ReferenceField::Field))
    end

    context "ポリモーフィック関連の場合" do
      subject{ form_builder.reference_field(:profilable) }

      context "検索条件が設定されていない場合" do
        let(:ransack){ Profile.ransack }

        it "は、属性`name`を持つが値を持たない参照先モデル名隠しフィールドを含む未参照状態の参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_type#q_profilable-foreign_type:not([value]):not([pattern])", with: {name: "q[profilable_type_eq]"})
            with_tag(".foreign_key#q_profilable-foreign_key:not([value])", with: {name: "q[profilable_id_eq]"})
          }
        end

        it "は、未参照バナーを含む検索用参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".banner_container"){
              with_tag(".record_banner.unreferenced")
            }
          }
        end

        context "オプション引数`:banner_proc`に手続きを渡した場合" do
          it "は、手続きを実行しないこと" do
            expect{|block| form_builder.reference_field(:profilable, banner_proc: block) }.to yield_successive_args
          end
        end
      end

      context "検索条件が設定されている場合" do
        let(:ransack){ Profile.ransack(profilable_type_eq: "Company", profilable_id_eq: 1) }

        it "は、属性`name`と値を持つ参照先モデル名隠しフィールドを含む参照状態の参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_type#q_profilable-foreign_type:not([pattern])", with: {name: "q[profilable_type_eq]", value: "Company"})
            with_tag(".foreign_key#q_profilable-foreign_key", with: {name: "q[profilable_id_eq]", value: "1"})
          }
        end

        it "は、参照バナーを含む検索用参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".banner_container"){
              with_tag(".record_banner.referenced", with: {"data-model" => "Company", "data-record-id" => "1"})
            }
          }
        end

        context "オプション引数`:banner_proc`に手続きを渡した場合" do
          it "は、手続き引数に検索条件から作成したレコードを渡すこと" do
            expect{|block| form_builder.reference_field(:profilable, banner_proc: block) }.to yield_with_args(have_attributes(model_name: "Company", id: 1))
          end
        end
      end
    end

    context "ポリモーフィック関連ではない場合" do
      subject{ form_builder.reference_field(:parent) }

      context "検索条件が設定されていない場合" do
        it "は、属性`name`を持たないが値を持つ参照先モデル名隠しフィールドを含む未参照状態の参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_type#q_parent-foreign_type:not([value]):not([name])", with: {pattern: "^Person$"})
            with_tag(".foreign_key#q_parent-foreign_key:not([value])", with: {name: "q[parent_id_eq]"})
          }
        end

        it "は、未参照バナーを含む検索用参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".banner_container"){
              with_tag(".record_banner.unreferenced")
            }
          }
        end

        context "オプション引数`:banner_proc`に手続きを渡した場合" do
          it "は、手続きを実行しないこと" do
            expect{|block| form_builder.reference_field(:parent, banner_proc: block) }.to yield_successive_args
          end
        end
      end

      context "検索条件が設定されている場合" do
        let(:ransack){ Person.ransack(parent_id_eq: 1) }

        it "は、属性`name`を持たないが値を持つ参照先モデル名隠しフィールドを含む参照状態の参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".foreign_type#q_parent-foreign_type:not([name])", with: {value: "Person", pattern: "^Person$"})
            with_tag(".foreign_key#q_parent-foreign_key", with: {name: "q[parent_id_eq]", value: "1"})
          }
        end

        it "は、参照バナーを含む検索用参照フィールドを返すこと" do
          is_expected.to have_tag(".reference_field-field"){
            with_tag(".banner_container"){
              with_tag(".record_banner.referenced", with: {"data-model" => "Person", "data-record-id" => "1"})
            }
          }
        end

        context "オプション引数`:banner_proc`に手続きを渡した場合" do
          it "は、手続き引数に検索条件から作成したレコードを渡すこと" do
            expect{|block| form_builder.reference_field(:parent, banner_proc: block) }.to yield_with_args(have_attributes(model_name: "Person", id: 1))
          end
        end
      end
    end
  end

  describe "#collection_reference_field" do
    subject do
      form_builder.collection_reference_field(:friends)
    end

    it "は、ブロック引数に参照フィールドインスタンスを渡すこと" do
      expect{|block| form_builder.collection_reference_field(:friends, &block) }.to yield_successive_args(be_a(ReferenceField::Field))
    end

    context "検索条件が設定されていない場合" do
      it "は、空のActionFieldコンテナを返すこと" do
        is_expected.to have_tag(
          ".action_field-container#q_friends-action_field-container",
          with: {"data-object-name" => "q[friends]"},
        ){
          without_tag(".action_field-member")
          without_tag("input[name$='[id]']")
          without_tag("input[name$='[_destroy]']")
        }
      end

      context "オプション引数`:banner_proc`に手続きを渡した場合" do
        it "は、手続きを実行しないこと" do
          expect{|block| form_builder.collection_reference_field(:friends, banner_proc: block) }.to yield_successive_args
        end
      end
    end

    context "検索条件が設定されている場合" do
      let(:ransack){ Person.ransack(friends_id_in: values) }
      let(:values){ [1, 2, 3] }

      it "は、参照フィールドを含むActionFieldコンテナを返すこと" do
        is_expected.to have_tag(
          ".action_field-container#q_friends-action_field-container",
          with: {"data-object-name" => "q[friends]"},
        ){
          values.each_with_index do |value, i|
            with_tag(
              ".action_field-member#q_friends_#{i}-action_field-member",
              with: {"data-object-name" => "q[friends][#{i}]"},
              count: 1,
            ){
              with_tag("label.reference_field-field#q_friends_#{i}-reference_field"){
                with_tag("input.foreign_type#q_friends_#{i}-foreign_type:not([name])", with: {type: "hidden", value: "Person"})
                with_tag("input.foreign_key#q_friends_#{i}-foreign_key", with: {type: "hidden", name: "q[friends_id_in][]", value: value.to_s})
                with_tag(".event_button#q_friends_#{i}-find", with: {"data-event" => "reference_field:find"})
                with_tag(".banner_container"){
                  with_tag(".record_banner.referenced", with: {"data-model" => "Person", "data-record-id" => value.to_s})
                }
              }

              with_tag(".event_button", with: {"data-event" => "action_field:remove", for: "q_friends_#{i}-action_field-member"})
            }
          end

          with_tag(".action_field-member", count: 3)

          without_tag("input[name$='[id]']")
          without_tag("input[name$='[_destroy]']")
        }
      end

      context "オプション引数`:banner_proc`に手続きを渡した場合" do
        it "は、手続き引数に検索条件から作成したレコードを渡すこと" do
          expect{|block| form_builder.collection_reference_field(:friends, banner_proc: block) }.to yield_successive_args(
            *values.map{|value|
              have_attributes(model_name: "Person", id: value)
            },
          )
        end
      end
    end

    it "は、複数コンテナ用のイベントボタンを含むメニュー要素を生成すること" do
      is_expected.to have_tag(".action_field-menu#q_friends-action_field-menu"){
        with_tag(".event_button[data-template]", with: {"data-event" => "action_field:add", for: "q_friends-action_field-container"})
        with_tag(".event_button", with: {"data-event" => "action_field:clear", for: "q_friends-action_field-container"})
      }
    end

    describe "イベントボタンのテンプレート" do
      let(:template_member){ subject.slice(/\bdata-template="(.+?)"/, 1).gsub("&quot;", '"') }

      it "は、未参照状態の参照フィールドを含むメンバー要素であること" do
        expect(template_member).to have_tag(".action_field-member", count: 1){
          with_tag(".reference_field-field"){
            with_tag(".foreign_type:not([name]):not([value])", with: {type: "hidden", pattern: "^Person$"})
            with_tag(".foreign_key:not([value])", with: {type: "hidden", name: "q[friends_id_in][]"})
            with_tag(".event_button", with: {"data-event" => "reference_field:find"})
            with_tag(".banner_container"){
              with_tag(".record_banner.unreferenced")
            }
          }

          with_tag(".event_button", with: {"data-event" => "action_field:remove"})
        }
      end
    end
  end
end
