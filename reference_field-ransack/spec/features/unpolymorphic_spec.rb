require 'rails_helper'

feature "ポリモーフィック非対応", js: true do
  before do
    create_list(:person, 3)
    create_list(:person, 3, parent: parent)
    create_list(:person, 3, :with_parent)

    visit search_people_path(skip_animation: true)
  end

  let(:parent){ create(:person) }

  context "検索用参照フィールドから参照ファインダーを開き、" do
    before do
      click_button "探"
      find(".page-tab:nth-of-type(1)").click
    end

    context "参照先を決定して送信した場合" do
      before do
        find(".event_button[data-model='Person'][data-record-id='#{parent.id}']").click
        click_button "検索"
      end

      scenario "は、条件にあった検索結果の一覧を表示するページへ移動すること" do
        expect(page).to have_selector("h1", text: "People")
        expect(page).to have_selector("tbody > tr", count: parent.children.size)
        parent.children.each do |child|
          expect(page).to have_selector("a[href='/people/#{child.id}/edit']", count: 1)
        end
      end
    end
  end
end
