require 'rails_helper'

feature "ポリモーフィック対応", js: true do
  before do
    create_list(:profile, 3)
    create_list(:profile, 3, profilable: profilable)
    create_list(:profile, 3, :with_company)

    visit search_profiles_path(skip_animation: true)
  end

  let(:profilable){ create(:company) }
  let(:profiles){ Profile.where(profilable_type: profilable.model_name.name, profilable_id: profilable.id) }

  context "検索用参照フィールドから参照ファインダーを開き、" do
    before do
      click_button "探"
      find(".page-tab:nth-of-type(3)").click
    end

    context "参照先を決定して送信した場合" do
      before do
        find(".event_button[data-model='#{profilable.model_name.name}'][data-record-id='#{profilable.id}']").click
        click_button "検索"
      end

      scenario "は、条件にあった検索結果の一覧を表示するページへ移動すること" do
        expect(page).to have_selector("h1", text: "Profiles")
        expect(page).to have_selector("tbody > tr", count: profiles.size)
        profiles.each do |profile|
          expect(page).to have_selector("a[href='/profiles/#{profile.id}/edit']", count: 1)
        end
      end
    end
  end
end
