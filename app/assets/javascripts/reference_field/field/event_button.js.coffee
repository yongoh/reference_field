class ReferenceField.Field.EventButton extends EventButton

  # @return [ReferenceField.Finder.Container] 対応する参照ファインダーコンテナ
  # @raise [ReferenceField.Field.MissingError] 対応する参照ファインダーコンテナ要素が見つからない場合に発生
  finderContainer: ->
    if mwTrigger = @elementOfAction()
      elm = document.getElementById(ModalWindow.SELECTORS.toWindowId(mwTrigger.id))
      new ReferenceField.Finder.Container(elm)
    else
      throw @missingError()

  # @return [ReferenceField.Field.Que] 参照フィールドのキュー
  # @raise [ReferenceField.Field.MissingError] 対応するキュー属性を持つ要素が見つからない場合に発生
  fieldQue: ->
    if mwTrigger = @elementOfAction()
      elm = document.getElementById(ModalWindow.SELECTORS.toWindowId(mwTrigger.id))
      new ReferenceField.Field.Que(elm)
    else
      throw @missingError()

  missingError: ->
    new ReferenceField.Field.MissingError("Missing reference finder. for='#{@elementOfPower().getAttribute("for")}'")

  # @return [ReferenceField.Field] 自身が所属する参照フィールド
  field: ->
    new ReferenceField.Field(@elementOfPower().closest(ReferenceField.Field.SELECTORS.FIELD))
