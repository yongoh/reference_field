class ReferenceField.Field.MissingError extends Error
  constructor: (message)->
    @message = message
    super(message)

  name: "MissingError"
