class ReferenceField.Field.BannerContainer

  # @!attribute [r] element
  #   @return [HTMLSpanElement] バナーコンテナ要素

  constructor: (element)->
    @element = element

  # コンテナにバナー要素を入れる
  setValue: (value)->
    value = value.outerHTML if value instanceof HTMLElement
    @element.innerHTML = value
    Array.prototype.forEach.call @element.children, (child)->
      child.classList.remove(ReferenceField.Field.HTML_CLASSES.CURRENT)
      child.classList.remove(EventButton.HTML_CLASSES.HIGHLIGHT)

  # バナーを検証
  validate: (value)->
    throw new ReferenceField.Field.ValidationError("Banner is required.") unless value
