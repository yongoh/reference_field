class ReferenceField.Field.ValidationError extends Error
  constructor: (message)->
    @message = message
    super(message)

  name: "ValidationError"
