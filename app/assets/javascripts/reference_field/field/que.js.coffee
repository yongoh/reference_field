class ReferenceField.Field.Que

  # @!attribute [r] element
  #   @return [HTMLElement] 参照ファインダーコンテナ要素

  constructor: (element)->
    @element = element

  # @return [Array<String>] 参照フィールド要素のID（文字列）の配列
  _que: ->
    if !@element.dataset.fieldQue || @element.dataset.fieldQue == ""
      []
    else
      @element.dataset.fieldQue.split(ReferenceField.Field.Que.SEPARATOR)

  # 繰り返し
  forEach: (func)->
    String(@element.dataset.fieldQue).split(ReferenceField.Field.Que.SEPARATOR)
      .map((id)-> document.getElementById(id))
      .filter((elm)-> elm)
      .forEach((elm)-> func(new ReferenceField.Field(elm)))

  # @return [Boolean] キューが空かどうか
  isEmpty: ->
    @_que().length == 0

  # キューの末尾にIDを追加
  # @param [Array<String>] args 追加するID（複数可）
  # @return [Number] 追加後のID数
  _push: (args...)->
    que = @_que()
    que.push(args...)
    que = que.filter((val, i, ary)-> ary.indexOf(val) == i)
    @element.dataset.fieldQue = que.join(ReferenceField.Field.Que.SEPARATOR)
    que.length

  # キューの末尾のIDを削除
  # @return [String] 削除したID
  _pop: ->
    que = @_que()
    result = que.pop()
    if que.length > 0
      @element.dataset.fieldQue = que.join(ReferenceField.Field.Que.SEPARATOR)
    else
      delete @element.dataset.fieldQue
    result

  # 参照フィールドをキューに追加。末尾の参照フィールドをカレントにする。
  # @param [Array<ReferenceField.Field>] fields 追加する参照フィールド（複数可）
  # @return [Number] 追加後のキューの数
  push: (fields...)->
    ids = Array.prototype.map.call(fields, (field)-> field.element.id)
    result = @_push(ids...)
    @forEach (field)-> field.current(false)
    if field2 = @currentField()
      field2.current(true)
    result

  # 現在のカレント参照フィールドをキューから削除。次の参照フィールドをカレントにする。
  # @return [ReferenceField.Field] キューから削除した参照フィールド
  pop: ->
    @forEach (field)-> field.current(false)
    field1 = @currentField()
    @_pop()
    if field2 = @currentField()
      field2.current(true)
    field1

  # キューを空にする
  clear: ->
    @forEach (field)-> field.current(false)
    delete @element.dataset.fieldQue

  # @return [ReferenceField.Field] 現在選択されている参照フィールド
  currentField: ->
    que = @_que()
    if elm = document.getElementById(que[que.length - 1])
      new ReferenceField.Field(elm)
    else
      null

ReferenceField.Field.Que.SEPARATOR = ","
