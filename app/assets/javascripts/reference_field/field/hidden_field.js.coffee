class ReferenceField.Field.HiddenField

  # @!attribute [r] element
  #   @return [HTMLInputElement] 入力欄要素

  constructor: (element)->
    @element = element

  # フィールドに値をセット
  # @param [String] value フィールドにセットする値
  setValue: (value)->
    @element.value = value

  # @return [String] `pattern`属性値
  pattern: ->
    @element.getAttribute("pattern")

  # 値を検証
  # @param [String] value 検証する値
  # @raise [ReferenceField.Field.ValidationError] フィールドが必須の場合に値を削除しようとした場合に発生
  # @raise [ReferenceField.Field.ValidationError] `value`がパターンに一致しない場合に発生
  validate: (value)->
    if @element.required && !value
      throw new ReferenceField.Field.ValidationError("'##{@element.id}' is required.")
    pattern = @pattern()
    if value && pattern && !value.match(pattern)
      throw new ReferenceField.Field.ValidationError("Cannot set #{value} as reference.")
