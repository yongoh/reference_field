class ReferenceField.Finder.Container

  # @!attribute [r] element
  #   @return [HTMLElement] 参照ファインダーコンテナ要素
  # @!attribute [r] tabContainer
  #   @return [RadioButtonTab.Container] ラジオボタンコンテナ
  # @!attribute [r] modalWindowTrigger
  #   @return [HTMLInputElement] モーダルウインドウのトリガーとなるチェックボタン要素

  constructor: (element)->
    @element = element
    @tabContainer = new RadioButtonTab.Container(@element.querySelector(RadioButtonTab.SELECTORS.CONTAINER))
    @modalWindowTrigger = document.getElementById(ModalWindow.SELECTORS.toTriggerId(@element.id))

  # @return [ReferenceField.Finder] 現在選択されている参照ファインダーオブジェクト
  currentFinder: ->
    ReferenceField.Finder.build(@tabContainer.currentPageContent())

  # 参照ファインダーコンテナを開く
  # @param [ReferenceField.Field] field カレント参照フィールド
  open: (field)->
    # モーダルウインドウを表示
    @modalWindowTrigger.checked = true
    # 「閉じる」イベントボタンにフォーカス
    @element.querySelector(EventButton.SELECTORS.button("reference_field:close")).focus()
    # 渡された参照フィールドが必須項目の場合は「参照解除」ボタンを無効化する
    @disableDereferenceButtons(field.isRequired())
    # 現在の参照ファインダーのコールバックを実行
    @currentFinder().beforeActivation(field)

  # 「参照解除」ボタンを無効化/有効化
  disableDereferenceButtons: (bool)->
    buttons = @element.querySelectorAll(EventButton.SELECTORS.button("reference_field:dereference"))
    Array.prototype.forEach.call buttons, (button)->
      button.disabled = bool

  # 参照ファインダーコンテナを閉じる
  # @param [ReferenceField.Field.Que] fieldQue 参照フィールドのキュー
  close: (fieldQue)->
    @modalWindowTrigger.checked = false
    if field = fieldQue.currentField()
      field.focus()
    fieldQue.clear()
