class ReferenceField.Finder.EventButton extends EventButton

  # @return [HTMLElement] 自身に対応するバナー要素
  elementOfAction: ->
    super || @finder().getBanner(@elementOfPower().dataset)

  # @return [ReferenceField.Field.Que] 参照フィールドのキュー
  fieldQue: ->
    new ReferenceField.Field.Que(@elementOfPower().closest(ReferenceField.Finder.SELECTORS.CONTAINER))

  # @return [ReferenceField.Finder.Container] 自身が所属する参照ファインダーコンテナ
  finderContainer: ->
    new ReferenceField.Finder.Container(@elementOfPower().closest(ReferenceField.Finder.SELECTORS.CONTAINER))

  # @return [ReferenceField.Finder] 自身が所属する参照ファインダー
  finder: ->
    elm = @elementOfPower().closest(ReferenceField.Finder.SELECTORS.FINDER)
    ReferenceField.Finder.build(elm)

  # カレント参照フィールドへの値のセットとそれに付随する処理
  # @see ReferenceField.Field#setRecord
  setRecord: (record)->
    fieldQue = @fieldQue()
    field = fieldQue.currentField()
    try
      field.setRecord(record)
      fieldQue.pop()
      if fieldQue.isEmpty()
        @finderContainer().close(fieldQue)
      else if finder = field.closestFinder()
        finder.activate()
      field.focus()
    catch error
      if error.name == "ValidationError"
        alert(error.message)
      else
        throw error
