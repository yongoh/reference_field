window.ReferenceField ?= {}

class ReferenceField.Field

  # @!attribute [r] element
  #   @return [HTMLElement] 参照フィールド要素
  # @!attribute [] _findButton
  #   @return [HTMLButtonElement] 「探」イベントボタン要素
  # @!attribute [] _foreignTypeField
  #   @return [ReferenceField.Field.HiddenField] 外部キー（参照先モデル名）フィールド
  # @!attribute [] _foreignKeyField
  #   @return [ReferenceField.Field.HiddenField] 外部キー（レコードID）隠しフィールド
  # @!attribute [] _bannerContainer
  #   @return [ReferenceField.Field.BannerContainer] バナー要素の容れ物

  constructor: (element)->
    @element = element
    @_findButton = @element.querySelector(EventButton.SELECTORS.button("reference_field:find"))
    @_foreignTypeField = new ReferenceField.Field.HiddenField(@element.querySelector(":scope > #{ReferenceField.Field.SELECTORS.FOREIGN_TYPE}"))
    @_foreignKeyField = new ReferenceField.Field.HiddenField(@element.querySelector(":scope > #{ReferenceField.Field.SELECTORS.FOREIGN_KEY}"))
    @_bannerContainer = new ReferenceField.Field.BannerContainer(@element.querySelector(":scope > #{ReferenceField.Field.SELECTORS.BANNER_CONTAINER}"))

  # @return [String,null] 現在の参照先モデル名
  foreignType: ->
    @_foreignTypeField.element.value

  # @return [String,null] 現在の参照先レコードID
  foreignKey: ->
    @_foreignKeyField.element.value

  # @return [Object] 参照元の情報
  owner: ->
    {model: @element.dataset.ownerModel, recordId: @element.dataset.ownerId}

  # @return [Object] 現在の参照先の情報
  target: ->
    {model: @foreignType(), recordId: @foreignKey()}

  # @return [Array] 参照が禁止されたレコードの情報の配列
  prohibitedTargets: ->
    targets = @element.dataset.prohibitedTargets
    if targets == undefined
      []
    else
      targets.split(",").map (target)->
        matched = target.match(/^(.+):(.+?)$/)
        {model: matched[1], recordId: matched[2]}

  # 「カレント」指標の操作
  # @param [Boolean] bool `true`なら指標をつける。`false`なら指標をはずす。
  current: (bool = true)->
    @element.classList.toggle(ReferenceField.Field.HTML_CLASSES.CURRENT, bool)

  # フォーカス状態の操作
  # @param [Boolean] bool `true`ならフォーカスする。`false`ならフォーカスをはずす。
  focus: (bool = true)->
    if bool
      @_findButton.focus(preventScroll: true)
    else
      @_findButton.blur()

  # 参照が禁止されたレコードかどうか検証
  # @param [Object] record 参照先レコード情報
  # @option record [String] 参照先モデル名
  # @option record [Integer] 参照先レコードID
  # @raise [ReferenceField.Field.ValidationError] バリデーションエラーがあった場合に発生
  validateAllowedTarget: (record)->
    if @prohibitedTargets().some((target)-> target.model == record.type && target.recordId == record.id)
      throw new ReferenceField.Field.ValidationError("This record is not allowed to be referenced.")

  # 参照フィールドに参照先レコードをセット
  # @param [Object] record 参照先レコード情報
  # @option record [String] 参照先モデル名
  # @option record [Integer] 参照先レコードID
  # @option record [HTMLElement,String] banner 参照先レコードのバナー要素
  setRecord: (record)->
    # バリデーション
    @validateAllowedTarget(record)
    @_foreignTypeField.validate(record.type)
    @_foreignKeyField.validate(record.id)
    @_bannerContainer.validate(record.banner)
    # 値をフィールドにセット
    @_foreignTypeField.setValue(record.type)
    @_foreignKeyField.setValue(record.id)
    @_bannerContainer.setValue(record.banner)

  # @return [Boolean] ポリモーフィック関連か否か
  isPolymorphic: ->
    !!@_foreignTypeField.element.name

  # @param [Boolean] bool `true`の場合はフィールドを無効化。`false`の場合は有効化。
  disable: (bool = true)->
    @_findButton.disabled = bool
    @_foreignTypeField.element.disabled = bool
    @_foreignKeyField.element.disabled = bool
    @element.classList.toggle("disabled", bool)

  # @return [Boolean] 現在フィールドが無効状態かどうか
  isDisabled: ->
    @_foreignTypeField.element.disabled || @_foreignKeyField.element.disabled

  # @return [Boolean] 現在フィールドが必須状態かどうか
  isRequired: ->
    @_foreignTypeField.element.required || @_foreignKeyField.element.required

  # @return [ReferenceField.Finder] 自身の先祖要素の参照ファインダー
  closestFinder: ->
    if elm = @element.closest(ReferenceField.Finder.SELECTORS.FINDER)
      new ReferenceField.Finder(elm)
    else
      null

  _buffer = 50

ReferenceField.Field.HTML_CLASSES =
  CURRENT: "current"

ReferenceField.Field.SELECTORS =
  FIELD: ".reference_field-field"
  FOREIGN_TYPE: ".foreign_type"
  FOREIGN_KEY: ".foreign_key"
  BANNER_CONTAINER: ".banner_container"
