window.ReferenceField ?= {}

class ReferenceField.Finder

  # @!attribute [r] element
  #   @return [HTMLElement] 参照ファインダー要素
  # @!attribute [r] name
  #   @return [String] 参照ファインダー名

  constructor: (element)->
    @element = element
    @name = element.dataset.finder

  # @param [Object] record レコードの情報を含むオブジェクト
  # @option record [String] model モデル名
  # @option record [String] recordId レコードID
  # @return [HTMLElement] 指定したレコードのバナー要素
  getBanner: (record)->
    @element.querySelector(".record_banner[data-model='#{record.model}'][data-record-id='#{record.recordId}']")

  # @param [Object] record レコードの情報を含むオブジェクト
  # @option record [String] model モデル名
  # @option record [String] recordId レコードID
  # @return [HTMLButtonElement] 指定したレコードに対応する「決定」イベントボタン
  getChoiceButton: (record)->
    @element.querySelector("#{EventButton.SELECTORS.button("reference_field:choice")}[data-model='#{record.model}'][data-record-id='#{record.recordId}']")

  # @return [HTMLInputElement] タブ選択用チェックボックス要素
  checkbox: ->
    @element.closest(ReferenceField.Finder.SELECTORS.CONTAINER).querySelector("input#{ReferenceField.Finder.SELECTORS.FINDER}[data-finder='#{@name}']")

  # 自身のタブを選択
  activate: ->
    @checkbox().checked = true

  # コールバック

  # 「決定」イベント内で参照フィールドにレコードをセットするコールバック
  # @param [EventButton] button 「決定」イベントボタン
  # @yield [record] 決定したレコードを参照フィールドに返す関数
  # @yieldparam [Object] record 参照先レコード情報
  # @yieldoption record [String] 参照先モデル名
  # @yieldoption record [Integer] 参照先レコードID
  # @yieldoption record [HTMLElement,String] banner 参照先レコードのバナー要素
  # @note オーバーライド可
  choice: (button, func)->
    dataset = button.elementOfPower().dataset
    func(
      type: dataset.model
      id: dataset.recordId
      banner: button.elementOfAction()
    )

  # タブでファインダーを選択したときに発動するコールバック
  # @param [ReferenceField.Field] カレント参照フィールド
  # @note オーバーライド可
  beforeActivation: (field)->
    @highlightCurrentRecord(field.target())
    @enableAllChoiceButtons()
    field.prohibitedTargets().forEach (target)=>
      @disableChoiceButton(target)

  # 特定のレコードのバナー要素をハイライト
  # @param [Object] record レコードの情報
  highlightCurrentRecord: (record)->
    Array.prototype.forEach.call @element.querySelectorAll(".record_banner[data-model][data-record-id]"), (banner)->
      bool = banner.dataset.model == record.model && banner.dataset.recordId == record.recordId
      banner.classList.toggle(ReferenceField.Field.HTML_CLASSES.CURRENT, bool)

  # 特定のレコードに対応する「決定」イベントボタンを無効化
  # @param [Object] record レコードの情報
  disableChoiceButton: (record)->
    if button = @getChoiceButton(record)
      button.disabled = true

  # 全ての「決定」イベントボタンを有効化
  enableAllChoiceButtons: ->
    Array.prototype.forEach.call @element.querySelectorAll(EventButton.SELECTORS.button("reference_field:choice")), (button)->
      button.disabled = false

ReferenceField.Finder.SELECTORS =
  CONTAINER: ".reference_field-finders"
  FINDER: ".reference_field-finder"

# @param [HTMLElement] element 参照ファインダー要素
# @return [ReferenceField.Finder] 参照ファインダーオブジェクト
ReferenceField.Finder.build = (element)->
  klass = ReferenceField.Finder.getClass(element.dataset)
  new klass(element)

# @param [Object] dataset 取得する参照ファインダーの情報
# @option dataset [String] jsClass 優先度1。クラス名
# @option dataset [String] finder 優先度2。参照ファインダー名
# @return [Class] 参照ファインダークラス
ReferenceField.Finder.getClass = (dataset)->
  String(dataset.jsClass).constantize() || ((dataset.finder || "ReferenceField.").classify() + "Finder").constantize() || ReferenceField.Finder
