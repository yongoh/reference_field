# 「探」イベントリスナー
document.addEventButtonListener "click", "reference_field:find", class: "ReferenceField.Field.EventButton", (button)->
  try
    field = button.field()
    button.fieldQue().push(field)
    button.finderContainer().open(field)
  catch error
    if error.name == "MissingError"
      alert(error.message)
      field.current(false)
    else
      throw error

# 「閉じる」イベントリスナー
document.addEventButtonListener "click", "reference_field:close", class: "ReferenceField.Finder.EventButton", (button)->
  button.finderContainer().close(button.fieldQue())

# モーダルウインドウ背景をクリックすると参照ファインダーコンテナを閉じる
document.addEventListener "click", (event)->
  return unless event.target.matches("#{ModalWindow.SELECTORS.OVERLAY} > label[for]")
  event.preventDefault()
  elm = document.getElementById(ModalWindow.SELECTORS.toWindowId(event.target.getAttribute("for")))
  finderContainer = new ReferenceField.Finder.Container(elm)
  fieldQue = new ReferenceField.Field.Que(elm)
  finderContainer.close(fieldQue)

# 「参照解除」イベントリスナー
document.addEventButtonListener "click", "reference_field:dereference", class: "ReferenceField.Finder.EventButton", (button)->
  button.setRecord(
    type: null
    id: null
    banner: button.elementOfPower().dataset.banner
  )

# タブ選択時にコールバックを実行
document.addEventListener "change", (event)->
  return unless event.target.matches(ReferenceField.Finder.SELECTORS.FINDER)

  elm = event.target.closest(ReferenceField.Finder.SELECTORS.CONTAINER)
  fieldQue = new ReferenceField.Field.Que(elm)
  finderContainer = new ReferenceField.Finder.Container(elm)

  if field = fieldQue.currentField()
    finderContainer.currentFinder().beforeActivation(field)

# 「決定」イベントリスナー
document.addEventButtonListener "click", "reference_field:choice", class: "ReferenceField.Finder.EventButton", (button)->
  button.finder().choice button, (args...)->
    button.setRecord(args...)
