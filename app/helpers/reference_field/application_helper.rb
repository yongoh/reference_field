module ReferenceField
  module ApplicationHelper
    include FormHelper

    # @overload choice_button()
    #   @return [ActiveSupport::SafeBuffer] 「決定」イベントボタン
    # @overload choice_button(record)
    #   @param [ActiveRecord::Base] record レコード（モデルインスタンス）
    #   @return [ActiveSupport::SafeBuffer] 渡したレコードに対応する「決定」イベントボタン
    def choice_button(*args, **options, &block)
      if args.first.respond_to?(:model_name)
        record = args.pop
        options.html_attribute_merge!(data: {model: record.model_name.name, record_id: record.id})
      end
      event_button("reference_field:choice", *args, options, &block)
    end

    # @param [String] name 参照ファインダーコンテナ名
    # @yield [] 参照ファインダーを作成するブロック
    # @return [ActiveSupport::SafeBuffer] 参照ファインダーコンテナ要素
    def reference_finder(name, &block)
      modal_window(sanitize_to_id(name), class: "reference_field-finders") do
        concat(event_button("reference_field:close", for: sanitize_to_id(name)){ "&#x2716;".html_safe })
        concat(event_button("reference_field:dereference", data: {banner: ReferenceField.unreferenced_field_banner(self)}))
        concat(rbtab("#{name}[tab]", {}, finder: Finder, &block))
      end
    end
  end
end
