class SelectBoxFinder < ReferenceField::Finder

  # @return [ActionView::SafeBuffer] 選択肢要素に対応するラベル要素
  def select_label(*args, &block)
    h.label_tag(unique_id, *args, &block)
  end

  # @param [Enumerable<ActiveRecord::Base>] レコードのコレクション
  # @return [ActionView::SafeBuffer] 選択肢要素
  def select(collection, *args, &block)
    h.content_tag(:select, id: unique_id) do
      collection.each do |object|
        h.concat(ReferenceField::SelectBoxFinder::OptionTagDecorator.new(@template, object).render(*args, &block))
      end
    end
  end
end
