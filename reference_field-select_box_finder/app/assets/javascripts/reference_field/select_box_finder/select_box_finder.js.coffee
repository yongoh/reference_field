class window.SelectBoxFinder extends ReferenceField.Finder

  choice: (button, func)->
    selectElm = button.elementOfAction()
    optionElm = selectElm.querySelector("option[value='#{selectElm.value}']")
    [model, id] = optionElm.value.split(SelectBoxFinder.VALUE_SEPARATOR)
    func(type: model, id: id, banner: optionElm.dataset.banner)

  beforeActivation: (field)->
    super(field)
    value = [field.foreignType(), field.foreignKey()].join(SelectBoxFinder.VALUE_SEPARATOR)
    Array.prototype.forEach.call @element.querySelectorAll("option[value='#{value}']"), (optionElm)->
      optionElm.closest("select").value = optionElm.value

SelectBoxFinder.VALUE_SEPARATOR = ":"
