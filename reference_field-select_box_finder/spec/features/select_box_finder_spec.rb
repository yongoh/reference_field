require 'rails_helper'

feature "SelectBoxFinder", js: true do
  before do
    create_list(:person, 5)
    create_list(:company, 5)
    company
    visit edit_profile_path(profile, skip_animation: true)
  end

  let(:profile){ create(:profile) }
  let(:company){ create(:company) }

  context "関連'profilable'の参照フィールドから参照ファインダーを開いて選択肢参照ファインダーを選択した場合" do
    before do
      click_button "探"
      find(".page-tab[data-finder='select_box']").click
    end

    scenario "は、現在の関連先が選択されていること" do
      value = ["Person", profile.profilable_id].join(ReferenceField::SelectBoxFinder::VALUE_SEPARATOR)
      expect(find_field("人物,Company").value).to eq(value)
    end

    context "参照先を決定して送信した場合" do
      before do
        select company.name, from: "人物,Company"
        click_button "決定"
      end

      subject do
        proc{ click_button "Update Profile" }
      end

      scenario "は、選択した要素を関連先にセットすること" do
        expect(subject).to change{ profile.reload.profilable }.from(profile.profilable).to(company)
      end

      scenario "は、更新成功の通知を表示すること" do
        subject.call
        expect(page).to have_selector("#notice", text: "Profile was successfully updated.")
      end
    end
  end
end
