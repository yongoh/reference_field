require 'rails_helper'

describe SelectBoxFinder do
  let(:finder){ described_class.new(view_context, nil, "select_box") }

  describe "#select_label" do
    context "引数無しの場合" do
      it "は、`for`属性値を自然言語化した内容を持つラベル要素を返すこと" do
        expect(finder.select_label).to have_tag("label", with: {for: finder.unique_id}, text: finder.unique_id.humanize)
      end
    end

    context "引数を渡した場合" do
      it "は、第1引数を内容として持つラベル要素を返すこと" do
        expect(finder.select_label("(´･ω･｀)")).to have_tag("label", with: {for: finder.unique_id}, text: "(´･ω･｀)")
      end
    end
  end

  describe "#select" do
    let(:people){ create_list(:person, 5) }

    it "は、第1引数に渡したコレクションのオプション要素群を内容に持つ選択肢要素を返すこと" do
      expect(finder.select(people)).to have_tag("select:not([name])", with: {id: finder.unique_id}){
        with_tag("option", count: people.size)
        people.each do |person|
          with_option(/Person id: #{person.id},/)
        end
      }
    end

    context "第2引数以降を渡した場合" do
      subject do
        finder.select(people, :name)
      end

      it "は、第2引数以降をオプション要素生成メソッドに委譲すること" do
        is_expected.to have_tag("select:not([name])", with: {id: finder.unique_id}){
          with_tag("option", count: people.size)
          people.each do |person|
            with_option(person.name)
          end
        }
      end
    end

    context "ブロックを渡した場合" do
      subject do
        finder.select(people) do
          h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
          h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
        end
      end

      it "は、ブロックをオプション要素生成メソッドに委譲すること" do
        is_expected.to have_tag("select:not([name])", with: {id: finder.unique_id}){
          with_tag("option", with: {"data-banner" => '<div id="shobon">(´･ω･｀)</div><span class="boon">（＾ω＾）</span>'}, count: people.size)
        }
      end
    end
  end
end
