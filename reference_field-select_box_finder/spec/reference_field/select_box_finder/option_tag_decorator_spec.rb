require 'rails_helper'

describe ReferenceField::SelectBoxFinder::OptionTagDecorator do
  let(:decorator){ described_class.new(view_context, person) }
  let(:person){ create(:person) }

  describe "#render" do
    describe "ブロック" do
      let(:banner){ decorator.render.slice(/\bdata-banner="(.*?)"/, 1) }

      context "を渡さない場合" do
        it "は、デフォルトの`data-banner`属性値を持つオプション要素を生成すること" do
          expect(banner.gsub('&quot;', '"')).to have_tag(".record_banner", with: {"data-model" => "Person", "data-record-id" => person.id})
        end
      end

      context "を渡した場合" do
        it "は、ブロック引数にレコードを渡すこと" do
          expect{|block| decorator.render(&block) }.to yield_with_args(equal(person))
        end

        subject do
          decorator.render do
            h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
            h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
          end
        end

        it "は、ブロック内で作成した要素を`data-banner`属性に持つオプション要素を生成すること" do
          is_expected.to have_tag("option", with: {"data-banner" => '<div id="shobon">(´･ω･｀)</div><span class="boon">（＾ω＾）</span>'})
        end
      end
    end

    describe "第1引数" do
      context "を渡さない場合" do
        it "は、`#object.inspect`の結果を内容に持つオプション要素を生成すること" do
          expect(decorator.render{}).to have_tag("option", text: person.inspect)
        end
      end

      context "にシンボルを渡した場合" do
        it "は、渡した名前の`#object`のメソッドの結果を内容に持つオプション要素を生成すること" do
          expect(decorator.render(:name){}).to have_tag("option", text: person.name)
        end
      end

      context "に手続きを渡した場合" do
        it "は、手続きの結果を内容に持つオプション要素を生成すること" do
          expect(decorator.render(proc{ "(´･ω･｀)" }){}).to have_tag("option", text: "(´･ω･｀)")
        end

        it "は、手続きの引数に`#object`を渡すこと" do
          expect{|block| decorator.render(proc(&block)){} }.to yield_with_args(equal(person))
        end
      end

      context "に文字列を渡した場合" do
        it "は、渡した文字列を内容に持つオプション要素を生成すること" do
          expect(decorator.render("（＾ω＾）"){}).to have_tag("option", text: "（＾ω＾）")
        end
      end
    end

    describe "オプション`:value`" do
      context "を渡さない場合" do
        it "は、デフォルトの`value`属性値を持つオプション要素を生成すること" do
          expect(decorator.render{}).to have_tag("option", with: {value: "Person:#{person.id}"})
        end
      end

      context "にシンボルを渡した場合" do
        it "は、渡した名前の`#object`のメソッドの結果を`value`属性値に持つオプション要素を生成すること" do
          expect(decorator.render(value: :name){}).to have_tag("option", with: {value: person.name})
        end
      end

      context "に手続きを渡した場合" do
        it "は、手続きの結果を`value`属性値に持つオプション要素を生成すること" do
          expect(decorator.render(value: proc{ "(´･ω･｀)" }){}).to have_tag("option", with: {value: "(´･ω･｀)"})
        end

        it "は、手続きの引数に`#object`を渡すこと" do
          expect{|block| decorator.render(value: proc(&block)){} }.to yield_with_args(equal(person))
        end
      end

      context "に文字列を渡した場合" do
        it "は、渡した文字列を`value`属性値に持つオプション要素を生成すること" do
          expect(decorator.render(value: "（＾ω＾）"){}).to have_tag("option", with: {value: "（＾ω＾）"})
        end
      end
    end

    context "HTML属性ハッシュを渡した場合" do
      it "は、渡した文字列を`value`属性値に持つオプション要素を生成すること" do
        expect(decorator.render(id: "my-id", class: "my-class"){}).to have_tag("option#my-id.my-class")
      end
    end
  end
end
