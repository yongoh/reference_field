# ReferenceField::SelectBoxFinder
Short description and motivation.

## Usage
How to use my plugin.

### ジェネレータ
```sh
$ rails generate select_box_finder person
      create  app/views/people/finders/_select_box.html.erb
      invoke  rspec
      create    spec/views/people/finders/_select_box.html.erb_spec.rb
```
### 自分で追記
```erb
<!-- app/views/people/_form.html.erb -->

<div class="field">
  <%= form.reference_label :parent %>
  <%= form.reference_field :parent %>
</div>
```
```erb
<!-- app/views/people/_form.html.erb -->

<%= reference_finder "people" do |x| %>
  <%= x.finder("select_box:people") %>
<% end %>
```

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'reference_field-select_box_finder'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install reference_field-select_box_finder
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
