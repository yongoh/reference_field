module ReferenceField
  module SelectBoxFinder
    class OptionTagDecorator

      # @!attribute [r] object
      #   @return [ActiveRecord::Base] モデルインスタンス
      attr_reader :object

      # @param [ActionView::Base] ビューコンテキスト
      def initialize(template, object)
        @object, @template = object, template
      end

      # @param [Object] content 要素の内容
      # @param [Object] value 要素の`value`属性値
      # @param [Hash] HTML属性ハッシュ
      # @yield [] `data-banner`属性値を作るブロック
      # @return [ActionView::SafeBuffer] オプション要素
      def render(content = :inspect, value: proc{ default_value }, **html_attributes, &block)
        banner = block_given? ? h.capture(object, &block) : default_banner
        html_attributes = {value: object_send(value), data: {banner: banner}}.html_attribute_merge(html_attributes)
        h.content_tag(:option, object_send(content), html_attributes)
      end

      private

      def h
        @template
      end

      def object_send(arg)
        case arg
        when Symbol then object.public_send(arg)
        when Proc then arg.call(object)
        else arg
        end
      end

      def default_value
        [object.model_name.name, object.id].join(VALUE_SEPARATOR)
      end

      def default_banner
        h.record_banner(object)
      end
    end
  end
end
