require "reference_field/select_box_finder/engine"
require "reference_field/select_box_finder/option_tag_decorator"

module ReferenceField
  module SelectBoxFinder
    VALUE_SEPARATOR = ":".freeze
  end
end
