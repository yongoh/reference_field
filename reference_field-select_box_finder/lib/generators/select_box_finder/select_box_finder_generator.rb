require 'rails/generators/named_base'

module Rails
  module Generators
    class SelectBoxFinderGenerator < NamedBase
      include ResourceHelpers

      source_root File.expand_path("../templates", __FILE__)

      def create_view_file
        template "_select_box.html.erb", "app/views/#{controller_file_path}/finders/_select_box.html.erb"
      end

      hook_for :test_framework
    end
  end
end
