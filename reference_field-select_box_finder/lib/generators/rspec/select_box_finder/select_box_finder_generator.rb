require 'generators/rspec'

module Rspec
  module Generators
    class SelectBoxFinderGenerator < Base
      include Rails::Generators::ResourceHelpers

      source_root File.expand_path("../templates", __FILE__)

      def create_view_spec_file
        template "_select_box.html.erb_spec.rb.erb", "spec/views/#{controller_file_path}/finders/_select_box.html.erb_spec.rb"
      end
    end
  end
end
