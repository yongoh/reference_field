# ReferenceField
Short description and motivation.

## 参照フィールドの使い方
```erb
<!-- app/views/people/_form.html.erb -->

<div class="field">
  <%= form.reference_label :parent %>
  <%= form.reference_field :parent %>
</div>
```
### 参照先の初期値（レコード）を渡す
```rb
person = Person.create
form.reference_field(:parent, person)
```
### 自己参照を禁止
```rb
form.reference_field(:parent, self_referencing: false)
```
`form.object`のレコードを選択できなくなる
### 使用する参照ファインダーコンテナを指定
```rb
# オプション`for`に参照ファインダーコンテナ名を渡す
form.reference_field(:parent, for: "hoge")
# 同じ名前の参照ファインダーに対応
reference_finder("hoge")
```
### 所属するフォームを指定
```erb
<form id="my-form">...</form>

<!-- 所属するフォームのID属性値を指定 -->
<%= form.reference_field(:parent, form: "my-form") %>
```
### フィールドの状態を指定
```rb
# 必須フィールドにする
form.reference_field(:parent, required: true)
# 無効状態にする
form.reference_field(:parent, disabled: true)
# 書き換え不可状態にする
form.reference_field(:parent, readonly: true)
# ページ読み込み時にこのフィールド（の「探」ボタン）に自動的にフォーカス
form.reference_field(:parent, autofocus: true)
```
### ポリモーフィック対応にする
```rb
form.reference_field(:parent, polymorphic: true)
```
`name`属性を持つforeign_type隠し入力欄を含む参照フィールドを生成
### 各要素のID属性値のプレフィックスを指定
```rb
form.reference_field(:parent, id_prefix: "hoge")
form.reference_field(:parent, id_prefix: "piyo")
```
同じID属性値がかぶる場合などに使用
### 参照フィールド内の各要素を直接設定
```rb
form.reference_field(:parent) do |field|
  # 隠し入力欄の`name`属性値をセット
  field.foreign_key.name = "foo[bar][baz_id]"

  # ボタンの内容をセット
  field.button.content = "Find"

  # 対応する参照ファインダーコンテナ名をセット
  field.for = "hoge"
end
```

## 参照ファインダーの使い方
```erb
<!-- app/views/people/_form.html.erb -->

<%= reference_finder "people" do |x|
  x.finder("hoge")
end %>
```
### 参照ファインダークラスの優先順位
* 参照ファインダー名のみで指定
  ```rb
  x.finder("hoge")
  ```
  1. `HogeFinder`
  1. `ReferenceField::Finder`
  ```rb
  x.finder("foo/bar/baz")
  ```
  1. `Foo::Bar::BazFinder`
  1. `ReferenceField::Finder`
* 参照ファインダークラスを渡す
  ```rb
  PiyoFinder = Class.new(ReferenceField::Finder)
  x.finder("hoge", finder_class: PiyoFinder)
  ```
  1. `PiyoFinder`
### 部分テンプレートの選択
* 参照ファインダー名のみで指定
  ```rb
  x.finder("hoge")
  ```
  1. app/views/application/finders/_hoge.html.erb
* 部分テンプレートのディレクトリを指定
  ```rb
  # どちらの方法も同等
  x.finder("hoge", templates: {dir: "foo/bar/baz"})
  x.finder("hoge:foo/bar/baz")
  ```
  1. app/views/foo/bar/baz/finders/_hoge.html.erb
* 部分テンプレートのファイル名を指定
  ```rb
  # どちらの方法も同等
  x.finder("hoge", templates: {file: "foo/bar/baz"})
  x.finder("hoge#foo/bar/baz")
  ```
  1. app/views/application/finders/foo/bar/_baz.html.erb

  パス名は`ActionView::Base#render`の仕様で展開される
* 部分テンプレートのパスを指定
  ```rb
  x.finder("hoge", templates: "foo/bar/baz")
  ```
  1. app/views/foo/bar/_baz.html.erb

  パスは`ActionView::Base#render`の仕様で展開される
* 部分テンプレートを複数指定
  ```rb
  x.finder("hoge:people", templates: [{dir: "animals"}, "foo/bar/baz", {file: "piyo"}])
  ```
  1. app/views/animals/finders/_hoge.html.erb
  1. app/views/foo/bar/_baz.html.erb
  1. app/views/application/finders/_piyo.html.erb
  1. app/views/people/finders/_hoge.html.erb

  配列の先頭から実行。区切り文字で指定したパスは末尾に。
### 部分テンプレートにローカル変数を渡す
```rb
x.finder("hoge", locals: {model: Person})
```
```erb
<!-- app/views/application/finders/_hoge.html.erb -->

<%= model.model_name.human %><!-- #=> Person -->
```

## 参照ファインダージェネレータの使い方
```sh
$ rails d reference_field:finder hoge
      create  app/finders/hoge_finder.rb
      create  app/views/application/finders/_hoge.html.erb
      create  app/assets/javascripts/finders/hoge_finder.js.coffee
      create  app/assets/stylesheets/finders/hoge.css.scss
      invoke  rspec
      create    spec/finders/hoge_finder_spec.rb
      create    spec/views/application/finders/_hoge.html.erb_spec.rb
```
### app/views/application/finders/_hoge.html.erb
参照ファインダーの本文を記述する部分テンプレート
* ```erb
  <%= finder.name %>
  ```
  参照ファインダー名
* ```erb
  <%= finder.human_name %>
  ```
  翻訳された参照ファインダー名
* ```erb
  <%= choice_button(record) %>
  ```
  レコード`record`を選択するための「決定」ボタン
### app/views/people/finders/_hoge.html.erb（必要に応じて自分で作成）
特定のコントローラ用の参照ファインダー部分テンプレート
* 上記より優先して読み込まれる
### app/finders/hoge_finder.rb
上記部分テンプレートの変数`finder`のクラス
* ここで定義したメソッドが部分テンプレートで使える
* **削除しても機能する**
  * 基底クラス`ReferenceField::Finder`が代わりに使われる
### app/assets/javascripts/finders/hoge_finder.js.coffee
参照ファインダーに関するイベントに対応する処理を記述するスクリプト
* `#choice`
  「決定」ボタンを押した際の処理
* `#beforeActivation`
  参照ファインダーページを選択した際の処理
* **削除しても機能する**
  * 基底クラス`ReferenceField.Finder`が代わりに使われる

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'reference_field'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install reference_field
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
