require "reference_field/engine"

require "reference_field/field"
require "reference_field/field/element"
require "reference_field/field/label"
require "reference_field/field/hidden_field"
require "reference_field/field/find_event_button"
require "reference_field/field/banner_container"

require "reference_field/finder"

require "reference_field/form_builder"
require "reference_field/form_helper"
require "reference_field/self_reference"

module ReferenceField
  class << self

    # @return [ActiveSupport::SafeBuffer] 参照バナー要素
    def referenced_field_banner(template, *args, **options, &block)
      options.html_attribute_merge!(class: "referenced")
      template.record_banner(*args, options, &block)
    end

    # @return [ActiveSupport::SafeBuffer] 未参照のバナー要素
    def unreferenced_field_banner(template)
      template.content_tag(:span, template.t(:unreferenced, scope: "reference_field"), class: %w(record_banner unreferenced))
    end
  end
end
