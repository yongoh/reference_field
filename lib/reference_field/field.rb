module ReferenceField
  class Field

    # @return [Array<Symbol>] 状態オプション名の配列
    STATUS_OPTIONS = %i(required disabled readonly polymorphic).freeze

    # @return [Array<Symbol>] 属性オプション名の配列
    ATTRIBUTE_OPTIONS = %i(for form id_prefix).freeze

    class << self

      # @param [ActionView::Base] template ビューコンテキスト
      # @param [String] object_name フィールド名のオブジェクト部分
      # @param [String] association_name フィールド名の関連名部分
      # @return [ReferenceField::Field::Field] 参照フィールドオブジェクト
      def association(template, object_name, association_name)

        # 参照フィールドオブジェクトを作成
        field = new(template)

        # 隠し入力欄の`name`属性値をセット
        field.foreign_type.name = "#{object_name}[#{association_name}_type]"
        field.foreign_key.name = "#{object_name}[#{association_name}_id]"

        # 各要素IDのプレフィックスをセット
        field.id_prefix = [template.send(:sanitize_to_id, object_name), association_name].join("_")

        # 対応する参照ファインダーコンテナ名をセット
        field.for = template.send(:sanitize_to_id, association_name)

        field
      end
    end

    # @!attribute [rw] autofocus
    #   @return [Boolean] 自動フォーカス設定の有無
    # @!attribute [rw] for
    #   @return [String] 対応する参照ファインダーコンテナを示す属性値
    delegate :autofocus?, :autofocus=, :for, :for=, to: :button
    delegate :prohibited_targets=, to: :label

    def initialize(template)
      @template = template
    end

    # @return [ActionView::SafeBuffer] 参照フィールド要素
    def render(&block)
      label.render do
        h.concat(foreign_type.render)
        h.concat(foreign_key.render)
        h.concat(button.render)
        h.concat(banner_container.render(&block))
      end
    end

    # @return [ReferenceField::Field::Label] ラベル要素オブジェクト
    def label
      @label ||= Label.new(@template)
    end

    # @return [ReferenceField::Field::HiddenField] 外部キー（参照先モデル名）隠し入力欄オブジェクト
    def foreign_type
      @foreign_type ||= HiddenField.new(@template, class: "foreign_type")
    end

    # @return [ReferenceField::Field::HiddenField] 外部キー（参照先レコードID）隠し入力欄オブジェクト
    def foreign_key
      @foreign_key ||= HiddenField.new(@template, class: "foreign_key")
    end

    # @return [ReferenceField::Field::FindEventButton] 「探」イベントボタン要素オブジェクト
    def button
      @button ||= FindEventButton.new(@template)
    end

    def banner_container
      @banner_container ||= BannerContainer.new(@template)
    end

    # `true`を渡すと必須フィールドに、`false`を渡すと非必須フィールドにする
    def required=(arg)
      label.required = arg
      foreign_type.required = arg
      foreign_key.required = arg
    end

    # @return [Boolean] 隠し入力欄が必須フィールドかどうか
    def required?
      foreign_type.required? || foreign_key.required?
    end

    # `true`を渡すとフィールドを無効に、`false`を渡すと有効にする
    def disabled=(arg)
      label.disabled = arg
      foreign_type.disabled = arg
      foreign_key.disabled = arg
      button.disabled = arg
    end

    # @return [Boolean] 隠し入力欄が無効フィールドかどうか
    def disabled?
      foreign_type.disabled? || foreign_key.disabled?
    end

    # `true`を渡すと読み取り専用に、`false`を渡すと読み書き可能に設定する
    def readonly=(arg)
      label.readonly = arg
      foreign_type.readonly = arg
      foreign_key.readonly = arg
      button.disabled = arg
    end

    # @return [Boolean] 隠し入力欄が読み取り専用フィールドかどうか
    def readonly?
      foreign_type.readonly? || foreign_key.readonly?
    end

    # @param [Boolean] bool ポリモーフィック関連用参照フィールドにするかどうか
    def polymorphic=(bool)
      hash = foreign_type.html_attributes
      if bool
        hash[:name] = hash[:data].delete(:name) if !hash.has_key?(:name) && hash[:data].has_key?(:name)
      else
        hash[:data][:name] = hash.delete(:name) if hash.has_key?(:name)
      end
    end

    # @return [Boolean] ポリモーフィック関連用参照フィールドであるかどうか
    def polymorphic?
      !!foreign_type.name
    end

    # @return [Boolean] 参照先が設定されている場合
    def referenced?
      foreign_type.referenced? && foreign_key.referenced?
    end

    # ラベル要素のID属性値とイベントボタンとの対応関係をセット
    def id_prefix=(arg)
      label.id = [arg, :reference_field].join("-")
      label.for = FindEventButton.id_format(arg)
      foreign_type.id = [arg, :foreign_type].join("-")
      foreign_key.id = [arg, :foreign_key].join("-")
      button.id = FindEventButton.id_format(arg)
    end

    # 隠しフィールドに`form`属性を追加
    def form=(arg)
      foreign_type.form = arg
      foreign_key.form = arg
    end

    # @return [ActiveRecord::Base,nil] 参照先レコード
    def target
      banner_container.record
    end

    # 参照先レコードをセット
    def target=(record)
      banner_container.record = record
      if record && record.model_name
        foreign_type.value = record.model_name.name
        foreign_key.value = record.id
      else
        foreign_type.value = nil
        foreign_key.value = nil
      end
    end

    # ポリモーフィック設定と対応する参照ファインダーコンテナ名をセット
    # @param [ActiveRecord::Reflection::AssociationReflection,nil] ref 関連の種類オブジェクト
    def reflection=(ref)
      unless ref.polymorphic?
        foreign_type.pattern = [ref.klass.model_name.name]
        self.for = ref.klass.model_name.route_key
      end
      self.polymorphic = ref.polymorphic?
    end

    # オプションを適用・ブロックに自身を渡して実行
    # REVIEW: メソッド名がふさわしくない
    # REVIEW: そもそも1つのメソッドにこれら複数の処理をまとめるべきなのかどうか
    def options_set(options)
      # ポリモーフィック設定のデフォルト
      options.fetch(:polymorphic){ self.polymorphic = false }

      yield(self) if block_given?

      # `xxx=`記法のオプション群をセット
      options.each do |option_name, value|
        send("#{option_name}=", value)
      end
    end

    private

    def h
      @template
    end
  end
end
