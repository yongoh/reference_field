module ReferenceField
  class Engine < ::Rails::Engine

    initializer "extend ActionView" do
      ActionView::Helpers::FormBuilder.prepend(FormBuilder)
    end

    initializer "extend ActionField" do
      ActionField::MemberFormBuilder.prepend(SelfReference)
    end
  end
end
