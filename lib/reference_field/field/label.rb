module ReferenceField
  class Field
    class Label < Element

      html_attr_accessor :id, :for

      # @return [ActionView::SafeBuffer] ラベル要素
      def render(&block)
        h.content_tag(:label, html_attributes, &block)
      end

      # 特定のレコードへの参照を禁止する指標を付与
      # @param [Array<ActiveRecord::Base>] records 参照を禁止するレコード群
      def prohibited_targets=(records)
        str = records.select(&:persisted?).map{|record| [record.model_name.name, record.id].join(":") }.join(",")
        html_attributes.html_attribute_merge!(data: {prohibited_targets: str})
      end

      STATUS_OPTIONS.each do |status|

        # 状態指標をつける・はずす
        # @param [Boolean] bool `true`を渡したら指標をつける。`false`ならはずす。
        define_method("#{status}=") do |bool|
          html_attributes.html_attribute_merge!(class: status.to_s)
          html_attributes[:class].delete(status.to_s) unless bool
        end
      end

      private

      def default_html_attributes
        {class: "reference_field-field"}
      end
    end
  end
end
