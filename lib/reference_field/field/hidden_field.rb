module ReferenceField
  class Field
    class HiddenField < Element

      html_attr_accessor :id, :value, :name, :pattern, :form
      boolean_html_attr_accessor :required, :disabled, :readonly

      # @return [ActionView::SafeBuffer] 隠し入力欄要素
      def render
        h.tag(:input, html_attributes)
      end

      # `pattern`属性値をセット
      def pattern=(arg)
        html_attributes.merge!(
          pattern: arg.respond_to?(:join) ? "^#{arg.join("|")}$" : arg.to_s,
        )
      end

      # @return [Boolean] 参照先が設定されている場合
      def referenced?
        !!value
      end

      private

      def default_html_attributes
        {type: "hidden"}
      end
    end
  end
end
