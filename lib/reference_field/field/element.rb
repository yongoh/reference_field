module ReferenceField
  class Field
    class Element

      class << self

        # HTML属性値アクセサメソッドを定義
        def html_attr_accessor(*method_names)
          method_names.each do |method_name|

            # HTML属性値ゲッターメソッド
            define_method(method_name) do
              html_attributes[method_name]
            end

            # HTML属性値セッターメソッド
            define_method("#{method_name}=") do |arg|
              html_attributes[method_name] = arg
            end
          end
        end

        # 真偽値HTML属性アクセサメソッドを定義
        def boolean_html_attr_accessor(*method_names)
          method_names.each do |method_name|

            # HTML属性値ゲッターメソッド
            define_method("#{method_name}?") do
              !!html_attributes[method_name]
            end

            # HTML属性値セッターメソッド
            define_method("#{method_name}=") do |arg|
              if arg
                html_attributes[method_name] = true
              else
                html_attributes.delete(method_name.to_sym)
              end
            end
          end
        end
      end

      # @!attribute [rw] html_attributes
      #   @return [Hash] HTML属性
      attr_accessor :html_attributes

      # @param [ActionView::Base] template ビューコンテキスト
      # @param [Hash] html_attributes HTML属性の初期値
      def initialize(template, **html_attributes)
        @template = template
        self.html_attributes = default_html_attributes.html_attribute_merge(html_attributes, compact: false)
      end

      private

      def h
        @template
      end

      def default_html_attributes
        {}
      end
    end
  end
end
