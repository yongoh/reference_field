module ReferenceField
  class Field
    class BannerContainer < Element

      # @!attribute [rw] record
      #   @return [ActiveRecord::Base,nil] コンテナに入れるバナーのレコード
      attr_accessor :record

      # @return [ActionView::SafeBuffer] バナー入りコンテナ要素
      def render(&block)
        h.content_tag(:span, banner(&block), html_attributes)
      end

      # @yield [record] バナー要素作成ブロック
      # @yieldparam [ActiveRecord::Base] record 参照先レコード
      # @return [ActionView::SafeBuffer] バナー要素
      def banner(&block)
        if record.nil?
          ReferenceField.unreferenced_field_banner(@template)
        elsif block_given?
          h.capture(record, &block)
        else
          ReferenceField.referenced_field_banner(@template, record)
        end
      end

      private

      def default_html_attributes
        {class: "banner_container"}
      end
    end
  end
end
