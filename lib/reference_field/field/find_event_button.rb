module ReferenceField
  class Field
    class FindEventButton < Element

      class << self

        # @return [String] イベントボタンのID属性値用文字列
        def id_format(arg)
          [arg, :find].join("-")
        end
      end

      # @!attribute [rw] content
      #   @return [String] イベントボタン要素の内容
      attr_writer :content

      html_attr_accessor :id, :for
      boolean_html_attr_accessor :disabled, :autofocus
      alias_method :readonly?, :disabled?
      alias_method :readonly=, :disabled=

      # @return [ActionView::SafeBuffer] 「探」イベントボタン要素
      def render
        h.event_button("reference_field:find", content, html_attributes)
      end

      # @return [String] ボタン要素の内容もしくはその訳文キー
      def content
        @content ||= EventButton.humanize_name("reference_field:find")
      end

      # `for`属性値（参照ファインダーコンテナのスイッチのID属性値に対応）をセット
      def for=(arg)
        html_attributes[:for] = [h.send(:sanitize_to_id, arg.to_s.gsub("/", "_")), "trigger"].join("-")
      end
    end
  end
end
