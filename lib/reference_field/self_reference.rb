module ReferenceField
  module SelfReference

    class << self

      # @param [Hash] options 分割前のオプション群
      # @return [Array<Hash>] ActionField用オプション群・参照フィールド用オプション群の順に含む配列
      def options_partition(options)
        options = options.reverse_merge(include_id: false, collection: true)
        options.fetch(:templates){|key| options[key] = [RecordBanner::DummyRecord.new(nil, nil)] }

        [
          options.extract!(:include_id, :collection, :templates),
          options,
        ]
      end
    end

    def self_reference_field_object
      field = Field.new(@template)

      # 隠し入力欄の`name`属性値をセット
      re = /\[(\w+?)(_attributes)?\]\[#{index}\]\z/
      field.foreign_type.name = object_name.to_s.sub(re){ "[#{$1.to_s.singularize}_types][]" }
      field.foreign_key.name = object_name.to_s.sub(re){ "[#{$1.to_s.singularize}_ids][]" }

      # 各要素IDのプレフィックスをセット
      field.id_prefix = @template.send(:sanitize_to_id, object_name)

      # 参照先レコードをセット
      field.target = object unless object.model_name.nil?

      field
    end
  end
end
