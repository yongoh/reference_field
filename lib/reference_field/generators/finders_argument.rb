module ReferenceField
  module Generators
    module FindersArgument

      def self.included(klass)
        super(klass)
        klass.argument :finders, type: :array, default: [], banner: 'finder:dir#file finder:dir#file'
      end

      private

      Finder = Struct.new(:name, :dir, :file) do
        def to_s
          str = [name, dir].join(ReferenceField::Finder::SEPARATORS[:dir]) if dir
          str = [str, file].join(ReferenceField::Finder::SEPARATORS[:file]) if file
          str
        end
      end

      def _finders
        finders.map do |arg|
          path = ReferenceField::Finder.parse(arg)
          Finder.new(path[:finder], path.fetch(:dir, controller_name), path.fetch(:file, path[:finder]))
        end
      end
    end
  end
end
