module ReferenceField
  module FormHelper

    # @param [String] object_name フィールド名のオブジェクト部分
    # @param [String] association_name フィールド名の関連名部分
    # @param [ActiveRecord::Base,nil] record 参照先レコード
    # @param [Proc,nil] banner_proc 参照先レコードからバナー要素を作る手続き
    # @yield [field] 参照フィールドをより詳しく定義するブロック
    # @yieldparam [ReferenceField::Field] field 参照フィールドインスタンス
    # @return [ActiveSupport::SafeBuffer] 参照フィールド
    # @see ReferenceField::Field::association
    def reference_field(object_name, association_name, record = nil, banner_proc: nil, **options, &block)
      field = Field.association(self, object_name, association_name)
      field.target = record
      field.options_set(options, &block)

      field.render(&banner_proc)
    end

    # @param [String] object_name フィールド名のオブジェクト部分
    # @param [String] association_name フィールド名の関連名部分
    # @param [Array<ActiveRecord::Base>] records 参照先レコード群
    # @param [Hash] options ActionField用・参照フィールド用が混合したオプション群
    # @param [Proc,nil] banner_proc 参照先レコードからバナー要素を作る手続き
    # @yield [field] 参照フィールドをより詳しく定義するブロック
    # @yieldparam [ReferenceField::Field] field 参照フィールドインスタンス
    # @return [ActiveSupport::SafeBuffer] 複数関連対応参照フィールド
    def collection_reference_field(object_name, association_name, records = [], banner_proc: nil, **options, &block)
      af_options, rf_options = SelfReference.options_partition(options)

      action_field("#{object_name}[#{association_name}]", records, af_options) do |mf|
        field = mf.self_reference_field_object
        field.options_set(rf_options, &block)

        concat(field.render(&banner_proc))
        concat(mf.remove_button)
      end
    end

    # @param [String] object_name フィールド名のオブジェクト部分
    # @param [String] association_name フィールド名の関連名部分
    # @param [String] for_prefix `for`属性値の一部
    # @param [Hash] html_attributes HTML属性
    # @return [ActiveSupport::SafeBuffer] ラベル要素
    def reference_label(object_name, association_name, for_prefix: nil, **html_attributes, &content_proc)
      html_attributes.fetch(:for) do
        for_prefix ||= [sanitize_to_id(object_name), association_name].join("_")
        html_attributes[:for] = Field::FindEventButton.id_format(for_prefix)
      end
      label(object_name, association_name, html_attributes, &content_proc)
    end
  end
end
