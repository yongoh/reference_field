require 'radio_button_tab/page'

module ReferenceField
  class Finder < RadioButtonTab::Page

    # @return [String] 部分テンプレートファイルを含むコントローラディレクトリのデフォルト
    DEFAULT_CONTROLLER_DIR = "application".freeze

    # @return [String] 部分テンプレートファイルを含むディレクトリ名のデフォルト
    MIDDLE_DIR = "finders".freeze

    # @return [Hash<String>] 参照ファインダー名の区切り文字セット
    SEPARATORS = {
      dir: ":".freeze,
      file: "#".freeze,
    }.freeze

    class << self

      # @param [String] name 参照ファインダー名
      # @param [Array<String,Hash>,String,Hash] templates 部分テンプレートのパスの配列
      # @option options [Class] :finder_class 参照ファインダークラス
      # @return [ReferenceField::Finder] 参照ファインダーインスタンス
      def build(*args, name, templates: [], **options, &block)
        # 区切り文字付き参照ファインダー名を分割
        path = parse(name)

        # 参照ファインダークラスを取得
        finder_class = options.fetch(:finder_class){ class_get(path[:finder]) }

        # 部分テンプレートパスをセット
        templates = [templates] unless templates.is_a?(Array)
        templates.push(path)

        finder_class.new(*args, path[:finder], templates: templates, **options, &block)
      end

      # @param [String] name 参照ファインダー名
      # @return [Class<ReferenceField::Finder>] 参照ファインダークラス
      def class_get(name)
        [name.to_s.classify, "Finder"].join.constantize
      rescue NameError, LoadError => err
        Finder
      end

      # @param [String,Hash] パス文字列もしくはその断片の情報（ハッシュ）
      # @yieldreturn [String] デフォルトのファイル名
      # @return [String] 部分テンプレートパス
      def init_path(arg, &default_file_proc)
        if arg.respond_to?(:fetch)
          File.join(arg.fetch(:dir, DEFAULT_CONTROLLER_DIR), MIDDLE_DIR, arg.fetch(:file, &default_file_proc))
        else
          arg
        end.to_s
      end

      # 区切り文字付き参照ファインダー名を分割
      # @param [String] str 区切り文字付き参照ファインダー名
      # @return [Hash] 参照ファインダー名と部分テンプレートのパスの断片を持つハッシュ
      def parse(str)
        result = {}
        result[:finder], result[:dir] = *str.split(SEPARATORS[:dir])
        %i(finder dir).each do |key|
          result[key], result[:file] = *result[key].split(SEPARATORS[:file]) if !result[key].nil? && result[key].include?(SEPARATORS[:file])
        end
        result.compact
      end
    end

    # @!attribute [r] name
    #   @return [String] 参照ファインダー名
    # @!attribute [r] templates
    #   @return [Array<String>] 部分テンプレートのパスの配列
    # @!attribute [r] locals
    #   @return [Hash] 部分テンプレートにローカル変数として渡すオブジェクト群
    attr_reader :name, :templates, :locals

    # @see RadioButtonTab::Page#initialize
    def initialize(*args, name, templates: [], locals: {}, **options, &block)
      @name = name
      @templates = templates.map{|tmp| self.class.init_path(tmp){ self.name } }
      @locals = locals
      super(*args, human_name, options, &block)
    end

    # @return [String] 翻訳した参照ファインダー名
    def human_name
      I18n.t(name, scope: "finders", default: proc{|key, params| key.humanize })
    end

    # @return [String] 一意な文字列。要素のID属性用。
    def unique_id
      sanitize_to_id(:finder)
    end

    # @param [Array<String>] paths 部分テンプレートパスの配列
    # @return [ActionView::SafeBuffer] 部分テンプレートの内容
    def render(paths, *args, &block)
      h.render(paths.shift, *args, &block)
    rescue ActionView::MissingTemplate => err
      if paths.empty?
        raise err
      else
        render(paths, *args, &block)
      end
    end

    private

    def default_html_attributes
      {class: "reference_field-finder", data: {finder: name}}
    end

    def default_content_proc
      proc{ render(templates.dup, finder: self, **locals) }
    end
  end
end
