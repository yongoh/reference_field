module ReferenceField
  module FormBuilder

    # @param [String] association_name 関連名
    # @param [ActiveRecord::Base,nil] record 参照先レコード
    # @param [Boolean] self_referencing 自己参照を許可するかどうか
    # @return [ActiveSupport::SafeBuffer] 単数関連対応参照フィールド
    # @see ActionView::Base#reference_field
    def reference_field(association_name, record = object.association(association_name).load_target, self_referencing: true, **options)
      rendered = @template.reference_field(object_name, association_name, record, options) do |field|
        # 関連の種類を設定
        field.reflection = reflect_on_association(association_name)

        # 自己参照を禁止
        field.prohibited_targets = [object] unless self_referencing

        yield(field) if block_given?
      end

      # バリデーションエラーがある場合に指標をつける
      if valid_association?(association_name)
        rendered
      else
        @template.content_tag(:div, rendered, class: "field_with_errors")
      end
    end

    # @param [String] association_name 関連名
    # @param [Array<ActiveRecord::Base>] records 参照先レコード群
    # @param [Boolean] self_referencing 自己参照を許可するかどうか
    # @param [Hash] options ActionField用・参照フィールド用が混合したオプション群
    # @return [ActiveSupport::SafeBuffer] 複数関連対応参照フィールド
    # @see ActionField::FormHelper#action_field
    # @see ActionView::Base#collection_reference_field
    def collection_reference_field(association_name, records = object.association(association_name).load_target, self_referencing: true, **options)
      reflection = reflect_on_association(association_name)

      @template.collection_reference_field(object_name, [association_name, :attributes].join("_"), records, options) do |field|
        # 関連の種類を設定
        field.reflection = reflection

        # 自己参照を禁止
        field.prohibited_targets = [object] unless self_referencing

        yield(field) if block_given?
      end
    end

    # @return [ActiveSupport::SafeBuffer] ラベル要素
    def reference_label(*args, &block)
      @template.reference_label(object_name, *args, &block)
    end

    private

    def reflect_on_association(association_name)
      object.association(association_name).reflection
    end

    # @param [String] association_name 関連名
    # @return [Boolean] 外部キーの値が妥当かどうか
    def valid_association?(association_name)
      reflection = reflect_on_association(association_name)
      (!reflection.polymorphic? || object.errors[reflection.foreign_type].empty?) && object.errors[reflection.foreign_key].empty?
    end
  end
end
