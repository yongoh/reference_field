require 'rails/generators/named_base'

module ReferenceField
  module Generators
    class FinderGeneratorGenerator < Rails::Generators::NamedBase
      source_root File.expand_path("../templates", __FILE__)

      class_option :ajax, type: :boolean, default: false, desc: "Finder uses ajax"

      def create_root_derectory
        empty_directory generator_root
      end

      def create_generator_file
        template "finder_generator.rb.erb", File.join(generator_root, "#{file_path}_finder_generator.rb")
      end

      def create_usage_file
        template "USAGE.erb", File.join(generator_root, "USAGE")
      end

      def create_finder_file
        template "finder.rb.erb", "app/finders/#{file_path}_finder.rb"
      end

      def create_template_file
        template "templates/_finder.html.erb", File.join(templates_root, "#{view_file_path}.html.erb")
      end

      def create_asset_files
        template "finder.js.coffee.erb", "app/assets/javascripts/finders/#{file_path}_finder.js.coffee"
        template "finder.css.scss.erb", "app/assets/stylesheets/finders/#{file_path}.css.scss"
      end

      hook_for :test_framework, as: "reference_field:finder_generator"

      private

      def generator_root
        "lib/generators/#{file_path}_finder"
      end

      def templates_root
        File.join(generator_root, "templates")
      end

      def command_name
        "#{name.underscore}_finder"
      end

      def superclass_name
        "ReferenceField::Finder"
      end

      def view_file_path
        file_path.sub(/\w+$/, '_\0')
      end

      def js_class_name
        class_name.gsub("::", ".")
      end
    end
  end
end
