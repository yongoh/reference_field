require 'rails/generators/named_base'
require "reference_field/generators/finders_argument"

module ReferenceField
  module Generators
    class FinderContainerGenerator < Rails::Generators::NamedBase
      include Rails::Generators::ResourceHelpers
      include FindersArgument

      source_root File.expand_path("../templates", __FILE__)

      def create_generator_file
        template "_finder_container.html.erb", "app/views/#{controller_name}/finders/_finder_container.html.erb"
      end

      hook_for :test_framework, as: "reference_field:finder_container"
    end
  end
end
