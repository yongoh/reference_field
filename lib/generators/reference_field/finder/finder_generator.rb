require 'rails/generators/named_base'

module ReferenceField
  module Generators
    class FinderGenerator < Rails::Generators::NamedBase

      source_root File.expand_path("../templates", __FILE__)

      class_option :finder_class, type: :boolean, default: true
      class_option :assets, type: :boolean, default: true

      def create_finder_file
        if options[:finder_class]
          template "finder.rb.erb", "app/finders/#{file_path}_finder.rb"
        end
      end

      def create_view_file
        template "_finder.html.erb", "app/views/application/finders/#{view_file_path}.html.erb"
      end

      def create_asset_files
        if options[:assets]
          template "finder.js.coffee.erb", "app/assets/javascripts/finders/#{file_path}_finder.js.coffee"
          template "finder.css.scss.erb", "app/assets/stylesheets/finders/#{file_path}.css.scss"
        end
      end

      hook_for :test_framework, as: "reference_field:finder"

      private

      def superclass_name
        "ReferenceField::Finder"
      end

      def view_file_path
        file_path.sub(/\w+$/, '_\0')
      end

      def js_class_name
        class_name.gsub("::", ".")
      end
    end
  end
end
