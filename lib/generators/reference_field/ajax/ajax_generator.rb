require 'rails/generators/named_base'

module ReferenceField
  module Generators
    class AjaxGenerator < Rails::Generators::NamedBase
      include Rails::Generators::ResourceHelpers

      source_root File.expand_path("../templates", __FILE__)

      class_option :responding, type: :boolean, default: true, desc: "Add respond code to controller"

      def add_format
        if options[:responding]
          gsub_file "app/controllers/#{controller_name}_controller.rb", regexp do |matched|
            matched = matched.match(regexp)
            matched.captures.insert(2, respond_to_code.gsub(/^/, matched[1] + " " * 2)).join
          end
        end
      end

      def create_view_file
        template "_index.html.erb", "app/views/#{controller_name}/ajax/_index.html.erb"
      end

      hook_for :test_framework, as: "reference_field:ajax"

      private

      def regexp
        /^(\s*)(def index$.+?)^(\1end)/m
      end

      def respond_to_code
        <<~EOF
        respond_to do |format|
          format.html
          format.json
          format.js{ render formats: :js }
        end
        EOF
      end
    end
  end
end
