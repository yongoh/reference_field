require 'generators/rspec'

module Rspec
  module Generators
    module ReferenceField
      class FinderGenerator < Base

        source_root File.expand_path("../templates", __FILE__)

        class_option :finder_class, type: :boolean, default: true

        def create_finder_spec_file
          if options[:finder_class]
            template "finder_spec.rb.erb", "spec/finders/#{file_path}_finder_spec.rb"
          end
        end

        def create_view_spec_file
          template "_finder.html.erb_spec.rb.erb", "spec/views/application/finders/#{view_file_path}.html.erb_spec.rb"
        end

        private

        def view_file_path
          file_path.sub(/\w+$/, '_\0')
        end
      end
    end
  end
end
