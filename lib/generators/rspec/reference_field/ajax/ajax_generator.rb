require 'generators/rspec'
require 'rails/generators/resource_helpers'

module Rspec
  module Generators
    module ReferenceField
      class AjaxGenerator < Base
        include Rails::Generators::ResourceHelpers

        source_root File.expand_path("../templates", __FILE__)

        class_option :fixture, type: :string, default: "factory_bot", desc: "ActiveRecord fixture used in the test"

        def create_view_spec_file
          template "_index.html.erb_spec.rb.erb", "spec/views/#{controller_name}/ajax/_index.html.erb_spec.rb"
        end
      end
    end
  end
end
