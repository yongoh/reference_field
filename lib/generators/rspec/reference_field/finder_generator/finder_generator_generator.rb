require 'generators/rspec'

module Rspec
  module Generators
    module ReferenceField
      class FinderGeneratorGenerator < Base

        source_root File.expand_path("../templates", __FILE__)

        class_option :ajax, type: :boolean, default: false, desc: "Finder uses ajax"
        class_option :test_argument, type: :string, default: "foo/bar/baz", desc: "Command argument in the test"

        def create_root_derectory
          empty_directory generator_root
        end

        def create_generator_file
          template "finder_generator.rb.erb", File.join(generator_root, "#{file_path}_finder_generator.rb")
        end

        def create_generator_spec_file
          template "finder_generator_spec.rb.erb", "spec/generators/#{file_path}_finder_generator_spec.rb"
        end

        def create_finder_spec_file
          template "finder_spec.rb.erb", "spec/finders/#{file_path}_finder_spec.rb"
        end

        def create_template_file
          template "templates/_finder.html.erb_spec.rb.erb", File.join(templates_root, "#{view_file_path}.html.erb_spec.rb.erb")
        end

        private

        def generator_root
          "lib/generators/rspec/#{file_path}_finder"
        end

        def templates_root
          File.join(generator_root, "templates")
        end

        def command_name
          "#{name.underscore}_finder"
        end

        def command_options
          if options[:ajax]
            %w(
              --responding=false
              --fixture=dummy_record
            )
          else
            []
          end
        end

        def view_file_path
          file_path.sub(/\w+$/, '_\0')
        end
      end
    end
  end
end
