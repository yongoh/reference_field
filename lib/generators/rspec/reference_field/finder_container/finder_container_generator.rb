require 'generators/rspec'
require 'rails/generators/resource_helpers'
require "reference_field/generators/finders_argument"

module Rspec
  module Generators
    module ReferenceField
      class FinderContainerGenerator < Base
        include Rails::Generators::ResourceHelpers
        include ::ReferenceField::Generators::FindersArgument

        source_root File.expand_path("../templates", __FILE__)

        def create_generator_spec_file
          template "_finder_container.html.erb_spec.rb.erb", "spec/views/#{controller_name}/finders/_finder_container.html.erb_spec.rb"
        end
      end
    end
  end
end
